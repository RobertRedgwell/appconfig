﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using AppConfig.Core;

namespace AppConfig.Manager
{
    public partial class CreateUserForm : Form
    {
        public CreateUserForm()
        {
            InitializeComponent();
        }

        public string EmailAddress { get { return textBoxEmailAddress.Text; } }

        public string Password { get { return textBoxPassword.Text; } }

        private void buttonCreateAccount_Click(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(textBoxEmailAddress.Text, Constants.EmailRegex))
            {
                MessageBox.Show("Please enter a valid email address.", "Please check your email address.");
                textBoxEmailAddress.Focus();
                return;
            }

            if (string.IsNullOrEmpty(textBoxPassword.Text) || textBoxPassword.Text.Length < 5)
            {
                MessageBox.Show("Please ensure your password is at least 5 characters long.", "Please enter a longer password.");
                textBoxPassword.Focus();
                return;
            }
            if (textBoxPassword.Text != textBoxConfirmPassword.Text)
            {
                MessageBox.Show("Please ensure your password is the same in both fields.", "Please confirm your password.");
                textBoxPassword.Focus();
                return;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        }
    }
}
