﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Microsoft.SqlServer.Management.Smo;
using AppConfig.Manager.Migrations;
using AppConfig.Model;
using Microsoft.Win32;
using ServiceStack.OrmLite;
using Microsoft.SqlServer.Management.Common;
using System.ServiceProcess;
using AppConfig.Core;

namespace AppConfig.Manager
{
    public partial class Manager : Form
    {
        private const string ApiWebsiteFolderRegistryKey = @"SOFTWARE\ZapConfig\ApiWebsite";
        private const string ApiWebsiteFolderRegistryValue = "InstallLocation";
        private const string AppConfigDbConnectionStringKey = "AppConfigDb";
        private const string AppConfigDbName = "AppConfig";
        private const string AppConfigDbUserName = "AppConfigUser";
        private const string DefaultConnectionString = @"Data Source=localhost\SqlExpress;Integrated Security=True;";
        private readonly IisManager _iisManager;
        private bool _isMixedMode;
        private string _appConnectionString;

        public Manager()
        {
            InitializeComponent();
            _iisManager = new IisManager(OnIisProgress, OnIisComplete);

            dbConnectionWorker.DoWork += (s, e) => { e.Result = AttemptDatabaseConnection(e.Argument.ToString()); };
            dbConnectionWorker.RunWorkerCompleted += (s, e) => { OnDbConnectionCompleted(e.Result); };

            dbMixedModeWorker.DoWork += (s, e) => { EnableMixedMode(ConfigurationManager.ConnectionStrings[Manager.AppConfigDbConnectionStringKey].ConnectionString); };
            dbMixedModeWorker.RunWorkerCompleted += (s, e) => { OnMixedModeSet(); };
        }

        private void Manager_Load(object sender, EventArgs e)
        {
            LoadDbConnectionStatus();

            UpdateIisInstallationState();

            UpdateWebsiteInstallationState();
        }

        /// <summary>
        /// Grabs the connection string the apiwebsite web.config, and sets the ConnectionString textbox accordingly
        /// </summary>
        private void LoadDbConnectionStatus()
        {
            var configLoaded = false;
            var appConnectionString = string.Empty;

            while (!configLoaded)
            {
                try
                {
                    var apiWebsiteConfig = GetApiWebsiteConfigLocation();
                    appConnectionString = QueryApplicationConnectionString(apiWebsiteConfig, DefaultConnectionString, false);
                }
                catch (UserAbortedException)
                {
                    Application.Exit();
                }
                
                if (appConnectionString == null) continue;
                configLoaded = true;
            }

            if (textBoxMasterConnectionString.Text == string.Empty)
            {
                var connectionStringElement = ConfigurationManager.ConnectionStrings[Manager.AppConfigDbConnectionStringKey];
                textBoxMasterConnectionString.Text = 
                    connectionStringElement == null ? DefaultConnectionString : connectionStringElement.ConnectionString;
            }

            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings.Remove(Manager.AppConfigDbConnectionStringKey);
            config.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings(Manager.AppConfigDbConnectionStringKey, textBoxMasterConnectionString.Text));
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionStrings");

            buttonVerifyMasterConnection.Enabled = false;
            labelDatabaseConnectionStatus.Visible = true;
            SetIconState(iconMasterConnectionString, false);
            SetIconState(iconDatabaseInstalled, false);
            labelDatabaseConnectionStatus.Text = "Connecting...";
            textBoxMasterConnectionString.Enabled = false;
            dbConnectionWorker.RunWorkerAsync(textBoxMasterConnectionString.Text);
        }

        private string AttemptDatabaseConnection(string connectionString)
        {
            var builder = new DbConnectionStringBuilder();
            try
            {
                builder.ConnectionString = connectionString;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(string.Format("An error occurred installing/upgrading the database. Ensure the connection string is valid. Error: {0}", ex.Message));
                return null;
            }

            builder.Remove("Initial Catalog");

            try
            {
                using (var c = new OrmLiteConnectionFactory(builder.ConnectionString, SqlServerDialect.Provider).Open())
                {
                    var exists = c.QuerySingle<int?>(string.Format("SELECT db_id('{0}')", AppConfigDbName));
                    if (!exists.HasValue)
                        c.ExecuteSql(string.Format("CREATE DATABASE [{0}]", AppConfigDbName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("An error occurred installing/upgrading the database. Ensure the connection string is valid. Error: {0}", ex.Message));
                return null;
            }

            // Switch to the ZapConfig database
            builder.Add("Initial Catalog", AppConfigDbName);

            try
            {
                DatabaseMigrator.MigrateDatabaseToLatest(builder.ConnectionString);
            }
            catch (MigrationException ex)
            {
                MessageBox.Show(string.Format("An error occurred installing/upgrading the database. Ensure the connection string is valid. Error: {0}", ex.Message));
                return null;
            }

            // create the app config user
            try
            {
                var password = GenerateDbPassword();                    
                var server = new Server(new ServerConnection(new SqlConnection(builder.ConnectionString)));
                _isMixedMode = server.LoginMode == ServerLoginMode.Mixed;
                var database = server.Databases[AppConfigDbName];
                if (!server.Logins.Contains(AppConfigDbUserName))
                {
                    var login = new Login(server, AppConfigDbUserName);
                    login.LoginType = LoginType.SqlLogin;
                    login.PasswordExpirationEnabled = false;
                    login.PasswordPolicyEnforced = false;
                    login.DefaultDatabase = AppConfigDbName;
                    login.Create(password);

                    // Add user to database, linked to login
                    if (database.Users.Contains(login.Name))
                    {
                        database.Users[login.Name].Drop();
                    }

                    var user = new Microsoft.SqlServer.Management.Smo.User(database, login.Name);
                    user.Login = login.Name;
                    user.Create();
                }
                else
                {
                    server.Logins[AppConfigDbUserName].ChangePassword(password);
                }

                var permissions = new DatabasePermissionSet();
                permissions.Select = true;
                permissions.Insert = true;
                permissions.Update = true;
                permissions.Delete = true;
                permissions.Execute = true;
                database.Grant(permissions, AppConfigDbUserName);

                // update our connection string 
                builder.Remove("Integrated Security");
                builder.Add("User Id", AppConfigDbUserName);
                builder.Add("Password", password);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("An error occurred setting up the ZapConfig database user. Ensure the connection string is valid and the connection string's user has permission to create new Sql users. Error: {0}", ex.Message));
                return null;
            }

            return builder.ConnectionString;
        }

        private static string GenerateDbPassword()
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty);
        }

        private void EnableMixedMode(string connectionString)
        {
            var server = new Server(new ServerConnection(new SqlConnection(connectionString)));
            server.LoginMode = ServerLoginMode.Mixed;
            server.Alter();
            var sc = new ServiceController("MSSQL$" + server.ServiceName);
            sc.Stop();
            sc.WaitForStatus(ServiceControllerStatus.Stopped);
            sc.Start();
            sc.WaitForStatus(ServiceControllerStatus.Running);
            _isMixedMode = true;
        }

        private void OnMixedModeSet()
        {
            MessageBox.Show("Mixed mode authentication enabled.");
            QueryAccountSetup(_appConnectionString);
        }

        private void OnDbConnectionCompleted(object resultConnectionString)
        {
            textBoxMasterConnectionString.Enabled = true;

            if (resultConnectionString == null)
            {
                labelDatabaseConnectionStatus.Visible = false;
                buttonVerifyMasterConnection.Enabled = true;
                labelDatabaseConnectionStatus.Text = "No database found";
            }
            else
            {
                SetIconState(iconMasterConnectionString, true);
                SetIconState(iconDatabaseInstalled, true);
                labelDatabaseConnectionStatus.Text = "Verified";

                QueryApplicationConnectionString(GetApiWebsiteConfigLocation(), resultConnectionString.ToString(), true);

                if (!_isMixedMode)
                {
                    MessageBox.Show(
                            this,
                            "SQL mixed mode authentication is required, but is not currently enabled. Click Ok to enable it now.",
                            "SQL mixed mode authentication required.",
                            MessageBoxButtons.OK);

                    dbMixedModeWorker.RunWorkerAsync();
                }
                else
                {
                    QueryAccountSetup(resultConnectionString.ToString());
                }
            }
        }

        private string QueryApplicationConnectionString(string apiWebsiteConfig, string defaultConnectionString, bool overwriteExisting)
        {
            try
            {
                var config = new XmlDocument();
                config.Load(apiWebsiteConfig);

                var rootNode = config.SelectSingleNode("//configuration");
                if (rootNode == null)
                {
                    throw new XmlException("Configuration element not found in web.config. Are you sure this is the folder of the ZapConfig server?");
                }

                var connectionStringsContainerNode = rootNode.SelectSingleNode("connectionStrings");
                if (connectionStringsContainerNode == null)
                {
                    connectionStringsContainerNode = config.CreateNode(XmlNodeType.Element, "connectionStrings", string.Empty);
                    rootNode.AppendChild(connectionStringsContainerNode);
                }

                var connectionStringNode = connectionStringsContainerNode.SelectSingleNode(string.Format("add[@name='{0}']", AppConfigDbConnectionStringKey));
                if (connectionStringNode == null)
                {
                    connectionStringNode = config.CreateNode(XmlNodeType.Element, "add", string.Empty);
                    var nameAttribute = config.CreateAttribute("name");
                    nameAttribute.Value = AppConfigDbConnectionStringKey;

                    var connectionStringAttribute = config.CreateAttribute("connectionString");
                    connectionStringAttribute.Value = defaultConnectionString;

                    var providerNameAttribute = config.CreateAttribute("providerName");
                    providerNameAttribute.Value = "System.Data.EntityClient";

                    connectionStringNode.Attributes.Append(nameAttribute);
                    connectionStringNode.Attributes.Append(connectionStringAttribute);
                    connectionStringNode.Attributes.Append(providerNameAttribute);

                    connectionStringsContainerNode.AppendChild(connectionStringNode);
                }
                else if (overwriteExisting)
                {
                    var attribute = connectionStringNode.Attributes["connectionString"];
                    if (attribute == null)
                    {
                        var connectionStringAttribute = config.CreateAttribute("connectionString");
                        connectionStringAttribute.Value = defaultConnectionString;
                        connectionStringNode.Attributes.Append(connectionStringAttribute);
                    }
                    else
                    {
                        attribute.Value = defaultConnectionString;
                    }
                }

                config.Save(apiWebsiteConfig);

                _appConnectionString = connectionStringNode.Attributes["connectionString"].Value;
                return _appConnectionString;
            }
            catch (XmlException ex)
            {
                MessageBox.Show(string.Format("An error occurred setting the connection string in the web.config for the api website. Please ensure you have selected the correct folder for the ZapConfig website location. Error: {0}", ex.Message));
                return null;
            }
        }

        private void QueryAccountSetup(string connectionString)
        {
            var dbFactory = new OrmLiteConnectionFactory(connectionString, SqlServerDialect.Provider);
            try
            {
                Account existingAccount;
                using (var c = dbFactory.Open())
                {
                    existingAccount = c.FirstOrDefault<Account>(a => a.Id != -1);
                }

                if (existingAccount == null)
                {
                    var createUserForm = new CreateUserForm();
                    while (createUserForm.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                    {
                        if (MessageBox.Show(
                                this,
                                "You must create an account! If you quit now, you will have to launch the ZapConfig manager again before you can use ZapConfig. Are you sure you want to quit?",
                                "Are you sure you want to quit?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            Application.Exit();
                            return;
                        }
                    }

                    using (var c = dbFactory.Open())
                    {
                        var account = new Account
                        {
                            AccountType = EAccountType.Storm,
                            ApiKey = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 16),
                            Name = "SelfHosted",
                            ValidationString = (Guid.NewGuid().ToString() + Guid.NewGuid().ToString()).Replace("-", string.Empty),
                            HasBeenValidated = true,
                        };
                        account.SetLicenseData(string.Empty, DateTime.MinValue, ELicenseStatus.Unknown, false);
                        c.Save(account);

                        c.Save(new AppConfig.Model.User(c.GetLastInsertId(), createUserForm.EmailAddress, createUserForm.Password));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("An error occurred setting up your account. Error: {0}", ex.Message));
            }

        }

        private void SetIconState(PictureBox icon, bool okay)
        {
            icon.Image = okay ? Images.tick : Images.cross;
        }

        private string GetApiWebsiteConfigLocation()
        {
            string apiWebsiteFolder = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "../Server");

            // check for registry key. If not found, check for the expected location of the api website
            var local64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            var zapConfigKey = local64.OpenSubKey(ApiWebsiteFolderRegistryKey);
            if (zapConfigKey != null)
            {
                var registryValue = zapConfigKey.GetValue(ApiWebsiteFolderRegistryValue) as string;
                if (registryValue != null)
                {
                    apiWebsiteFolder = registryValue;
                }
            }

            while (!File.Exists(Path.Combine(apiWebsiteFolder, "Web.config")))
            {
                MessageBox.Show(
                            this,
                            "Please specify the path to the api website to proceed with the configuration.",
                            "Api website folder required",
                            MessageBoxButtons.OK);

                var browser = new FolderBrowserDialog();
                var result = browser.ShowDialog(this);
                if (result == DialogResult.Cancel)
                {
                    if (MessageBox.Show(
                            this,
                            "You must specify the path to the api website to proceed with the configuration. Do you want to abort the configuration?",
                            "Are you sure you want to quit?",
                            MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        throw new UserAbortedException();
                    }
                    continue; // loop back around again
                }

                apiWebsiteFolder = browser.SelectedPath;
            }

            var key = local64.CreateSubKey(ApiWebsiteFolderRegistryKey, RegistryKeyPermissionCheck.ReadWriteSubTree);
            key.SetValue(ApiWebsiteFolderRegistryValue, apiWebsiteFolder);
            key.Close();

            return Path.Combine(apiWebsiteFolder, "Web.config");
        }

        private void UpdateIisInstallationState()
        {
            _iisManager.QueryInstallationState();

            var isWebserverInstalled = _iisManager.InstallationState == IisManager.EInstallationState.AllOkay;
            buttonFixWebserver.Enabled = !isWebserverInstalled;
            SetIconState(iconWebserverInstalled, isWebserverInstalled);
            labelWebserverNotInstalled.Text = _iisManager.GetStatusMessage();
            groupBoxConfigureWebserver.Enabled = isWebserverInstalled;
        }

        private void UpdateWebsiteInstallationState()
        {
            var isWebsiteInstalled = _iisManager.IsWebsiteInstalledAndStarted();
            SetIconState(iconWebserverConfigured, isWebsiteInstalled);
            textBoxHostname.Text = _iisManager.GetWebsiteHostname();
            textBoxPort.Text = _iisManager.GetWebsitePort();
            textBoxVirtualDirectory.Text = _iisManager.GetWebsiteVirtualDirectory();
            buttonSetWebserver.Enabled = !isWebsiteInstalled;
        }

        private void OnIisComplete(bool success)
        {
            UpdateIisInstallationState();
            if (!success)
            {
                MessageBox.Show("An error occurred trying to automatically fix IIS.");
            }
        }

        private void OnIisProgress(string status)
        {
            labelWebserverNotInstalled.Text = "Processing: " + status;
        }

        private void textBoxMasterConnectionString_TextChanged(object sender, EventArgs e)
        {
            buttonVerifyMasterConnection.Enabled = true;
            SetIconState(iconMasterConnectionString, false);
        }

        private void buttonVerifyMasterConnection_Click(object sender, EventArgs e)
        {
            LoadDbConnectionStatus();
        }

        private void buttonFixWebserver_Click(object sender, EventArgs e)
        {
            buttonFixWebserver.Enabled = false;
            _iisManager.FixInstallationIssue();
        }

        private void buttonSetWebserver_Click(object sender, EventArgs e)
        {
            int port;
            if (!int.TryParse(textBoxPort.Text, out port) || port < 1)
            {
                MessageBox.Show("Please enter a positive number for the port.");
                return;
            }

            var hostname = textBoxHostname.Text;
            if (string.IsNullOrEmpty(hostname)) hostname = "*";

            var virtualDirectory = textBoxVirtualDirectory.Text;
            if (string.IsNullOrEmpty(virtualDirectory)) virtualDirectory = "/";

            string serverFolder;

            try
            {
                serverFolder = Path.GetDirectoryName(GetApiWebsiteConfigLocation());
            }
            catch (UserAbortedException)
            {
                return;
            }

            var result = _iisManager.SetWebsite(hostname, port, virtualDirectory, serverFolder);
            if (result != null) MessageBox.Show(string.Format("An error occurred: {0}", result));
            UpdateWebsiteInstallationState();            
        }

        private void textBoxWebsiteConfigTextChanged(object sender, EventArgs e)
        {
            buttonSetWebserver.Enabled = true;
        }
    }
}
