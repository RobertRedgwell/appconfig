﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Web.Administration;
using Microsoft.Win32;

namespace AppConfig.Manager
{
    public class IisManager
    {
        private static readonly string[] RequiredComponents = new[] { "NetFxExtensibility", "ASPNET", "ISAPIExtensions", "ISAPIFilter", "DefaultDocument", "HttpErrors", "HttpLogging", "RequestFiltering" };
        private const string IISComponentKey = @"Software\Microsoft\InetStp\Components";
        private const string FeatureNamePrefix = "/FeatureName:IIS-";
        private const string WebsiteName = "ZapConfig Server";
        private const string AppPoolName = "ZapConfig Application Pool";

        private readonly BackgroundWorker _backgroundWorker;

        public enum EInstallationState
        {
            Unknown,
            IisNotFound,
            MissingIisComponents,
            AspNetNotRegistered,
            AllOkay
        }

        public IisManager(Action<string> onProgressUpdate, Action<bool> onCompleted)
        {
            _backgroundWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true
            };

            _backgroundWorker.ProgressChanged += (s, e) => 
            { 
                onProgressUpdate("" + e.UserState); 
            };

            _backgroundWorker.RunWorkerCompleted += (s, e) =>
            {
                onCompleted(Boolean.TrueString.Equals("" + e.Result));
            };

            _backgroundWorker.DoWork += (s, e) => { e.Result = RunIisInstallFixer(); };
        }

        public EInstallationState InstallationState { get; private set; }
        
        public EInstallationState QueryInstallationState()
        {
            if (!IsIISInstalled())
            {
                return InstallationState = EInstallationState.IisNotFound;
            }

            if (!AreAllIISComponentsInstalled())
            {
                return InstallationState = EInstallationState.MissingIisComponents;
            }

            if (!IsAspnetRegistered())
            {
                return InstallationState = EInstallationState.AspNetNotRegistered;
            }
            InstallationState = EInstallationState.AllOkay;
            
            return InstallationState;
        }

        /// <summary>
        /// Attempts to fix the current issues. 
        /// </summary>
        public void FixInstallationIssue()
        {
            _backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Run inside the thread of the background worker
        /// </summary>
        private bool RunIisInstallFixer() 
        {
            var currentState = InstallationState;
            while (InstallationState != EInstallationState.AllOkay)
            {
                switch (InstallationState)
                {
                    case IisManager.EInstallationState.IisNotFound:
                        RunCommand(
                            @"c:\windows\sysnative\dism.exe", 
                            "/online /enable-feature /norestart /FeatureName:IIS-WebServerRole /FeatureName:IIS-WebServer"
                        );
                        RegisterAspWithIIS(); // do this too to avoid weird issues where it might have been installed before but now it's not
                        break;
                    case IisManager.EInstallationState.MissingIisComponents:
                        var featureList = new StringBuilder();
                        foreach (var c in RequiredComponents)
                        {
                            featureList.Append(FeatureNamePrefix);
                            featureList.Append(c);
                            featureList.Append(" ");
                        }
                        RunCommand(
                            @"c:\windows\sysnative\dism.exe",
                            "/online /enable-feature /norestart " + featureList
                        );                        
                        break;
                    case IisManager.EInstallationState.AspNetNotRegistered:
                        RegisterAspWithIIS();
                        break;
                }
                if (currentState == QueryInstallationState())
                {
                    return false;
                }
                currentState = InstallationState;
            }
            return true;
        }

        /// <summary>
        /// Gets an error reason, if any. If this returns string.empty, then everything is fine!
        /// </summary>
        /// <returns></returns>
        public string GetStatusMessage()
        {
            switch (InstallationState)
            {
                case IisManager.EInstallationState.IisNotFound:
                    return "IIS was not found on this machine.";
                case IisManager.EInstallationState.MissingIisComponents:
                    return "Not all required IIS components are installed.";
                case IisManager.EInstallationState.AspNetNotRegistered:
                    return "AspNet 4.0 is not registered with IIS.";
                default:
                    return "IIS is installed and configured.";
            }
        }

        public string SetWebsite(string hostname, int port, string virtualDirectory, string pathToServer)
        {
            if (IsPortAlreadyTaken(port))
            {
                return "The specified port is already in use by another website. Please select another.";
            }

            var manager = new ServerManager();

            try
            {
                var appPool = manager.ApplicationPools.FirstOrDefault(a => a.Name == AppPoolName);
                if (appPool == null) appPool = manager.ApplicationPools.Add(AppPoolName);

                appPool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                appPool.ManagedRuntimeVersion = "v4.0";

                var bindingInfo = string.Format("*:{0}:{1}", port, hostname);
                var website = GetWebsite(manager);
                if (website == null)
                {
                    website = manager.Sites.Add(WebsiteName, "http", bindingInfo, pathToServer);
                }
                else
                {
                    website.Bindings.Clear();
                    website.Bindings.Add(bindingInfo, "http");
                }
                website.ApplicationDefaults.ApplicationPoolName = AppPoolName;
                website.Applications.Clear();
                website.Applications.Add(virtualDirectory, pathToServer);
                manager.CommitChanges();
                
                // Set permissions for IIS_IUSRS for read access to pathToServer
                //var user = string.Format(@"{0}\IIS_IUSRS", Environment.MachineName);
                var user = new NTAccount("IIS_IUSRS");

                var info = new DirectoryInfo(pathToServer);
                var accessControl = info.GetAccessControl();
                var iFlags = InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit;
                accessControl.AddAccessRule(new FileSystemAccessRule(user, FileSystemRights.ReadAndExecute, iFlags, PropagationFlags.None, AccessControlType.Allow));
                accessControl.AddAccessRule(new FileSystemAccessRule(user, FileSystemRights.Write, iFlags, PropagationFlags.None, AccessControlType.Allow));
                info.SetAccessControl(accessControl);

                Thread.Sleep(1000); // wait for IIS to do its thing....

                website = GetWebsite();
                website.Start();
            }
            catch (ServerManagerException se)
            {
                return se.Message;
            }
            return null;
        }

        public bool IsWebsiteInstalledAndStarted()
        {
            var website = GetWebsite();
            var state = ObjectState.Unknown;
            if (website != null) state = website.State;

            return state == ObjectState.Started && DoesIISUserHaveAccessToFolder();
        }

        public string GetWebsiteHostname()
        {
            var site = GetWebsite();
            return (site == null || site.Bindings.Count == 0)
                ? "*"
                : site.Bindings[0].Host;
        }

        public string GetWebsitePort()
        {
            var site = GetWebsite();
            return (site == null || site.Bindings.Count == 0)
                ? "80"
                : site.Bindings[0].EndPoint.Port.ToString();
        }

        public string GetWebsiteVirtualDirectory()
        {
            var site = GetWebsite();
            return (site == null || site.Applications.Count == 0 || site.Applications[0].VirtualDirectories.Count == 0)
                ? "/"
                : site.Applications[0].VirtualDirectories.FirstOrDefault().Path;
        }

        private Site GetWebsite(ServerManager manager = null)
        {
            return (manager ?? new ServerManager()).Sites.FirstOrDefault(s => s.Name == WebsiteName);
        }

        private bool IsPortAlreadyTaken(int port)
        {
            return new ServerManager().Sites.Any(
                s => s.Name != WebsiteName &&
                s.Bindings.Any(b => b.EndPoint.Port == port)
            );
        }

        private void RegisterAspWithIIS()
        {
            RunCommand(
                @"C:\Windows\System32\cmd.exe",
                @"/c C:\Windows\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis -i"
            );
        }

        private void RunCommand(string filename, string arguments)
        {
            using (var process = new Process())
            {
                process.StartInfo = new ProcessStartInfo(filename, arguments)
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                };
                process.Start();
                var currentStatus = "Initialising...";
                while (!process.StandardOutput.EndOfStream)
                {
                    var status = process.StandardOutput.ReadLine();
                    if (status != string.Empty) currentStatus = status;

                    _backgroundWorker.ReportProgress(0, currentStatus);
                }
                process.WaitForExit();
            }
        }

        private bool IsIISInstalled()
        {
            return IsRegistryValueEqual(@"Software\Microsoft\InetStp", "MajorVersion", "7");
        }

        private bool AreAllIISComponentsInstalled()
        {
            foreach (var c in RequiredComponents)
            {
                if (!IsRegistryValueEqual(IISComponentKey, c, "1"))
                    return false;
            }
            return true;
        }

        private bool IsAspnetRegistered()
        {
            return IsRegistryKeyPresent(@"Software\Microsoft\ASP.NET\4.0.30319.0");
        }

        private bool IsRegistryKeyPresent(string keyPath)
        {
            return GetRegistryKey(keyPath) != null;
        }

        private bool IsRegistryValueEqual(string keyPath, string valueName, string expectedValue)
        {
            var key = GetRegistryKey(keyPath);
            return key == null 
                ? false
                : "" + key.GetValue(valueName) == expectedValue;
        }

        private RegistryKey GetRegistryKey(string keyPath)
        {
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            foreach (var k in keyPath.Split('\\'))
            {
                key = key.OpenSubKey(k);
                if (key == null) return null;
            }
            return key;
        }

        private bool DoesIISUserHaveAccessToFolder()
        {
            return true;
            /*
            var website = GetWebsite();
            if (website == null || website.Applications.Count == 0 || string.IsNullOrEmpty(website.Applications[0].Path))
                return false;

            try
            {
                var principal = new WindowsPrincipal(new WindowsIdentity("BUILTIN\\IIS_IUSRS"));
                var authRules = Directory
                    .GetAccessControl(website.Applications[0].Path)
                    .GetAccessRules(true, true, typeof(SecurityIdentifier));

                foreach (FileSystemAccessRule accessRule in authRules)
                {
                    
                    if (principal.IsInRole(accessRule.IdentityReference as SecurityIdentifier))
                    {
                        if ((FileSystemRights.ReadAndExecute & accessRule.FileSystemRights) == FileSystemRights.ReadAndExecute)
                        {
                            return accessRule.AccessControlType == AccessControlType.Allow;
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException)
            {
            }
            return false;*/
        }
    }
}
