﻿/****** Object:  Table [dbo].[Account]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[Name] [varchar](8000) NULL,
	[ApiKey] [varchar](8000) NULL,
	[License] [varchar](8000) NULL,
	[AccountType] [varchar](8000) NOT NULL,
	[ValidationString] [varchar](8000) NULL,
	[HasBeenValidated] [bit] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AuditEntry]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditEntry](
	[ActionType] [varchar](8000) NOT NULL,
	[Message] [varchar](8000) NULL,
	[When] [datetime] NOT NULL,
	[UserId] [bigint] NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Environment]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Environment](
	[Name] [varchar](8000) NULL,
	[Description] [varchar](8000) NULL,
	[Deleted] [bit] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Group]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Group](
	[Name] [varchar](8000) NULL,
	[Description] [varchar](8000) NULL,
	[CanRead] [bit] NOT NULL,
	[CanWrite] [bit] NOT NULL,
	[CanAdmin] [bit] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupUser]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupUser](
	[UserId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[Package]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Package](
	[Name] [varchar](8000) NULL,
	[Version] [varchar](8000) NULL,
	[Description] [varchar](8000) NULL,
	[Transform] [varchar](8000) NULL,
	[Deleted] [bit] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PackageVersion]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PackageVersion](
	[PackageId] [bigint] NOT NULL,
	[SemanticVersion] [varchar](8000) NULL,
	[Transform] [varchar](8000) NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[EmailAddress] [varchar](8000) NULL,
	[Password] [varchar](8000) NULL,
	[Salt] [varchar](8000) NULL,
	[LastLogin] [datetime] NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Variable]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Variable](
	[Name] [varchar](8000) NULL,
	[Description] [varchar](8000) NULL,
	[DefaultValue] [varchar](8000) NULL,
	[IsRuntimeAccessible] [bit] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VariableEnvironment]    Script Date: 19/01/2014 9:04:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VariableEnvironment](
	[VariableId] [bigint] NOT NULL,
	[EnvironmentId] [bigint] NOT NULL,
	[Value] [varchar](8000) NULL,
	[IsHidden] [bit] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
ALTER TABLE [dbo].[AuditEntry]  WITH CHECK ADD  CONSTRAINT [FK_AuditEntry_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[AuditEntry] CHECK CONSTRAINT [FK_AuditEntry_Account_AccountId]
GO
ALTER TABLE [dbo].[AuditEntry]  WITH CHECK ADD  CONSTRAINT [FK_AuditEntry_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[AuditEntry] CHECK CONSTRAINT [FK_AuditEntry_User_UserId]
GO
ALTER TABLE [dbo].[Environment]  WITH CHECK ADD  CONSTRAINT [FK_Environment_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Environment] CHECK CONSTRAINT [FK_Environment_Account_AccountId]
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK_Group_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK_Group_Account_AccountId]
GO
ALTER TABLE [dbo].[GroupUser]  WITH CHECK ADD  CONSTRAINT [FK_GroupUser_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[GroupUser] CHECK CONSTRAINT [FK_GroupUser_Account_AccountId]
GO
ALTER TABLE [dbo].[GroupUser]  WITH CHECK ADD  CONSTRAINT [FK_GroupUser_Group_GroupId] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([Id])
GO
ALTER TABLE [dbo].[GroupUser] CHECK CONSTRAINT [FK_GroupUser_Group_GroupId]
GO
ALTER TABLE [dbo].[GroupUser]  WITH CHECK ADD  CONSTRAINT [FK_GroupUser_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[GroupUser] CHECK CONSTRAINT [FK_GroupUser_User_UserId]
GO
ALTER TABLE [dbo].[Package]  WITH CHECK ADD  CONSTRAINT [FK_Package_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Package] CHECK CONSTRAINT [FK_Package_Account_AccountId]
GO
ALTER TABLE [dbo].[PackageVersion]  WITH CHECK ADD  CONSTRAINT [FK_PackageVersion_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[PackageVersion] CHECK CONSTRAINT [FK_PackageVersion_Account_AccountId]
GO
ALTER TABLE [dbo].[PackageVersion]  WITH CHECK ADD  CONSTRAINT [FK_PackageVersion_Package_PackageId] FOREIGN KEY([PackageId])
REFERENCES [dbo].[Package] ([Id])
GO
ALTER TABLE [dbo].[PackageVersion] CHECK CONSTRAINT [FK_PackageVersion_Package_PackageId]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Account_AccountId]
GO
ALTER TABLE [dbo].[Variable]  WITH CHECK ADD  CONSTRAINT [FK_Variable_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Variable] CHECK CONSTRAINT [FK_Variable_Account_AccountId]
GO
ALTER TABLE [dbo].[VariableEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_VariableEnvironment_Account_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[VariableEnvironment] CHECK CONSTRAINT [FK_VariableEnvironment_Account_AccountId]
GO
ALTER TABLE [dbo].[VariableEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_VariableEnvironment_Environment_EnvironmentId] FOREIGN KEY([EnvironmentId])
REFERENCES [dbo].[Environment] ([Id])
GO
ALTER TABLE [dbo].[VariableEnvironment] CHECK CONSTRAINT [FK_VariableEnvironment_Environment_EnvironmentId]
GO
ALTER TABLE [dbo].[VariableEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_VariableEnvironment_Variable_VariableId] FOREIGN KEY([VariableId])
REFERENCES [dbo].[Variable] ([Id])
GO
ALTER TABLE [dbo].[VariableEnvironment] CHECK CONSTRAINT [FK_VariableEnvironment_Variable_VariableId]
GO