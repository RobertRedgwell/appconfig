﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace AppConfig.Manager.Migrations
{
    [Migration(1)]
    public class Baseline : Migration
    {
        public override void Up()
        {
            Execute.EmbeddedScript("Baseline.sql");
        }

        public override void Down()
        {
        }
    }
}