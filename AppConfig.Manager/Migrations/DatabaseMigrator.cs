﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using FluentMigrator;
using FluentMigrator.Infrastructure;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using ServiceStack.OrmLite;

namespace AppConfig.Manager.Migrations
{
    public static class DatabaseMigrator
    {
        //private const string DbConnectionString = @"Server=(LocalDB)\v11.0;Integrated Security=true;AttachDbFileName=|DataDirectory|\App_Data\MigrationTestDatabase.mdf";

        /// <summary>
        /// Migrates the database all the way to the latest version
        /// </summary>
        /// <param name="connectionString"></param>
        public static void MigrateDatabaseToLatest(string connectionString)
        {
            var factory = new SqlServer2012ProcessorFactoryEx();
            var runner = GetMigrationRunner(factory, connectionString);
            MigrateDatabaseForwardTo(runner);
            factory.Connection.Close();
        }

        /// <summary>
        /// Given a migration runner (such as that given by the GetMigrationRunner() method), migrates the database forward until the stopMigration function returns false
        /// </summary>
        /// <param name="runner"></param>
        /// <param name="stopMigration"></param>
        public static void MigrateDatabaseForwardTo(MigrationRunner runner,
            Func<IMigrationInfo, bool> stopMigration = null)
        {
            // migrate database to the level of one less than this test's migration
            var migrations = runner.MigrationLoader.LoadMigrations();

            try
            {
                foreach (var m in migrations.Values)
                {
                    if (stopMigration != null && stopMigration(m)) break;
                    runner.ApplyMigrationUp(m, false);
                }
            }
            catch (Exception e)
            {
                throw new MigrationException(e);
            }
        }

        public static MigrationRunner GetMigrationRunner(IMigrationProcessorFactory factory, string connectionString)
        {
            Announcer announcer = new TextWriterAnnouncer(s => {
                //System.IO.File.AppendAllText(@"c:\temp\out.sql", s); 
                System.Diagnostics.Debug.WriteLine(s);
            });
            announcer.ShowSql = true;

            var options = new ProcessorOptions
            {
                PreviewOnly = false,  // set to true to see the SQL
                Timeout = 60
            };

            var processor = factory.Create(connectionString, announcer, options);
            var runner = new MigrationRunner(
                typeof(Baseline).Assembly,
                new RunnerContext(announcer),
                processor);

            return runner;
        }    
    }
}