﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppConfig.Manager.Migrations
{
    public class MigrationException : ApplicationException
    {
        public MigrationException(Exception innerException)
            : base("An error occurred upgrading the database.", innerException)
        {
        }
    }
}
