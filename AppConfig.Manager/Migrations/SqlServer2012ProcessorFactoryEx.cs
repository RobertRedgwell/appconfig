﻿using System.Data;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Generators.SqlServer;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;
using ServiceStack.OrmLite;

namespace AppConfig.Manager.Migrations
{
    public class SqlServer2012ProcessorFactoryEx : MigrationProcessorFactory
    {
        public override IMigrationProcessor Create(string connectionString, IAnnouncer announcer, IMigrationProcessorOptions options)
        {
            OrmLiteConfig.DialectProvider = SqlServerDialect.Provider;
            var factory = new SqlServerDbFactory();
            Connection = factory.CreateConnection(connectionString);
            return new SqlServerProcessor(Connection, new SqlServer2012Generator(), announcer, options, factory);
        }

        public IDbConnection Connection { get; private set; }
    }
}