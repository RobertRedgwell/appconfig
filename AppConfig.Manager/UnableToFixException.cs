﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Manager
{
    public class UnableToFixException : ApplicationException
    {
        public UnableToFixException(string issue)
            : base(issue)
        {
        }
    }
}
