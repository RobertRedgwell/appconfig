﻿namespace AppConfig.Manager
{
    partial class Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Manager));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelDatabaseConnectionStatus = new System.Windows.Forms.Label();
            this.iconDatabaseInstalled = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.iconMasterConnectionString = new System.Windows.Forms.PictureBox();
            this.buttonVerifyMasterConnection = new System.Windows.Forms.Button();
            this.textBoxMasterConnectionString = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxConfigureWebserver = new System.Windows.Forms.GroupBox();
            this.iconWebserverConfigured = new System.Windows.Forms.PictureBox();
            this.buttonSetWebserver = new System.Windows.Forms.Button();
            this.textBoxVirtualDirectory = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxHostname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelWebserverNotInstalled = new System.Windows.Forms.Label();
            this.iconWebserverInstalled = new System.Windows.Forms.PictureBox();
            this.buttonFixWebserver = new System.Windows.Forms.Button();
            this.dbConnectionWorker = new System.ComponentModel.BackgroundWorker();
            this.dbMixedModeWorker = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconDatabaseInstalled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconMasterConnectionString)).BeginInit();
            this.groupBoxConfigureWebserver.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconWebserverConfigured)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconWebserverInstalled)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelDatabaseConnectionStatus);
            this.groupBox1.Controls.Add(this.iconDatabaseInstalled);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.iconMasterConnectionString);
            this.groupBox1.Controls.Add(this.buttonVerifyMasterConnection);
            this.groupBox1.Controls.Add(this.textBoxMasterConnectionString);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(633, 87);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configure Database";
            // 
            // labelDatabaseConnectionStatus
            // 
            this.labelDatabaseConnectionStatus.AutoSize = true;
            this.labelDatabaseConnectionStatus.Location = new System.Drawing.Point(139, 60);
            this.labelDatabaseConnectionStatus.Name = "labelDatabaseConnectionStatus";
            this.labelDatabaseConnectionStatus.Size = new System.Drawing.Size(70, 13);
            this.labelDatabaseConnectionStatus.TabIndex = 11;
            this.labelDatabaseConnectionStatus.Text = "Connecting...";
            this.labelDatabaseConnectionStatus.Visible = false;
            // 
            // iconDatabaseInstalled
            // 
            this.iconDatabaseInstalled.Image = ((System.Drawing.Image)(resources.GetObject("iconDatabaseInstalled.Image")));
            this.iconDatabaseInstalled.InitialImage = null;
            this.iconDatabaseInstalled.Location = new System.Drawing.Point(604, 56);
            this.iconDatabaseInstalled.Name = "iconDatabaseInstalled";
            this.iconDatabaseInstalled.Size = new System.Drawing.Size(23, 23);
            this.iconDatabaseInstalled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconDatabaseInstalled.TabIndex = 10;
            this.iconDatabaseInstalled.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(46, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Database status:";
            // 
            // iconMasterConnectionString
            // 
            this.iconMasterConnectionString.Image = ((System.Drawing.Image)(resources.GetObject("iconMasterConnectionString.Image")));
            this.iconMasterConnectionString.InitialImage = null;
            this.iconMasterConnectionString.Location = new System.Drawing.Point(604, 27);
            this.iconMasterConnectionString.Name = "iconMasterConnectionString";
            this.iconMasterConnectionString.Size = new System.Drawing.Size(23, 23);
            this.iconMasterConnectionString.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconMasterConnectionString.TabIndex = 7;
            this.iconMasterConnectionString.TabStop = false;
            // 
            // buttonVerifyMasterConnection
            // 
            this.buttonVerifyMasterConnection.Location = new System.Drawing.Point(530, 27);
            this.buttonVerifyMasterConnection.Name = "buttonVerifyMasterConnection";
            this.buttonVerifyMasterConnection.Size = new System.Drawing.Size(63, 23);
            this.buttonVerifyMasterConnection.TabIndex = 4;
            this.buttonVerifyMasterConnection.Text = "Verify";
            this.buttonVerifyMasterConnection.UseVisualStyleBackColor = true;
            this.buttonVerifyMasterConnection.Click += new System.EventHandler(this.buttonVerifyMasterConnection_Click);
            // 
            // textBoxMasterConnectionString
            // 
            this.textBoxMasterConnectionString.Location = new System.Drawing.Point(139, 30);
            this.textBoxMasterConnectionString.Name = "textBoxMasterConnectionString";
            this.textBoxMasterConnectionString.Size = new System.Drawing.Size(385, 20);
            this.textBoxMasterConnectionString.TabIndex = 1;
            this.textBoxMasterConnectionString.TextChanged += new System.EventHandler(this.textBoxMasterConnectionString_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Master connection string:";
            // 
            // groupBoxConfigureWebserver
            // 
            this.groupBoxConfigureWebserver.Controls.Add(this.iconWebserverConfigured);
            this.groupBoxConfigureWebserver.Controls.Add(this.buttonSetWebserver);
            this.groupBoxConfigureWebserver.Controls.Add(this.textBoxVirtualDirectory);
            this.groupBoxConfigureWebserver.Controls.Add(this.label6);
            this.groupBoxConfigureWebserver.Controls.Add(this.textBoxPort);
            this.groupBoxConfigureWebserver.Controls.Add(this.label5);
            this.groupBoxConfigureWebserver.Controls.Add(this.textBoxHostname);
            this.groupBoxConfigureWebserver.Controls.Add(this.label4);
            this.groupBoxConfigureWebserver.Location = new System.Drawing.Point(13, 201);
            this.groupBoxConfigureWebserver.Name = "groupBoxConfigureWebserver";
            this.groupBoxConfigureWebserver.Size = new System.Drawing.Size(633, 86);
            this.groupBoxConfigureWebserver.TabIndex = 1;
            this.groupBoxConfigureWebserver.TabStop = false;
            this.groupBoxConfigureWebserver.Text = "Configure Webserver";
            // 
            // iconWebserverConfigured
            // 
            this.iconWebserverConfigured.Image = ((System.Drawing.Image)(resources.GetObject("iconWebserverConfigured.Image")));
            this.iconWebserverConfigured.InitialImage = null;
            this.iconWebserverConfigured.Location = new System.Drawing.Point(604, 26);
            this.iconWebserverConfigured.Name = "iconWebserverConfigured";
            this.iconWebserverConfigured.Size = new System.Drawing.Size(23, 23);
            this.iconWebserverConfigured.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconWebserverConfigured.TabIndex = 13;
            this.iconWebserverConfigured.TabStop = false;
            // 
            // buttonSetWebserver
            // 
            this.buttonSetWebserver.Location = new System.Drawing.Point(530, 26);
            this.buttonSetWebserver.Name = "buttonSetWebserver";
            this.buttonSetWebserver.Size = new System.Drawing.Size(63, 23);
            this.buttonSetWebserver.TabIndex = 8;
            this.buttonSetWebserver.Text = "Set";
            this.buttonSetWebserver.UseVisualStyleBackColor = true;
            this.buttonSetWebserver.Click += new System.EventHandler(this.buttonSetWebserver_Click);
            // 
            // textBoxVirtualDirectory
            // 
            this.textBoxVirtualDirectory.Location = new System.Drawing.Point(407, 29);
            this.textBoxVirtualDirectory.Name = "textBoxVirtualDirectory";
            this.textBoxVirtualDirectory.Size = new System.Drawing.Size(117, 20);
            this.textBoxVirtualDirectory.TabIndex = 7;
            this.textBoxVirtualDirectory.TextChanged += new System.EventHandler(this.textBoxWebsiteConfigTextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(319, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Virtual directory:";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(258, 29);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(50, 20);
            this.textBoxPort.TabIndex = 5;
            this.textBoxPort.Text = "80";
            this.textBoxPort.TextChanged += new System.EventHandler(this.textBoxWebsiteConfigTextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(223, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Port:";
            // 
            // textBoxHostname
            // 
            this.textBoxHostname.Location = new System.Drawing.Point(71, 29);
            this.textBoxHostname.Name = "textBoxHostname";
            this.textBoxHostname.Size = new System.Drawing.Size(144, 20);
            this.textBoxHostname.TabIndex = 3;
            this.textBoxHostname.Text = "*";
            this.textBoxHostname.TextChanged += new System.EventHandler(this.textBoxWebsiteConfigTextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Hostname:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(385, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Before using ZapConfig, you will need to configure the database and webserver:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelWebserverNotInstalled);
            this.groupBox3.Controls.Add(this.iconWebserverInstalled);
            this.groupBox3.Controls.Add(this.buttonFixWebserver);
            this.groupBox3.Location = new System.Drawing.Point(13, 134);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(633, 61);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Install Webserver";
            // 
            // labelWebserverNotInstalled
            // 
            this.labelWebserverNotInstalled.AutoSize = true;
            this.labelWebserverNotInstalled.Location = new System.Drawing.Point(13, 28);
            this.labelWebserverNotInstalled.Name = "labelWebserverNotInstalled";
            this.labelWebserverNotInstalled.Size = new System.Drawing.Size(309, 13);
            this.labelWebserverNotInstalled.TabIndex = 13;
            this.labelWebserverNotInstalled.Text = "IIS was not found on this machine. Click Install to setup IIS now.";
            // 
            // iconWebserverInstalled
            // 
            this.iconWebserverInstalled.Image = ((System.Drawing.Image)(resources.GetObject("iconWebserverInstalled.Image")));
            this.iconWebserverInstalled.InitialImage = null;
            this.iconWebserverInstalled.Location = new System.Drawing.Point(604, 19);
            this.iconWebserverInstalled.Name = "iconWebserverInstalled";
            this.iconWebserverInstalled.Size = new System.Drawing.Size(23, 23);
            this.iconWebserverInstalled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconWebserverInstalled.TabIndex = 12;
            this.iconWebserverInstalled.TabStop = false;
            // 
            // buttonFixWebserver
            // 
            this.buttonFixWebserver.Location = new System.Drawing.Point(530, 19);
            this.buttonFixWebserver.Name = "buttonFixWebserver";
            this.buttonFixWebserver.Size = new System.Drawing.Size(63, 23);
            this.buttonFixWebserver.TabIndex = 7;
            this.buttonFixWebserver.Text = "Fix";
            this.buttonFixWebserver.UseVisualStyleBackColor = true;
            this.buttonFixWebserver.Click += new System.EventHandler(this.buttonFixWebserver_Click);
            // 
            // Manager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 303);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBoxConfigureWebserver);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Manager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage ZapConfig";
            this.Load += new System.EventHandler(this.Manager_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconDatabaseInstalled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconMasterConnectionString)).EndInit();
            this.groupBoxConfigureWebserver.ResumeLayout(false);
            this.groupBoxConfigureWebserver.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconWebserverConfigured)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconWebserverInstalled)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxConfigureWebserver;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxMasterConnectionString;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonVerifyMasterConnection;
        private System.Windows.Forms.TextBox textBoxHostname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxVirtualDirectory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonSetWebserver;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonFixWebserver;
        private System.Windows.Forms.PictureBox iconMasterConnectionString;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox iconDatabaseInstalled;
        private System.Windows.Forms.Label labelDatabaseConnectionStatus;
        private System.Windows.Forms.PictureBox iconWebserverInstalled;
        private System.Windows.Forms.PictureBox iconWebserverConfigured;
        private System.Windows.Forms.Label labelWebserverNotInstalled;
        private System.ComponentModel.BackgroundWorker dbConnectionWorker;
        private System.ComponentModel.BackgroundWorker dbMixedModeWorker;
    }
}

