﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Core
{
    public enum ELicenseStatus
    {
        Unknown = 0,
        Unlicensed = 1,
        MismatchedLicense = 2,
        TrialLicensed = 3,
        Licensed = 10,
        LicensedOldVersion = 11,
    }
}
