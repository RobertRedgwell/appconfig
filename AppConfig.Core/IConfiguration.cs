﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace AppConfig.Core
{
    public interface IConfiguration
    {
        NameValueCollection AppSettings { get; set; }

        NameValueCollection ConnectionStrings { get; set; }
    }
}
