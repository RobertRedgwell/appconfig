﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppConfig.Core
{
    public class LicenseQueryResponseDto
    {
        public string ValidationResponse { get; set; }
        public string NotificationMessage { get; set; }
    }
}