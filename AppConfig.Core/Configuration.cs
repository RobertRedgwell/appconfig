﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AppConfig.Core
{
    public class Configuration : IConfiguration
    {
        public Configuration()
        {
            AppSettings = ConfigurationManager.AppSettings;
            ConnectionStrings = new NameValueCollection();
            foreach (ConnectionStringSettings c in ConfigurationManager.ConnectionStrings)
            {
                ConnectionStrings.Add(c.Name, c.ConnectionString);
            }
        }

        public NameValueCollection AppSettings { get; set; }

        public NameValueCollection ConnectionStrings { get; set; }
    }
}