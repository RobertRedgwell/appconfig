﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppConfig.Core
{
    public class LicenseQueryDto
    {
        public string MachineId { get; set; }
        public string License { get; set; }
        public string Version { get; set; }
        public string RequestId { get; set; }
    }
}