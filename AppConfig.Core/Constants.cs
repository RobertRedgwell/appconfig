﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Core
{
    public class Constants
    {
        public const string SemanticVersionRegex = @"^\d+(\.\d+){1,2}(-?([0-9a-zA-Z]))*";

        public const string EmailRegex = @".*@.*\..*";

        public static string GetHashForStatus(ELicenseStatus status, string requestId, string machineId)
        {
            return Convert.ToBase64String(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(requestId + (int)status + machineId)));
        }
    }
}
