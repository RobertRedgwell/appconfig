﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace AppConfig.Model
{
    public class PackageVersion : BaseAccountModel
    {
        [References(typeof(Package))]
        public long PackageId { get; set; }

        public string SemanticVersion { get; set; }

        public string Transform { get; set; }
    }
}
