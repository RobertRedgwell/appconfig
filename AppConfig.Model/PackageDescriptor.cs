﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;

namespace AppConfig.Model
{
    public class PackageDescriptor
    {
        /// <summary>
        /// The fully qualified package name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The (semantic) version of the package
        /// </summary>
        [Required]
        [RegularExpression(Constants.SemanticVersionRegex, ErrorMessage = "Version must be a semantic version string")]
        public string Version { get; set; }
    }
}
