﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using AppConfig.Core;
using ServiceStack.Text;

namespace AppConfig.Model
{
    public class Account : BaseModel
    {
        public string Name { get; set; }
        public string ApiKey { get; set; }
        public string License { get; set; }
        public EAccountType AccountType { get; set; }
        public string ValidationString { get; set; }
        public bool HasBeenValidated { get; set; }

        public void SetLicenseData(string license, DateTime lastCheck, ELicenseStatus status, bool lockDown)
        {
            var data = new LicenseCheckData
            {
                License = license,
                LastCheck = lastCheck,
                LastLicenseStatus = status,
                LockedDown = lockDown
            };

            License = Encrypt(data.ToJson(), ValidationString + ValidationSuffix);
        }

        /// <summary>
        /// Sets just the license key component of the license
        /// </summary>
        /// <param name="license"></param>
        public void SetLicenseKey(string license)
        {
            var data = GetLicenseData() ?? new LicenseCheckData();
            data.License = license;
            License = Encrypt(data.ToJson(), ValidationString + ValidationSuffix);
        }

        public LicenseCheckData GetLicenseData()
        {
            var decryptedLicense = License == null ? null : Decrypt(License, ValidationString + ValidationSuffix);
            return string.IsNullOrEmpty(decryptedLicense) 
                ? null
                : JsonSerializer.DeserializeFromString<LicenseCheckData>(decryptedLicense);
        }

        public bool IsLockedDown()
        {
            var data = GetLicenseData();
            return data == null || data.LockedDown;
        }

        public class LicenseCheckData
        {
            public LicenseCheckData()
            {
                LastCheck = DateTime.Now;
                LastLicenseStatus = ELicenseStatus.Unknown;
                LockedDown = false;
            }

            public string License { get; set; }
            public DateTime LastCheck { get; set; }
            public ELicenseStatus LastLicenseStatus { get; set; }
            public bool LockedDown { get; set; }
        }

        private static readonly string ValidationSuffix = "dfhjikol54h890wejg2j034g980ve";

        private static string Decrypt(string encryptedText, string key)
        {
            return StringCipher.Decrypt(encryptedText, key);
        }

        public static string Encrypt(string stringToEncrypt, string key)
        {
            return StringCipher.Encrypt(stringToEncrypt, key);
        }

        public static class StringCipher
        {
            // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
            // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
            // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
            private const string initVector = "23890jewfgq89034";

            // This constant is used to determine the keysize of the encryption algorithm.
            private const int keysize = 256;

            public static string Encrypt(string plainText, string passPhrase)
            {
                try 
                {
                    byte[] initVectorBytes = Encoding.UTF8.GetBytes(initVector);
                    byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                    PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
                    byte[] keyBytes = password.GetBytes(keysize / 8);
                    RijndaelManaged symmetricKey = new RijndaelManaged();
                    symmetricKey.Mode = CipherMode.CBC;
                    ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
                    MemoryStream memoryStream = new MemoryStream();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    byte[] cipherTextBytes = memoryStream.ToArray();
                    memoryStream.Close();
                    cryptoStream.Close();
                    return Convert.ToBase64String(cipherTextBytes);
                }
                catch (Exception e)
                {
                    return null;
                }
            }

            public static string Decrypt(string cipherText, string passPhrase)
            {
                try
                {
                    byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                    byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
                    PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
                    byte[] keyBytes = password.GetBytes(keysize / 8);
                    RijndaelManaged symmetricKey = new RijndaelManaged();
                    symmetricKey.Mode = CipherMode.CBC;
                    ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
                    MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                    byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                    memoryStream.Close();
                    cryptoStream.Close();
                    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }
    }
}
