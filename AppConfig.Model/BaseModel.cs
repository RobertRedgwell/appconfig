﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;
using ServiceStack.DesignPatterns.Model;

namespace AppConfig.Model
{
    public abstract class BaseModel : IHasId<long>
    {
        [PrimaryKey]
        [AutoIncrement]
        public long Id { get; set; }
    }
}
