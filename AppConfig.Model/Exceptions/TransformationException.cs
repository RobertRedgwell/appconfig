﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model.Exceptions
{
    public class TransformationException : ArgumentException
    {
        public TransformationException(PackageDescriptor package)
            : base(package == null ? "" : string.Format("Transformation failed when applying transform for package '{0}' with version '{1}'", package.Name, package.Version))
        {
        }
    }
}
