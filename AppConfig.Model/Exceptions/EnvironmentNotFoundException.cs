﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model.Exceptions
{
    public class EnvironmentNotFoundException : ArgumentException
    {
        public EnvironmentNotFoundException(string environmentName)
            : base(string.Format("No environment with the name '{0}' was found", environmentName))
        {            
        }
    }
}
