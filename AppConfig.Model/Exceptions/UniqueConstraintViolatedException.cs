﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AppConfig.Model.Exceptions
{
    public class UniqueConstraintViolatedException : ArgumentException 
    {
        private const string DefaultMessage = "Value must be unique";

        private UniqueConstraintViolatedException(string message, SqlException e)
            : base(message/*, e*/)
        {
        }

        public UniqueConstraintViolatedException(SqlException e)
            : base(DefaultMessage/*, e*/)
        {
        }

        public static UniqueConstraintViolatedException FromSqlException(SqlException e)
        {
            var message = DefaultMessage;
            var match = Regex.Match(e.Message, @"uidx_.*_(\w*)'");
            if (match.Groups.Count > 1)
            {
                message = string.Format("An item already exists with that {0}", match.Groups[1].Value);
            }

            return new UniqueConstraintViolatedException(message, e);
        }
    }
}
