﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace AppConfig.Model
{
    public class VariableEnvironment : BaseAccountModel
    {
        [References(typeof(Variable))]
        public long VariableId { get; set; }

        [References(typeof(Environment))]
        public long EnvironmentId { get; set; }

        public string Value { get; set; }

        public bool IsHidden { get; set; }
    }
}
