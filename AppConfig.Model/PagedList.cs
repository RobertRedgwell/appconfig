﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model
{
    public class PagedList<T>
    {
        public List<T> List { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public long TotalItems { get; set; }
    }
}
