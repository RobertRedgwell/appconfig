﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuGet;

namespace AppConfig.Model
{
    public class SemanticVersionComparer : Comparer<SemanticVersion>
    {
        public override int Compare(SemanticVersion x, SemanticVersion y)
        {
            if (y == null) return 1;
            if (x == null) return -1;
            return x.CompareTo(y);
        }
    }
}
