﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model
{
    public class VariableForEnvironment : Variable
    {
        /// <summary>
        /// The value for the environment this was generated for
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// The environment this was generated for
        /// </summary>
        public long EnvironmentId { get; set; }
    }
}
