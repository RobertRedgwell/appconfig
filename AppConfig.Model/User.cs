﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model
{
    public class User : BaseAccountModel
    {
        public User() {}
        public User(long accountId, string emailAddress, string password)
        {
            AccountId = accountId;
            EmailAddress = emailAddress;
            Salt = GenerateUserSalt();
            Password = HashPassword(password);
        }

        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public DateTime? LastLogin { get; set; }

        public string HashPassword(string plainTextPassword)
        {
            var hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes((plainTextPassword + Salt)));
            return Convert.ToBase64String(hash);
        }

        public static string GenerateUserSalt()
        {
            return Convert.ToBase64String(
                MD5.Create().ComputeHash(
                    Guid.NewGuid().ToByteArray())
            );
        }
    }
}
