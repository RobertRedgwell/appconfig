﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace AppConfig.Model
{
    public class Package : BaseAccountModel
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string Description { get; set; }
        public string Transform { get; set; }
        public bool Deleted { get; set; }
    }
}
