﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model
{
    public class Group : BaseAccountModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public bool CanRead { get; set; }
        public bool CanWrite { get; set; }
        public bool CanAdmin { get; set; }
    }
}
