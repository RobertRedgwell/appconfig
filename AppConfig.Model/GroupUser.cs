﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace AppConfig.Model
{
    public class GroupUser : BaseAccountModel
    {
        [References(typeof(User))]
        public long UserId { get; set; }

        [References(typeof(Group))]
        public long GroupId { get; set; }

    }
}
