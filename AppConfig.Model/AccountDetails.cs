﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model
{
    public class AccountDetails
    {
        public AccountDetails(Account account, int userCount, int environmentCount, int variableCount, int packageCount)
        {
            Account = account;
            UserCount = userCount;
            EnvironmentCount = environmentCount;
            VariableCount = variableCount;
            PackageCount = packageCount;
        }

        public Account Account { get; private set; }

        public int UserCount { get; private set; }

        public int EnvironmentCount { get; private set; }

        public int VariableCount { get; private set; }

        public int PackageCount { get; private set; } 
    }
}
