﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using ServiceStack.DataAnnotations;

namespace AppConfig.Model
{
    public class AuditEntry : BaseAccountModel
    {
        public EActionType ActionType { get; set; }

        public string Message { get; set; }

        public DateTime When { get; set; }

        [References(typeof(User))]
        public long? UserId { get; set; }

        public static AuditEntry CreatedEnvironment(Environment environment, long? userId)
        {
            if (environment == null) throw new ArgumentNullException("environment");

            return CreateAuditEntry(string.Format("Environment '{0}' created.", environment.Name), EActionType.CreatedEnvironment, environment.AccountId, userId);
        }

        public static AuditEntry UpdatedEnvironment(Environment environment, long? userId)
        {
            if (environment == null) throw new ArgumentNullException("environment");

            return CreateAuditEntry(string.Format("Environment '{0}' updated.", environment.Name), EActionType.UpdatedEnvironment, environment.AccountId, userId);
        }

        public static AuditEntry DeletedEnvironment(Environment environment, long? userId)
        {
            if (environment == null) throw new ArgumentNullException("environment");

            return CreateAuditEntry(string.Format("Environment '{0}' deleted.", environment.Name), EActionType.DeletedEnvironment, environment.AccountId, userId);
        }

        public static AuditEntry CreatedVariable(Variable variable, long? userId)
        {
            if (variable == null) throw new ArgumentNullException("variable");

            return CreateAuditEntry(string.Format("Variable '{0}' created.", variable.Name), EActionType.CreatedVariable, variable.AccountId, userId);
        }

        public static AuditEntry UpdatedVariable(Variable variable, long? userId)
        {
            if (variable == null) throw new ArgumentNullException("variable");

            return CreateAuditEntry(string.Format("Variable '{0}' updated.", variable.Name), EActionType.UpdatedVariable, variable.AccountId, userId);
        }

        public static AuditEntry DeletedVariable(Variable variable, long? userId)
        {
            if (variable == null) throw new ArgumentNullException("variable");

            return CreateAuditEntry(string.Format("Variable '{0}' deleted.", variable.Name), EActionType.DeletedVariable, variable.AccountId, userId);
        }

        public static AuditEntry CreatedPackage(Package package, long? userId)
        {
            if (package == null) throw new ArgumentNullException("package");

            return CreateAuditEntry(string.Format("Package '{0} {1}' created.", package.Name, package.Version), EActionType.CreatedPackage, package.AccountId, userId);
        }

        public static AuditEntry UpdatedPackage(Package package, long? userId)
        {
            if (package == null) throw new ArgumentNullException("package");

            return CreateAuditEntry(string.Format("Package '{0} {1}' updated.", package.Name, package.Version), EActionType.UpdatedPackage, package.AccountId, userId);
        }

        public static AuditEntry DeletedPackage(Package package, long? userId)
        {
            if (package == null) throw new ArgumentNullException("package");

            return CreateAuditEntry(string.Format("Package '{0} {1}' deleted.", package.Name, package.Version), EActionType.DeletedPackage, package.AccountId, userId);
        }

        public static AuditEntry CreatedUser(User user, long? performingUserId)
        {
            if (user == null) throw new ArgumentNullException("user");

            return CreateAuditEntry(string.Format("User '{0}' created.", user.EmailAddress), EActionType.CreatedUser, user.AccountId, performingUserId);
        }

        public static AuditEntry UpdatedUser(User user, long? performingUserId)
        {
            if (user == null) throw new ArgumentNullException("user");

            return CreateAuditEntry(string.Format("User '{0}' updated.", user.EmailAddress), EActionType.UpdatedUser, user.AccountId, performingUserId);
        }

        public static AuditEntry DeletedUser(User user, long? performingUserId)
        {
            if (user == null) throw new ArgumentNullException("user");

            return CreateAuditEntry(string.Format("User '{0}' deleted.", user.EmailAddress), EActionType.DeletedUser, user.AccountId, performingUserId);
        }

        public static AuditEntry TransformPerformed(Package package, Environment environment, long? userId)
        {
            if (package == null) throw new ArgumentNullException("package");
            if (environment == null) throw new ArgumentNullException("environment");

            return CreateAuditEntry(
                string.Format("Package transform performed for package '{0} {1}' and environment '{2}'.", package.Name, package.Version, environment.Name), 
                EActionType.TransformPerformed, 
                package.AccountId, 
                userId);
        }

        public static AuditEntry CreatedAccount(Account account)
        {
            if (account == null) throw new ArgumentNullException("account");

            return CreateAuditEntry(string.Format("Your account has been created. Please check your email for a confirmation link to ensure your account stays operational."), EActionType.CreatedAccount, account.Id, null);
        }

        public static AuditEntry AccountValidated(Account account)
        {
            if (account == null) throw new ArgumentNullException("account");

            return CreateAuditEntry("Your account has been successfully validated.", EActionType.ValidatedAccount, account.Id, null);
        }

        public static AuditEntry ApiKeyChanged(Account account, long? performingUserId)
        {
            if (account == null) throw new ArgumentNullException("account");

            return CreateAuditEntry("Apikey updated for account.", EActionType.ApiKeyChanged, account.Id, performingUserId);
        }

        public static AuditEntry AccountLicensingCheckFailed(long accountId, string responseMessage)
        {
            if (!string.IsNullOrEmpty(responseMessage)) {
                responseMessage = string.Format("The related error message was '{0}'.", responseMessage);
            }
            return CreateAuditEntry(string.Format("An error occurred checking for a valid license. Please contact us at ZapConfig.com to report this issue. {0}", responseMessage), 
                EActionType.LicenseCheckFailed, 
                accountId, 
                null);
        }

        public static AuditEntry AccountLicensingCheckSucceeded(long accountId, ELicenseStatus status)
        {
            return CreateAuditEntry(string.Format("License check succeeded. Your license status is: {0}", status), 
                EActionType.LicenseCheckSucceeded, 
                accountId, 
                null);
        }


        private static AuditEntry CreateAuditEntry(string message, EActionType actionType, long accountId, long? userId)
        {
            return new AuditEntry
            {
                AccountId = accountId,
                ActionType = actionType,
                Message = message,
                UserId = userId,
                When = DateTime.Now,
            };
        }
    }
}
