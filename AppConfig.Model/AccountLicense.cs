﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.DataAnnotations;
using AppConfig.Core;

namespace AppConfig.Model
{
    public class AccountLicense : BaseModel
    {
        public string MachineId { get; set; }

        public DateTime WhenCreated { get; set; }

        public DateTime WhenUpdated { get; set; }

        public ELicenseStatus LicenseStatus { get; set; }

        public string License { get; set; }

        public string Version { get; set; }

        public string LastRequestIp { get; set; }

        public DateTime Expiry { get; set; }
    }
}