﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace AppConfig.Model
{
    public class Variable : BaseAccountModel
    {
        [Index(Unique = true)]
        public string Name { get; set; }
        public string Description { get; set; }
        public string DefaultValue { get; set; }
        public bool IsRuntimeAccessible { get; set; }
    }
}
