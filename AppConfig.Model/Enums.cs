﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Model
{
    public enum EAccountType
    {
        Unknown = 0,
        Shock = 1,
        Bolt = 2,
        Storm = 3,
    }

    public enum EActionType
    {
        Unknown = 0,
        CreatedEnvironment = 1,
        UpdatedEnvironment = 2,
        DeletedEnvironment = 3,
        CreatedVariable = 4,
        UpdatedVariable = 5,
        DeletedVariable = 6,
        CreatedPackage = 7,
        UpdatedPackage = 8,
        DeletedPackage = 9,
        CreatedUser = 10,
        UpdatedUser = 11,
        DeletedUser = 12,
        TransformPerformed = 13,
        ApiKeyChanged = 14,
        CreatedAccount = 15,
        ValidatedAccount = 16,
        LicenseCheckFailed = 17,
        LicenseCheckSucceeded = 18,
    }
}
