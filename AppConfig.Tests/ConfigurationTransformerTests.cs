﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;
using AppConfig.Model.Exceptions;
using AppConfig.Persistence;
using AppConfig.Persistence.Interfaces;
using Moq;
using NuGet;
using NUnit.Framework;

namespace AppConfig.Tests
{
    [TestFixture]
    public class ConfigurationTransformerTests
    {
        private const string _validXml = "<?xml version='1.0' encoding='utf-8'?>" +
                "<configuration>" +
                    "<appSettings>" +
                        @"<add key='LogFolder' value='C:\Somewhere' />" +
                        @"<add key='SleepPeriod' value='300' />" +
                    "</appSettings>" +
                "</configuration>";
       
        [Test]
        public void DoTransformation_GivenNullCorePackage_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() =>
                CreateTransformer().DoTransformation(123, null, new List<PackageDescriptor>(), "Production", "<xml/>"));
        }

        [Test]
        [TestCase(null, typeof(ArgumentNullException))]
        [TestCase("", typeof(EnvironmentNotFoundException))]
        public void DoTransformation_GivenInvalidEnvironmentName_ThrowsArgumentException(string environmentName, Type exceptionType)
        {
            Assert.Throws(exceptionType, () =>
                CreateTransformer().DoTransformation(123, new PackageDescriptor { Name = "abc123", Version = "1.2.3" }, new List<PackageDescriptor>(), environmentName, _validXml));
        }

        [Test]
        [TestCase(null, typeof(ArgumentNullException))]
        [TestCase("", typeof(ArgumentException))]
        [TestCase("not valid xml", typeof(ArgumentException))]
        public void DoTransformation_GivenInvalidInputConfig_ThrowsArgumentException(string xml, Type exceptionType)
        {
            var environmentName = "Production";
            long accountId = 123;
            var mockEnvironmentRepo = new Mock<IEnvironmentRepo>();
            mockEnvironmentRepo.Setup(x => x.GetByNameForAccount(environmentName, accountId)).Returns(new Model.Environment());

            Assert.Throws(exceptionType, () =>
                CreateTransformer(mockEnvironmentRepo.Object)
                    .DoTransformation(accountId, new PackageDescriptor { Name = "abc123", Version = "1.2.3" }, new List<PackageDescriptor>(), environmentName, xml));
        }

        public void DoTransformation_GivenValidArguments_TransformsAsExpected()
        {
            long accountId = 123;
            var environment = new Model.Environment 
            {
                Name = "Production",
                Id = 456,
                AccountId = accountId,
            };
            
            var variables = new List<VariableForEnvironment>();
            variables.Add(new VariableForEnvironment{ AccountId = accountId, DefaultValue = "d:\\Some\\Other\\Place", EnvironmentId = environment.Id, Name = "FirstVariable" });
            variables.Add(new VariableForEnvironment{ AccountId = accountId, DefaultValue = "20", EnvironmentId = environment.Id, Name = "SecondVariable", Value = "30" });

            var transform = "<?xml version='1.0' encoding='utf-8'?>" +
                            "<configuration>" +
                                "<appSettings>" +
                                    @"<add key='LogFolder' value='${FirstVariable}' xdt:Transform='SetAttributes' xdt:Locator='Match(name)' />" +
                                    @"<add key='SleepPeriod' value='${SecondVariable}' xdt:Transform='SetAttributes' xdt:Locator='Match(name)' />" +
                                "</appSettings>" +
                            "</configuration>";

            var packageDescriptor = new PackageDescriptor { Name = "abc123", Version = "1.2.3" };
            var package = new Package { Transform = transform };
            
            var mockEnvironmentRepo = new Mock<IEnvironmentRepo>();
            mockEnvironmentRepo.Setup(x => x.GetByNameForAccount(environment.Name, accountId))
                .Returns(environment)
                .Verifiable();

            var mockVariableRepo = new Mock<IVariableRepo>();
            mockVariableRepo.Setup(x => x.GetVariableValuesForEnvironment(environment.Id, accountId))
                .Returns(variables)
                .Verifiable();

            var mockPackageRepo = new Mock<IPackageRepo>();
            mockPackageRepo.Setup(x => x.GetByNameAndVersionForAccount(packageDescriptor.Name, new SemanticVersion(packageDescriptor.Version), accountId))
                .Returns(package)
                .Verifiable();

            var resultXml = CreateTransformer(mockEnvironmentRepo.Object, mockPackageRepo.Object, mockVariableRepo.Object)
                    .DoTransformation(accountId, packageDescriptor, new List<PackageDescriptor>(), environment.Name, _validXml);

            var expectedResultXml = "<?xml version='1.0' encoding='utf-8'?>" +
                            "<configuration>" +
                                "<appSettings>" +
                                    @"<add key='LogFolder' value='" + variables[0].DefaultValue + "' />" +
                                    @"<add key='SleepPeriod' value='" + variables[1].Value + "' />" +
                                "</appSettings>" +
                            "</configuration>";

            Assert.AreEqual(expectedResultXml, resultXml);
            mockEnvironmentRepo.Verify();
            mockVariableRepo.Verify();
            mockPackageRepo.Verify();
        }

        private ConfigurationTransformer CreateTransformer(IEnvironmentRepo environmentRepo = null, IPackageRepo packageRepo = null, IVariableRepo variableRepo = null)
        {
            return new ConfigurationTransformer(
                environmentRepo ?? new Mock<IEnvironmentRepo>().Object,
                packageRepo ?? new Mock<IPackageRepo>().Object,
                variableRepo ?? new Mock<IVariableRepo>().Object,
                new Mock<IAuditEntryRepo>().Object
            );
        }
    }
}
