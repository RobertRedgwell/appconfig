﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;
using AppConfig.Model;
using ServiceStack.OrmLite;
using System.Data;

namespace AppConfig.Persistence
{
    public class VariableRepo : BaseAccountRepo<Variable>, IVariableRepo
    {
        private readonly IVariableEnvironmentRepo _variableEnvironmentRepo;
        private readonly IPackageRepo _packageRepo;
        private readonly IAuditEntryRepo _auditEntryRepo;

        public VariableRepo(IConfiguration configuration, IVariableEnvironmentRepo variableEnvironmentRepo, IPackageRepo packageRepo, IAuditEntryRepo auditEntryRepo)
            : base(configuration)
        {
            _variableEnvironmentRepo = variableEnvironmentRepo;
            _packageRepo = packageRepo;
            _auditEntryRepo = auditEntryRepo;
        }

        internal override void Save(IDbConnection c, Variable model, long? userId)
        {
            using (var t = c.BeginTransaction())
            {
                if (model.Id > 0) // if this is not a new model
                {
                    var existingModel = GetById(model.Id);
                    // If this is a name change
                    if (existingModel != null && existingModel.Name != model.Name)
                    {
                        // update any packages that are using this variable too
                        _packageRepo.UpdateVariableName(existingModel.Name, model.Name, model.AccountId);
                    }
                }
                base.Save(c, model, userId);
                t.Commit();
            }
        }
        

        public override bool DeleteForAccount(long id, long accountId, long userId)
        {
            _variableEnvironmentRepo.DeleteForVariable(id, accountId, userId);
            return base.DeleteForAccount(id, accountId, userId);
        }

        public override bool DeleteForAccount(Variable model, long accountId, long userId)
        {
            _variableEnvironmentRepo.DeleteForVariable(model.Id, accountId, userId);
            return base.DeleteForAccount(model, accountId, userId);
        }

        public List<VariableForEnvironment> GetVariableValuesForEnvironment(long environmentId, long accountId)
        {
            using (var c = GetConnection())
            {
                return c.Select<VariableForEnvironment>(@"SELECT * FROM Variable v 
                    LEFT OUTER JOIN VariableEnvironment ve ON v.Id = ve.VariableId AND ve.EnvironmentId = {0}
                    WHERE v.AccountId = {1}", environmentId, accountId);
            }
        }

        protected override void OnCreated(Variable model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.CreatedVariable(model, userId), userId);
        }

        protected override void OnUpdated(Variable model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.UpdatedVariable(model, userId), userId);
        }

        protected override void OnDeleted(Variable model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.DeletedVariable(model, userId), userId);
        }

    }
}
