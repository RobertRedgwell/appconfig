﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;
using ServiceStack.OrmLite;

namespace AppConfig.Persistence
{
    public class AccountRepo : BaseRepo<Account>, IAccountRepo
    {
        private readonly IAuditEntryRepo _auditEntryRepo;

        public AccountRepo(IConfiguration configuration, IAuditEntryRepo auditEntryRepo)
            : base(configuration)
        {
            _auditEntryRepo = auditEntryRepo;
        }

        public void InitDatabase(string apiKey, string validationString)
        {
            using (var c = GetConnection())
            {
                c.DropTable<AuditEntry>();
                c.DropTable<GroupUser>();
                c.DropTable<Group>();
                c.DropTable<User>();
                c.DropTable<PackageVersion>();
                c.DropTable<Package>();
                c.DropTable<VariableEnvironment>();
                c.DropTable<Variable>();
                c.DropTable<AppConfig.Model.Environment>();
                c.DropTable<Account>();
                c.DropTable<AccountLicense>();
            }
            using (var c = GetConnection()) 
            {
                c.CreateTable<AccountLicense>();
                c.CreateTable<Account>();
                c.CreateTable<User>();
                c.CreateTable<Group>();
                c.CreateTable<GroupUser>();
                c.CreateTable<Package>();
                c.CreateTable<AppConfig.Model.Environment>();
                c.CreateTable<Variable>();
                c.CreateTable<VariableEnvironment>();
                c.CreateTable<PackageVersion>();
                c.CreateTable<AuditEntry>();

                c.Insert(new Account { ApiKey = apiKey, Name = "Fake Account", License = string.Empty, AccountType = EAccountType.Bolt, ValidationString = validationString, HasBeenValidated = false });
                long accountId = c.GetLastInsertId();
                c.Insert(new User(accountId, "a@b.com", "a"));

                c.Insert(new AppConfig.Model.Environment { AccountId = accountId, Name = "Production", Description = "The production environment" });
                c.Insert(new AppConfig.Model.Environment { AccountId = accountId, Name = "QA", Description = "The qa environment" });
                c.Insert(new AppConfig.Model.Environment { AccountId = accountId, Name = "Staging", Description = "The staging environment" });
                c.Insert(new AppConfig.Model.Environment { AccountId = accountId, Name = "UAT", Description = "The uat environment" });

                c.Insert(new Variable { AccountId = accountId, DefaultValue = "123", Name = "ConnectionString", IsRuntimeAccessible = true, Description = "A test variable" });

                c.Insert(new Package { AccountId = accountId, Name = "My.Package", Transform = "TODO", Description = "A test package", Version = "1.2.3" });
            }
        }

        public Account GetByApiKey(string apiKey)
        {
            using (var c = GetConnection())
            {
                return c.FirstOrDefault<Account>(x => x.ApiKey == apiKey);
            }
        }

        public string ResetApiKey(long accountId, string apiKey, User user)
        {
            if (user == null || user.AccountId != accountId) throw new UnauthorizedAccessException();

            using (var c = GetConnection())
            {
                var account = c.GetById<Account>(accountId);
                if (account == null) return null;

                account.ApiKey = apiKey;

                c.Save(account);
                _auditEntryRepo.Save(AuditEntry.ApiKeyChanged(account, user.Id), user.Id);
                return account.ApiKey;
            }
        }

        public bool ValidateAccount(string validationKey)
        {
            using (var c = GetConnection())
            {
                var account = c.FirstOrDefault<Account>(x => x.ValidationString == validationKey);
                if (account == null) return false;

                if (account.HasBeenValidated) return true;

                account.HasBeenValidated = true;
                c.Save(account);
                _auditEntryRepo.Save(AuditEntry.AccountValidated(account), null);

                return true;
            }
        }

        protected override void OnCreated(Account model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.CreatedAccount(model), userId);
        }
    }
}
