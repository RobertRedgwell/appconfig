﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;
using AppConfig.Model;

namespace AppConfig.Persistence
{
    public class UserRepo : BaseAccountRepo<User>, IUserRepo
    {
        private readonly IAuditEntryRepo _auditEntryRepo;

        public UserRepo(IConfiguration configuration, IAuditEntryRepo auditEntryRepo)
            : base(configuration)
        {
            _auditEntryRepo = auditEntryRepo;
        }

        public User GetByEmailAndPassword(string email, string password)
        {
            var user = GetByEmail(email);

            return (user != null && user.Password == user.HashPassword(password))
                ? user
                : null;
        }

        public User GetByEmail(string email)
        {
            using (var c = GetConnection())
            {
                return c.FirstOrDefault<User>(x =>
                    x.EmailAddress == email
                );
            }
        }

        public bool SetLastLoginNow(long userId, long accountId)
        {
            using (var c = GetConnection())
            {
                var user = c.FirstOrDefault<User>(x =>
                    x.Id == userId && x.AccountId == accountId
                );

                if (user == null) return false;

                user.LastLogin = DateTime.Now;
                c.Save(user);
                return true;
            }
        }

        protected override void OnCreated(User model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.CreatedUser(model, userId), userId);
        }

        protected override void OnUpdated(User model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.UpdatedUser(model, userId), userId);
        }

        protected override void OnDeleted(User model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.DeletedUser(model, userId), userId);
        }
    }
}
