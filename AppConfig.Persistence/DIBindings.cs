﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Persistence
{
    public class DIBindings : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IAccountRepo>().To<AccountRepo>().InSingletonScope();
            Kernel.Bind<IAuditEntryRepo>().To<AuditEntryRepo>().InSingletonScope();
            Kernel.Bind<IEnvironmentRepo>().To<EnvironmentRepo>().InSingletonScope();
            Kernel.Bind<IGroupRepo>().To<GroupRepo>().InSingletonScope();
            Kernel.Bind<IGroupUserRepo>().To<GroupUserRepo>().InSingletonScope();
            Kernel.Bind<IPackageRepo>().To<PackageRepo>().InSingletonScope();
            Kernel.Bind<IPackageVersionRepo>().To<PackageVersionRepo>().InSingletonScope();
            Kernel.Bind<IUserRepo>().To<UserRepo>().InSingletonScope();
            Kernel.Bind<IVariableRepo>().To<VariableRepo>().InSingletonScope();
            Kernel.Bind<IVariableEnvironmentRepo>().To<VariableEnvironmentRepo>().InSingletonScope();

            Kernel.Bind<IConfigurationTransformer>().To<ConfigurationTransformer>().InSingletonScope();
        }
    }
}