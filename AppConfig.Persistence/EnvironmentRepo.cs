﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;
using AppConfig.Model;
using ServiceStack.OrmLite;

namespace AppConfig.Persistence
{
    public class EnvironmentRepo : BaseAccountRepo<Environment>, IEnvironmentRepo
    {
        private readonly IAuditEntryRepo _auditEntryRepo;

        public EnvironmentRepo(IConfiguration configuration, IAuditEntryRepo auditEntryRepo)
            : base(configuration)
        {
            _auditEntryRepo = auditEntryRepo;
        }

        /// <summary>
        /// Gets an environment with the specified name for the account
        /// </summary>
        /// <param name="environmentName"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Environment GetByNameForAccount(string environmentName, long accountId)
        {
            using (var c = GetConnection())
            {
                return c.FirstOrDefault<Environment>(
                    x => x.AccountId == accountId && x.Name == environmentName);
            }
        }

        protected override void OnCreated(Environment model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.CreatedEnvironment(model, userId), userId);
        }

        protected override void OnUpdated(Environment model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.UpdatedEnvironment(model, userId), userId);
        }

        protected override void OnDeleted(Environment model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.DeletedEnvironment(model, userId), userId);
        }
    }
}
