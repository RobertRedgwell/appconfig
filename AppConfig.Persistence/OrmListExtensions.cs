﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.OrmLite;

namespace AppConfig.Persistence
{
    public static class OrmListExtensions
    {
        public static SqlExpressionVisitor<T> OrderBy<T, TKey>(this SqlExpressionVisitor<T> query, Expression<Func<T, TKey>> keySelector, bool ascending)
        {
            return ascending 
                ? query.OrderBy(keySelector)
                : query.OrderByDescending(keySelector);
        }
    }
}
