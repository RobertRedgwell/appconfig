﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;
using AppConfig.Model;
using ServiceStack.OrmLite;
using System.Data;

namespace AppConfig.Persistence
{
    public class AuditEntryRepo : BaseAccountRepo<AuditEntry>, IAuditEntryRepo
    {
        public AuditEntryRepo(IConfiguration configuration)
            : base(configuration)
        {
        }
    }
}
