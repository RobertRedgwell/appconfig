﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;
using AppConfig.Model;
using NuGet;
using ServiceStack.OrmLite;

namespace AppConfig.Persistence
{
    public class PackageRepo : BaseAccountRepo<Package>, IPackageRepo
    {
        private readonly IAuditEntryRepo _auditEntryRepo;

        public PackageRepo(IConfiguration configuration, IAuditEntryRepo auditEntryRepo)
            : base(configuration)
        {
            _auditEntryRepo = auditEntryRepo;
        }

        public Package GetByNameAndVersionForAccount(string name, SemanticVersion version, long accountId)
        {
            using (var c = GetConnection())
            {
                // semantic version ordering is a bit strange; so grab all packages that have the name, and account id:
                var packages = c.Select<Package>(x => x.Name == name && x.AccountId == accountId).ToList();
                
                // Now order the packages, and return the first package that is equal to or closest to out semantic version we are looking for
                Package currentPackage = null;
                foreach (var p in packages.OrderBy(x => new SemanticVersion(x.Version), new SemanticVersionComparer()))
                {
                    if (new SemanticVersion(p.Version) <= version) currentPackage = p;
                    else break;
                }
                return currentPackage;
            }
        }

        public void UpdateVariableName(string existingVariableName, string newVariableName, long accountId)
        {
            using (var c = GetConnection())
            {

                // This is horrendous, I know. But the parameter escaping plays havoc when inside a replace function...
                // Then add the fact that the values need to be inside double {}'s (e.g. ${{0}} which becomes {0})...
                var count = c.ExecuteNonQuery(
                    "UPDATE Package SET Transform = REPLACE(Transform, " + 
                    "'${' + " + "{0}".Params(existingVariableName) + " + '}', " + 
                    "'${' + " + "{0}".Params(newVariableName) + " + '}') " + 
                    "WHERE AccountId = " + accountId);
            }
        }

        protected override void OnCreated(Package model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.CreatedPackage(model, userId), userId);
        }

        protected override void OnUpdated(Package model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.UpdatedPackage(model, userId), userId);
        }

        protected override void OnDeleted(Package model, long? userId)
        {
            _auditEntryRepo.Save(AuditEntry.DeletedPackage(model, userId), userId);
        }
    }
}
