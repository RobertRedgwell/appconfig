﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;
using NLog;
using ServiceStack.OrmLite;

namespace AppConfig.Persistence
{
    public abstract class BaseAccountRepo<T> : BaseRepo<T>, IBaseAccountRepo<T>
        where T : BaseAccountModel, new()
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        protected BaseAccountRepo(IConfiguration configuration)
            : base(configuration)
        {
        }

        public int GetCountForAccount(long accountId)
        {
            using (var c = GetConnection())
            {
                return (int)c.Count<T>(x => x.AccountId == accountId);
            }
        }

        public List<T> GetListForAccount(long accountId)
        {
            using (var c = GetConnection())
            {
                return c.Select<T>(x => x.AccountId == accountId);
            }
        }

        public PagedList<T> GetListForAccount<TKey>(long accountId, int itemsPerPage, int page, bool orderAscending, Expression<Func<T, TKey>> keySelector)
        {
            using (var c = GetConnection())
            {
                var list = c.Select<T>(x => x
                    .Where(i => i.AccountId == accountId)
                    .OrderBy(keySelector, orderAscending)
                    .Limit(page * itemsPerPage, itemsPerPage));

                var result = new PagedList<T>
                {
                    ItemsPerPage = itemsPerPage,
                    Page = page,
                    List = list,
                    TotalItems = c.Count<T>(x => x.AccountId == accountId),
                };
                
                return result;
            }
        }

        public T GetByIdForAccount(long id, long accountId)
        {
            using (var c = GetConnection())
            {
                return c.FirstOrDefault<T>(x => x.Id == id && x.AccountId == accountId);
            }
        }

        public void SaveForAccount(T model, long accountId, long userId)
        {
            if (model.AccountId != accountId)
            {
                Log.Warn("Attempt to save model of type {0} with id {1} by account {2} when the account owner was actually {3}",
                    typeof(T), model.Id, accountId, model.AccountId);

                throw new UnauthorizedAccessException();
            }

            base.Save(model, userId);
        }

        public void SaveForAccount(List<T> models, long accountId, long userId)
        {
            using (var c = GetConnection())
            {
                foreach (var m in models)
                {
                    if (m.AccountId != accountId)
                    {
                        Log.Warn("Attempt to save model of type {0} with id {1} by account {2} when the account owner was actually {3}",
                            typeof(T), m.Id, accountId, m.AccountId);

                        throw new UnauthorizedAccessException();
                    }
                    base.Save(c, m, userId);
                }
            }
        }

        public virtual bool DeleteForAccount(long id, long accountId, long userId)
        {
            using (var c = GetConnection())
            {
                var model = c.FirstOrDefault<T>(x => x.Id == id && x.AccountId == accountId);
                if (model == null) return false;
                c.Delete<T>(x => x.Id == id && x.AccountId == accountId);
                OnDeleted(model, userId);
            }
            return true;
        }

        public virtual bool DeleteForAccount(T model, long accountId, long userId)
        {
            if (model.AccountId != accountId)
            {
                Log.Warn("Attempt to delete model of type {0} with id {1} by account {2} when the account owner was actually {3}",
                    typeof(T), model.Id, accountId, model.AccountId);

                throw new UnauthorizedAccessException();
            }
            return base.Delete(model, userId);
        }
    }
}
