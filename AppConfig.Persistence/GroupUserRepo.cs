﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;
using AppConfig.Model;

namespace AppConfig.Persistence
{
    public class GroupUserRepo : BaseAccountRepo<GroupUser>, IGroupUserRepo
    {
        public GroupUserRepo(IConfiguration configuration)
            : base(configuration)
        {
        }
    }
}
