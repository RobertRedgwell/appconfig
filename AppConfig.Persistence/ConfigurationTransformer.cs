﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using AppConfig.Model;
using AppConfig.Model.Exceptions;
using AppConfig.Persistence.Interfaces;
using Microsoft.Web.XmlTransform;
using NLog;
using NuGet;

namespace AppConfig.Persistence
{
    public class ConfigurationTransformer : IConfigurationTransformer
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private readonly IEnvironmentRepo _environmentRepo;
        private readonly IPackageRepo _packageRepo;
        private readonly IVariableRepo _variableRepo;
        private readonly IAuditEntryRepo _auditEntryRepo;

        public ConfigurationTransformer(IEnvironmentRepo environmentRepo, IPackageRepo packageRepo, IVariableRepo variableRepo, IAuditEntryRepo auditEntryRepo)
        {
            _environmentRepo = environmentRepo;
            _packageRepo = packageRepo;
            _variableRepo = variableRepo;
            _auditEntryRepo = auditEntryRepo;
        }

        /// <summary>
        /// For the given package, dependencies, environment, and input, perform the appropriate transformation and return the result.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="corePackage"></param>
        /// <param name="dependencies"></param>
        /// <param name="environmentName"></param>
        /// <param name="inputConfig"></param>
        /// <exception cref="ArgumentNullException">Thrown if any of the arguments are null</exception>
        /// <exception cref="ArgumentException">Thrown if any of the arguments are invalid</exception>
        /// <exception cref="ValidationException">Thrown if any of the PackageDescriptors are invalid</exception>
        /// <exception cref="EnvironmentNotFoundException">Thrown if no environment exists with the specified name</exception>
        /// <exception cref="TransformationException">Thrown if on of the package transforms fails</exception>
        /// <returns>The transformed configuration file</returns>
        public string DoTransformation(long accountId, PackageDescriptor corePackage, List<PackageDescriptor> dependencies, string environmentName, string inputConfig)
        {
            if (corePackage == null) throw new ArgumentNullException("corePackage");
            if (environmentName == null) throw new ArgumentNullException("environmentName");
            if (inputConfig == null) throw new ArgumentNullException("inputConfig");

            var environment = _environmentRepo.GetByNameForAccount(environmentName, accountId);
            if (environment == null)
            {
                throw new EnvironmentNotFoundException(environmentName);
            }

            var document = new XmlDocument();
            document.PreserveWhitespace = true;
            try
            {
                document.LoadXml(inputConfig);
            }
            catch (XmlException e)
            {
                Log.Error("Error parsing xml for input {0}", inputConfig, e);
                throw new ArgumentException("inputConfig");
            }

            if (dependencies == null)
                dependencies = new List<PackageDescriptor>();

            // Put the core package at the beginning of the list
            dependencies.Insert(0, corePackage);

            var sortedVariables = new SortedList<string, VariableForEnvironment>();
            foreach (var v in _variableRepo.GetVariableValuesForEnvironment(environment.Id, accountId))
            {
                sortedVariables.Add(v.Name, v);
            }

            // Go through our packages, running the transform for each on the input config
            foreach (var p in dependencies)
            {
                try
                {
                    Validator.ValidateObject(p, new ValidationContext(p, null, null), true);
                }
                catch (ValidationException ve)
                {
                    Log.Warn(string.Format("Invalid package specified with name '{0}' and version '{1}'.", p.Name, p.Version), ve);
                    continue;
                }

                // get the package transform for the environment:
                var transformXml = GetTransformForPackageAndVariables(p, sortedVariables, accountId);
                Log.Debug("Transform for package {0} - {1} and account {2} resolved as {3}", p.Name, p.Version, accountId, transformXml);

                if (transformXml != null)
                {
                    using (var transformation = new XmlTransformation(transformXml, isTransformAFile: false, logger: null))
                    {
                        if (!transformation.Apply(document))
                        {
                            Log.Warn("Transformation failed for package {0} - {1} and account {2}. Transform resolved as {3}.", p.Name, p.Version, accountId, transformXml);
                        }
                    }
                }
            }

            var package = _packageRepo.GetByNameAndVersionForAccount(corePackage.Name, new SemanticVersion(corePackage.Version), accountId);
            if (package != null)
            {
                _auditEntryRepo.Save(AuditEntry.TransformPerformed(package, environment, null), null);
            }

            return document.OuterXml;
        }

        private string GetTransformForPackageAndVariables(PackageDescriptor packageDescriptor, SortedList<string, VariableForEnvironment> variables, long accountId)
        {
            var package = _packageRepo.GetByNameAndVersionForAccount(packageDescriptor.Name, new SemanticVersion(packageDescriptor.Version), accountId);
            // if there is no package, then there is not transform so return null:
            if (package == null) return null;

            var transform = package.Transform;

            if (variables != null)
            {
                // do the transform using the variables we have:
                foreach (var kvp in variables)
                {
                    transform = transform.Replace("${" + kvp.Key + "}", kvp.Value.Value ?? kvp.Value.DefaultValue);
                }
            }
            
            return transform;
        }
    }
}
