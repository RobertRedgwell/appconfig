﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Persistence.Interfaces;
using AppConfig.Model;
using ServiceStack.OrmLite;

namespace AppConfig.Persistence
{
    public class VariableEnvironmentRepo : BaseAccountRepo<VariableEnvironment>, IVariableEnvironmentRepo
    {
        private readonly IEnvironmentRepo _environmentRepo;

        public VariableEnvironmentRepo(IConfiguration configuration, IEnvironmentRepo environmentRepo)
            : base(configuration)
        {
            _environmentRepo = environmentRepo;
        }

        public List<VariableEnvironment> GetForVariable(long variableId, long accountId)
        {
            using (var c = GetConnection())
            {
                return c.Where<VariableEnvironment>(x => 
                    x.VariableId == variableId &&
                    x.AccountId == accountId);
            }
        }

        public List<VariableEnvironment> GetForEnvironment(long environmentId, long accountId)
        {
            using (var c = GetConnection())
            {
                return c.Where<VariableEnvironment>(x =>
                    x.EnvironmentId == environmentId &&
                    x.AccountId == accountId);
            }
        }

        public VariableEnvironment GetForEnvironmentAndVariable(long environmentId, long variableId, long accountId)
        {
            using (var c = GetConnection())
            {
                return c.FirstOrDefault<VariableEnvironment>(x =>
                    x.EnvironmentId == environmentId &&
                    x.VariableId == variableId &&
                    x.AccountId == accountId);
            }
        }

        public void SetVariableEnvironmentsForVariable(Variable model, List<VariableEnvironment> variableEnvironments, long? userId)
        {
            var availableEnvironments = new SortedList<long, bool>();

            using (var c = GetConnection())
            {
                c.Delete<VariableEnvironment>(x => x.VariableId == model.Id);
                foreach (var ve in variableEnvironments)
                {
                    // check the environment is available for this account
                    if (!availableEnvironments.ContainsKey(ve.EnvironmentId))
                    {
                        availableEnvironments.Add(ve.EnvironmentId, _environmentRepo.GetByIdForAccount(ve.EnvironmentId, model.AccountId) != null);
                    }

                    if (availableEnvironments[ve.EnvironmentId])
                    {
                        ve.VariableId = model.Id;
                        ve.AccountId = model.AccountId;
                        Save(ve, userId);
                    }
                }
            }
        }

        public bool DeleteForVariable(long variableId, long accountId, long? userId)
        {
            using (var c = GetConnection())
            {
                var ve = c.FirstOrDefault<VariableEnvironment>(x => x.VariableId == variableId && x.AccountId == accountId);
                if (ve == null) return false;
                c.Delete<VariableEnvironment>(x => x.VariableId == variableId && x.AccountId == accountId);
                OnDeleted(ve, userId);
                return true;
            }
        }
    }
}
