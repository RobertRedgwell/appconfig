﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;
using AppConfig.Model;
using AppConfig.Model.Exceptions;
using AppConfig.Persistence.Interfaces;
using ServiceStack.OrmLite;

namespace AppConfig.Persistence
{
    public abstract class BaseRepo<T> : IBaseRepo<T>
        where T : BaseModel, new()
    {
        public const string ConnectionStringKey = "AppConfigDb";
        private OrmLiteConnectionFactory _dbFactory;

        protected BaseRepo(IConfiguration configuration)
        {
            _dbFactory = new OrmLiteConnectionFactory(
                configuration.ConnectionStrings[ConnectionStringKey], 
                SqlServerDialect.Provider); 
        }

        /// <summary>
        /// Gets a connection to the database; should be used in a using block e.g.
        /// <code>using (var c = GetConnection()) {}</code>
        /// </summary>
        /// <returns></returns>
        protected IDbConnection GetConnection()
        {
            return _dbFactory.Open();
        }

        public List<T> GetList()
        {
            using (var c = GetConnection())
            {
                return c.Select<T>();
            }
        }

        public T GetById(long id)
        {
            using (var c = GetConnection())
            {
                return c.GetById<T>(id);
            }
        }

        public void Save(T model, long? userId)
        {
            try
            {
                using (var c = GetConnection())
                {
                    Save(c, model, userId);
                }
            }
            catch (SqlException e)
            {
                HandleSqlException(e);
            }
        }

        public void Save(List<T> models, long? userId)
        {
            try 
            {
                using (var c = GetConnection())
                {
                    foreach (var m in models)
                    {
                        Save(c, m, userId);
                    }
                }
            }
            catch (SqlException e)
            {
                HandleSqlException(e);
            }
        }

        public bool Delete(long id, long? userId)
        {
            using (var c = GetConnection())
            {
                var model = c.GetById<T>(id);
                if (model == null) return false;
                c.DeleteById<T>(id);
                OnDeleted(model, userId);
            }
            return true;
        }

        public bool Delete(T model, long? userId)
        {
            using (var c = GetConnection())
            {
                c.Delete(model);
                OnDeleted(model, userId);
            }
            return true;
        }

        private void HandleSqlException(SqlException e) 
        {
            if (e.Message.Contains("duplicate"))
            {
                throw UniqueConstraintViolatedException.FromSqlException(e);
            }
            else
            {
                throw e;
            }
        }

        internal virtual void Save(IDbConnection c, T model, long? userId)
        {
            c.Save(model);
            if (model.Id == 0)
            {
                model.Id = c.GetLastInsertId();
                OnCreated(model, userId);
            }
            else
            {
                OnUpdated(model, userId);
            }
        }

        protected virtual void OnCreated(T model, long? userId) { }
        protected virtual void OnUpdated(T model, long? userId) { }
        protected virtual void OnDeleted(T model, long? userId) { }
    }
}
