﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Persistence.Interfaces
{
    public interface IEnvironmentRepo : IBaseAccountRepo<Environment>
    {
        /// <summary>
        /// Gets an environment with the specified name for the account
        /// </summary>
        /// <param name="environmentName"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Environment GetByNameForAccount(string environmentName, long accountId);
    }
}
