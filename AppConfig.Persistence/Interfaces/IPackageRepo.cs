﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;
using NuGet;

namespace AppConfig.Persistence.Interfaces
{
    public interface IPackageRepo : IBaseAccountRepo<Package>
    {
        Package GetByNameAndVersionForAccount(string name, SemanticVersion version, long accountId);

        void UpdateVariableName(string existingVariableName, string newVariableName, long accountId);
    }
}
