﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Persistence.Interfaces
{
    public interface IVariableEnvironmentRepo : IBaseAccountRepo<VariableEnvironment>
    {
        List<VariableEnvironment> GetForVariable(long variableId, long accountId);

        List<VariableEnvironment> GetForEnvironment(long environmentId, long accountId);

        VariableEnvironment GetForEnvironmentAndVariable(long environmentId, long variableId, long accountId);

        void SetVariableEnvironmentsForVariable(Variable model, List<VariableEnvironment> variableEnvironments, long? userId);

        bool DeleteForVariable(long variableId, long accountId, long? userId);
    }
}
