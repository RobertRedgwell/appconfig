﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Persistence.Interfaces
{
    public interface IUserRepo : IBaseAccountRepo<User>
    {

        User GetByEmailAndPassword(string email, string password);

        User GetByEmail(string email);

        bool SetLastLoginNow(long userId, long accountId);
    }
}
