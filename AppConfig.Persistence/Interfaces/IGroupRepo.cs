﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Persistence.Interfaces
{
    public interface IGroupRepo : IBaseAccountRepo<Group>
    {
        
    }
}
