﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Persistence.Interfaces
{
    public interface IBaseAccountRepo<T> : IBaseRepo<T>
    {
        int GetCountForAccount(long accountId);

        List<T> GetListForAccount(long accountId);

        PagedList<T> GetListForAccount<TKey>(long accountId, int itemsPerPage, int page, bool orderAscending, Expression<Func<T, TKey>> keySelector);

        T GetByIdForAccount(long id, long accountId);

        void SaveForAccount(T model, long accountId, long userId);

        void SaveForAccount(List<T> models, long accountId, long userId);

        bool DeleteForAccount(long id, long accountId, long userId);

        bool DeleteForAccount(T model, long accountId, long userId);
    }
}