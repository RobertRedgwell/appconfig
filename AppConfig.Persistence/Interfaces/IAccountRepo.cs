﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppConfig.Model;

namespace AppConfig.Persistence.Interfaces
{
    public interface IAccountRepo : IBaseRepo<Account>
    {
        //void InitDatabase(string apiKey, string validationString);

        Account GetByApiKey(string apiKey);

        string ResetApiKey(long accountId, string apiKey, User user);

        bool ValidateAccount(string validationKey);
    }
}
