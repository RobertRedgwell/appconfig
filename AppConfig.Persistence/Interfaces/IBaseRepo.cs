﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Persistence.Interfaces
{
    public interface IBaseRepo<T>
    {
        List<T> GetList();

        T GetById(long id);

        void Save(T model, long? userId);

        void Save(List<T> models, long? userId);

        bool Delete(long id, long? userId);

        bool Delete(T model, long? userId);
    }
}
