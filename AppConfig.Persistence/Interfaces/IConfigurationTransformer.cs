﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AppConfig.Model;
using AppConfig.Model.Exceptions;

namespace AppConfig.Persistence.Interfaces
{
    public interface IConfigurationTransformer
    {
        /// <summary>
        /// For the given package, dependencies, environment, and input, perform the appropriate transformation and return the result.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="corePackage"></param>
        /// <param name="dependencies"></param>
        /// <param name="environmentName"></param>
        /// <param name="inputConfig"></param>
        /// <exception cref="ArgumentNullException">Thrown if any of the arguments are null</exception>
        /// <exception cref="ValidationException">Thrown if any of the PackageDescriptors are invalid</exception>
        /// <exception cref="EnvironmentNotFoundException">Thrown if no environment exists with the specified name</exception>
        /// <exception cref="TransformationException">Thrown if on of the package transforms fails</exception>
        /// <returns>The transformed configuration file</returns>
        string DoTransformation(long accountId, PackageDescriptor corePackage, List<PackageDescriptor> dependencies, string environmentName, string inputConfig);
    }
}
