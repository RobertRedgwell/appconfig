﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AppConfig.Model;
using AutoMapper;
using ServiceStack.ServiceInterface.ServiceModel;

namespace AppConfig.Api.Model
{
    public class VariableDto
    {
        public VariableDto()
        {
            VariableEnvironments = new List<VariableEnvironmentDto>();
        }

        public VariableDto(Variable v)
        {
            Mapper.Map(v, this);
            VariableEnvironments = new List<VariableEnvironmentDto>();
        }

        public long Id { get; set; }
        public long AccountId { get; set; }
        [Required]
        [RegularExpression(@"[a-zA-Z0-9\-_]*")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string DefaultValue { get; set; }
        public bool IsRuntimeAccessible { get; set; }

        public List<VariableEnvironmentDto> VariableEnvironments { get; set; }

        public ResponseStatus ResponseStatus { get; set; } //Exception gets serialized here
    }
}