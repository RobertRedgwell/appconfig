﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AppConfig.Core;
using AppConfig.Model;
using AutoMapper;

namespace AppConfig.Api.Model
{
    public class PackageDto
    {
        public PackageDto()
        {
        }

        public PackageDto(Package p)
        {
            Mapper.Map(p, this);
        }

        public long Id { get; set; }
        public long AccountId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public string Transform { get; set; }

        [Required]
        [RegularExpression(Constants.SemanticVersionRegex, ErrorMessage = "Version must be a semantic version string")]
        public string Version { get; set; }
        public bool Deleted { get; set; }

    }
}