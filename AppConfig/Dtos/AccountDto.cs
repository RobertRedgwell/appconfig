﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Core;
using AppConfig.Model;

namespace AppConfig.Api.Model
{
    public class AccountDto
    {
        public AccountDto(AccountDetails accountDetails)
        {
            AccountType = (int)accountDetails.Account.AccountType;
            AccountTypeName = accountDetails.Account.AccountType.ToString();
            ApiKey = accountDetails.Account.ApiKey;
            UserCount = accountDetails.UserCount;
            EnvironmentCount = accountDetails.EnvironmentCount;
            VariableCount = accountDetails.VariableCount;
            PackageCount = accountDetails.PackageCount;
            switch (accountDetails.Account.AccountType)
            {
                case EAccountType.Shock:
                    UserMax = 1;
                    EnvironmentMax = 2;
                    VariableMax = 20;
                    PackageMax = 10;
                    break;
                case EAccountType.Bolt:
                    UserMax = 5;
                    EnvironmentMax = 5;
                    VariableMax = 100;
                    PackageMax = 100;
                    break;
                case EAccountType.Storm:
                    UserMax = 10000;
                    EnvironmentMax = 10000;
                    VariableMax = 10000;
                    PackageMax = 10000;
                    break;
            }

            var licenseData = accountDetails.Account.GetLicenseData();
            if (licenseData == null)
            {
                LicenseKey = string.Empty;
                LicenseStatus = ELicenseStatus.Unlicensed.ToString();
                IsLockedDown = true;
            }
            else
            {
                LicenseKey = licenseData.License ?? string.Empty;
                IsLockedDown = licenseData.LockedDown;
                switch (licenseData.LastLicenseStatus)
                {
                    case ELicenseStatus.LicensedOldVersion:
                        LicenseStatus = "Licensed for a previous version";
                        break;
                    case ELicenseStatus.MismatchedLicense:
                        LicenseStatus = "Invalid license key";
                        break;
                    case ELicenseStatus.TrialLicensed:
                        LicenseStatus = "Licensed (Trial)";
                        break;
                    default:
                        LicenseStatus = licenseData.LastLicenseStatus.ToString();
                        break;
                }
            }
        }

        public int AccountType { get; set; }

        public string AccountTypeName { get; set; }
        
        public string LicenseKey { get; set; }
        public string LicenseStatus { get; set; }
        public bool IsLockedDown { get; set; }
        

        public string ApiKey { get; set; }

        public int UserCount { get; set; }
        public int UserMax { get; set; }

        public int EnvironmentCount { get; set; }
        public int EnvironmentMax { get; set; }

        public int VariableCount { get; set; }
        public int VariableMax { get; set; }

        public int PackageCount { get; set; }
        public int PackageMax { get; set; }
    }
}