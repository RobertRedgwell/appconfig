﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AutoMapper;

namespace AppConfig.Api.Model
{
    public class EnvironmentDto
    {
        public EnvironmentDto()
        {
        }

        public EnvironmentDto(AppConfig.Model.Environment e)
        {
            Mapper.Map(e, this);
        }

        public long Id { get; set; }
        public long AccountId { get; set; }
        
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Deleted { get; set; }
    }
}