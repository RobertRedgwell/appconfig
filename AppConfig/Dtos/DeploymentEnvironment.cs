﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppConfig.Api.Model
{
    public enum DeploymentEnvironment
    {
        Nightly = 1,
        QA = 2,
        UAT = 3,
        Staging = 4,
        PreProd = 5,
        Production = 6
    }
}