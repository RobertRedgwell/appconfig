﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Model;
using AutoMapper;

namespace AppConfig.Api.Model
{
    public class VariableEnvironmentDto
    {
        public VariableEnvironmentDto()
        {
        }

        public VariableEnvironmentDto(VariableEnvironment ve)
        {
            Mapper.Map(ve, this);
            IsOverridden = true; // we are always overridden if we are being created from a variable environment object
        }

        public long Id { get; set; }
        public long AccountId { get; set; }
        public long VariableId { get; set; }
        public long EnvironmentId { get; set; }
        public bool IsOverridden { get; set; }
        public string Value { get; set; }
        public bool IsHidden { get; set; }
    }
}