﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AppConfig.Core;
using AppConfig.Model;
using AutoMapper;

namespace AppConfig.Api.Model
{
    public class UserDto
    {
        public UserDto(User user)
        {
            Mapper.Map(user, this);
        }

        public long Id { get; set; }

        [Required]
        [RegularExpression(Constants.EmailRegex, ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }

        public DateTime? LastLogin { get; set; }
    }
}