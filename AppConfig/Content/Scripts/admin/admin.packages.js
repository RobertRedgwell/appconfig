﻿var Packages = 'Packages';
function PackagesViewModel(parent) {
    var self = this;
    var parent = parent;
    var selectedPackageId;

    var defaultXml = '<?xml version="1.0"?>\n' +
'<configuration xmlns:xdt="http://schemas.microsoft.com/XML-Document-Transform">\n\n' +
'</configuration>';

    self.requiresRefreshOnView = false;
    self.showingTransformHelp = ko.observable(false);
    self.onTabSelected = function () {
        if (self.requiresRefreshOnView) {
            self.selectedPackage(null);
            loadPackages();
            self.requiresRefreshOnView = false;
        }
        
        /*
        $.ajax("/api/environments", {
            contentType: "application/json",
            success: function (response) {
                self.exampleTransformVM.environments(response);
            }
        });
        */
    }
    self.saveErrors = ko.observableArray([]);
    self.isSaving = ko.observable(false);
    self.packages = ko.observableArray([]);
    self.packages.subscribe(function (p) {
        if (p.length > 0) {
            p.sort(function (p1, p2) { return ("" + p1.name + p1.version).toLowerCase() > ("" + p2.name + p2.version).toLowerCase(); });
            self.selectPackage(p[0]);
        }
    });

    self.selectPackage = function (p) { self.selectedPackage(ko.mapping.fromJS(p)); }
    self.selectedPackage = ko.observable();
    self.selectedPackage.subscribe(function (p) {
        if (!p) return;
        p.isNew = ko.computed(function () {
            return this.id() == null;
        }, p);

        setTimeout(function () {
            self.editor = ace.edit("package-transform");
            self.editor.setTheme("ace/theme/clouds");
            self.editor.getSession().setMode("ace/mode/xml");
            self.editor.getSession().setValue(self.selectedPackage().transform());
            self.editor.getSession().setTabSize(4);
            self.editor.getSession().setUseSoftTabs(false);

            self.editor.setShowPrintMargin(false);

            var cc = new codeComplete(self.editor, [
                { delimiter: 'xdt:', values: ["Transform", "Locator"], suffix: "=" },
                { delimiter: 'xdt:Transform="', values: ["Replace", "Remove", "RemoveAll", "Insert", "InsertBefore", "InsertAfter", "RemoveAttributes", "SetAttributes"], suffix: "\"" },
                { delimiter: 'xdt:Locator="', values: ["Condition()", "Match()", "XPath()"], suffix: "\"" },
                { delimiter: '${', values: self.suggests.slice(0), suffix: "}" }
            ]);
        }, 1);
    });
    self.deletePackage = function () {
        if (confirm("Are you sure you want to delete this package?")) {
            self.isSaving(true);
            $.ajax("/api/packages/" + self.selectedPackage().id(), {
                type: "delete", contentType: "application/json",
                success: function (result) {
                    removeFromList();
                    self.selectedPackage(null);
                    self.isSaving(false);
                },
                error: function () { self.isSaving(false); }
            });
        }
    };

    self.save = function () {
        var isUpdate = self.selectedPackage().id() != null;
        var url = "/api/packages";
        if (isUpdate) url += "/" + self.selectedPackage().id();

        self.selectedPackage().transform(self.editor.getSession().getValue());

        self.saveErrors([]);
        self.isSaving(true);
        $.ajax(url, {
            data: ko.mapping.toJSON({ Model: self.selectedPackage() }),
            type: (isUpdate ? "put" : "post"), contentType: "application/json",
            success: function (result) {
                self.isSaving(false);
                removeFromList();
                self.packages.push(result);
                self.selectPackage(result);
                toastSaveSuccess('Package saved successfully.');
            },
            error: function (response, errorCode, errorMessage) {
                self.isSaving(false);
                var errors = $.ss.getErrors(response);
                for (var k in errors) {
                    self.saveErrors().push(k + ": " + errors[k]);
                }
                self.saveErrors.valueHasMutated();
            }
        });
    }

    self.selectPackageWithId = function (packageId) {
        selectedPackageId = packageId;
        var package = ko.utils.arrayFirst(self.packages(), function (p) { return p.id == packageId; });
        if (package != null) {
            self.selectedPackage(package);
        }
    }

    self.createPackage = function () {
        self.selectPackage({ id: null, name: '', description: '', transform: defaultXml, version: '' });
    }

    self.toggleTransformHelp = function () {
        self.showingTransformHelp(!self.showingTransformHelp());
    }

    function removeFromList() {
        self.packages.remove(function (p) { return p.id == self.selectedPackage().id(); });
    }

    //self.exampleTransformVM = new ExampleTransformViewModel(self);


    // load our packages
    function loadPackages() {
        $.ajax("/api/packages", {
            contentType: "application/json",
            success: self.packages
        });
    }
    loadPackages();

    self.suggests = [];

    // load our variables 
    parent.variablesVM.variables.subscribe(function (v) {
        self.suggests = [];
        for (var i = 0; i < v.length; i++) {
            self.suggests.push(v[i].name);
        }
    });
}
/*
function ExampleTransformViewModel(parent) {
    var self = this;
    var parent = parent;

    var showLabel = 'Show environment viewer';
    var hideLabel = 'Hide environment viewer';
    
    self.isShowing = ko.observable(false);

    self.showHideLabel = ko.computed(function () {
        return (self.isShowing() ? hideLabel : showLabel);
    }, self);
    self.isEnabled = ko.computed(function () {
        return parent.selectedPackage() != null && !parent.selectedPackage().isNew();
    }, self);

    self.environments = ko.observableArray([]);
    self.selectedEnvironment = ko.observable();
    self.transformResult = ko.observable();

    self.showHideExamplePanel = function() {
        self.isShowing(!self.isShowing());
    }

    self.showExampleTransform = function () {
        if (self.selectedEnvironment() != null) {
            // TODO: Get the vars for the specified environment, and do the replace client side.
            self.transformResult('TODO: do transform for ' + self.selectedEnvironment().id);
        }
    }
}*/