﻿var Dashboard = 'Dashboard';
function DashboardViewModel(parent) {
    var self = this;
    var parent = parent;
    var selectedAuditEntryId;

    self.onTabSelected = function () {
        loadAuditLog();
    };

    self.page = ko.observable(0);
    self.total = ko.observable(0);
    self.itemsPerPage = ko.observable(0);

    self.auditEntries = ko.observableArray([]);
    self.auditEntries.subscribe(function (e) {
        for (var i = 0; i < e.length; i++) {
            e[i].formattedDate = AppConfig.dateUtil.serverFormat(e[i].when);
            var actionType = e[i].actionType;
            var actionTypeReadable = actionType[0];
            for (var j = 1; j < actionType.length; j++) {
                var c = actionType[j];
                if (c == c.toUpperCase()) {
                    c = c.toLowerCase();
                    actionTypeReadable += " ";
                }
                actionTypeReadable += c;
            }
            e[i].actionType = actionTypeReadable;
        }
    });  
        
    function loadAuditLog() {
        // load our dashboard data
        $.ajax("/api/dashboard/" + self.page(), {
            contentType: "application/json",
            success: function (response) {
                if (response.list.length == 0) {
                    response.list[0] = { actionType: "", message: "Welcome to ZapConfig!", when: new Date().toString() };
                }
                self.auditEntries(response.list);
                self.page(response.page);
                self.total(response.totalItems);
                self.itemsPerPage(response.itemsPerPage);
            }
        });
    }
    loadAuditLog();
}
