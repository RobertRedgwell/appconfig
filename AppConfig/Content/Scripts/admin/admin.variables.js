﻿var Variables = 'Variables';
function VariablesViewModel(parent) {
    var self = this;
    self.parent = parent;

    self.environments = ko.observableArray([]);
    self.editVariableVM = new EditVariableViewModel(self, self.environments);

    self.variables = ko.observableArray([]);
    self.variables.subscribe(function (v) {
        v.sort(function (v1, v2) { return ("" + v1.name).toLowerCase() > ("" + v2.name).toLowerCase(); });
        
        for (var i = 0; i < v.length; i++) {
            v[i].isExpanded = ko.observable(false);
            for (var j = 0; j < v[i].variableEnvironments.length; j++) {
                var ve = v[i].variableEnvironments[j];
                if (!ve.value) ve.value = '';
                var env = ko.utils.arrayFirst(self.environments(),
                    function (e) { return ve.environmentId === e.id; }
                );

                ve.environmentName = env.name;
            }
        }
    });

    self.newVariable = function () {
        self.editVariable({ id: null, name: '', description: '', defaultValue: '', isRuntimeAccessible: false });
    }

    self.editVariable = function (e) {
        if (e.variableEnvironments == null) e.variableEnvironments = [];
        self.editVariableVM.variable(ko.mapping.fromJS(e));
    }

    self.toggleExpanded = function (v) {
        if (!v) return false;
        v.isExpanded(!v.isExpanded());
    }

    self.onTabSelected = function () {
        // load our environments
        $.ajax("/api/environments", {
            contentType: "application/json",
            success: function (response) {
                self.environments(response);

                // load our variables
                $.ajax("/api/variables", {
                    contentType: "application/json",
                    success: self.variables
                });
            }
        });
    }
    self.onTabSelected();
}

function EditVariableViewModel(parent, environments) {
    var self = this;
    var parent = parent;
    var availableEnvironments = environments;

    self.isSaving = ko.observable(false);
    self.variable = ko.observable();
    self.variable.subscribe(function (v) {
        if (!v) return;
        v.isNew = ko.computed(function () {
            return self.variable() != null && self.variable().id() == null;
        }, v);

        var configuredEnvironments = v.variableEnvironments || ko.observableArray([]);
        v.variableEnvironments = ko.observableArray([]);
        for (var i = 0; i < availableEnvironments().length; i++) {
            var existing = ko.utils.arrayFirst(configuredEnvironments(),
                function (e) { return e.environmentId() === availableEnvironments()[i].id; }
            );
            var environmentName = availableEnvironments()[i].name;
            if (!existing) {
                existing = {
                    id: null,
                    environmentId: availableEnvironments()[i].id,
                    environmentName: environmentName,
                    isOverridden: false,
                    value: null,
                    isHidden: false
                }
            }
            else {
                existing.environmentName = ko.observable(environmentName);
            }
            v.variableEnvironments().push(ko.mapping.fromJS(existing));
        }
    });

    self.saveErrors = ko.observableArray([]);

    self.save = function () {
        var isUpdate = self.variable().id() != null;
        var url = "/api/variables";
        if (isUpdate) url += "/" + self.variable().id();

        self.saveErrors([]);
        self.isSaving(true);
        $.ajax(url, {
            data: ko.mapping.toJSON({ Model: self.variable() }),
            type: (isUpdate ? "put" : "post"), contentType: "application/json",
            success: function (result) {
                self.isSaving(false);
                removeFromParent();
                parent.variables.push(result);

                // Refresh our packages when we view them to make sure we have the latest variable names
                parent.parent.packagesVM.requiresRefreshOnView = true;

                $('#editVariableModal').foundation('reveal', 'close');
                toastSaveSuccess('Variable saved successfully.');
            },
            error: function (response, errorCode, errorMessage) {
                self.isSaving(false);
                var errors = $.ss.getErrors(response);
                for (var k in errors) {
                    self.saveErrors().push(k + ": " + errors[k]);
                }
                self.saveErrors.valueHasMutated();
            }
        });
    }

    self.deleteVariable = function () {
        if (confirm("Deleting this variable will also remove all variable settings you have configured for this variable. Are you sure you want to delete this variable? ")) {
            self.isSaving(true);
            $.ajax("/api/variables/" + self.variable().id(), {
                type: "delete", contentType: "application/json",
                success: function (result) {
                    removeFromParent();
                    self.isSaving(false);
                    self.cancel();

                    // Refresh our packages when we view them to make sure we have the latest variable names
                    parent.parent.packagesVM.requiresRefreshOnView = true;
                },
                error: function () { self.isSaving(false); }
            });
        }
    };

    self.cancel = function () {
        $('#editVariableModal').foundation('reveal', 'close');
        self.variable(null);
    }

    function removeFromParent() {
        parent.variables.remove(function (e) { return e.id == self.variable().id(); });
    }
}