﻿function AdminViewModel() {
    // Data
    var self = this;
    self.dashboardVM = new DashboardViewModel(self);
    self.environmentsVM = new EnvironmentsViewModel(self);
    self.variablesVM = new VariablesViewModel(self);
    self.packagesVM = new PackagesViewModel(self);
    self.accountVM = new AccountViewModel(self);

    self.vms = { Dashboard: self.dashboardVM, Environments: self.environmentsVM, Variables: self.variablesVM, Packages: self.packagesVM, Account: self.accountVM };
    self.tabs = [];
    for (var key in self.vms) self.tabs.push(key);

    self.selectedTabId = ko.observable();
    self.loading = ko.observable(false);

    // Behaviours    
    self.goToTab = function (tab) { location.hash = tab };

    // Client-side routes    
    Sammy(function () {
        this.get('#:tab', function () {
            self.selectedTabId(this.params.tab);
            self.vms[this.params.tab].onTabSelected();
        });

        // Show dashboard by default
        this.get('', function () {
            this.app.runRoute('get', '#Dashboard');
        });

    }).run();
}

ko.applyBindings(new AdminViewModel());

function toastSaveSuccess(message) {
    var options = {
        duration: 1500,
        sticky: false,
        type: 'success' // or 'info', 'danger'
    };

    $.toast(message, options);
}