﻿var Account = 'Account';
function AccountViewModel(parent) {
    var self = this;
    var parent = parent;
    var selectedAuditEntryId;

    self.onTabSelected = function () {
        loadAccountInfo();
    };

    self.tabs = ko.observableArray([{ id: 'status', name: 'Account Status' }, { id: 'users', name: 'Users' }, { id: 'apikey', name: 'API Key' } ]);
    self.subitem = ko.observable();
    self.account = ko.observable();
    self.account.subscribe(function (a) {
        a.userUsage = ((a.userMax() == 0 ? 0.5 : (a.userCount() / a.userMax())) * 100) + '%';
        a.environmentUsage = ((a.environmentMax() == 0 ? 1 : (a.environmentCount() / a.environmentMax())) * 100) + '%';
        a.variableUsage = ((a.variableMax() == 0 ? 1 : (a.variableCount() / a.variableMax())) * 100) + '%';
        a.packageUsage = ((a.packageMax() == 0 ? 1 : (a.packageCount() / a.packageMax())) * 100) + '%';
        a.isUpgradeable = a.accountType() < 3;
    });
    self.users = ko.observableArray([]);
    self.users.subscribe(function (users) {
        for (var i = 0; i < users.length; i++) {
            users[i].lastLoginFormatted = users[i].lastLogin == null ? '(Never)' : AppConfig.dateUtil.serverFormat(users[i].lastLogin);
            users[i].resetting = false;
            users[i] = ko.mapping.fromJS(users[i]);
        }
    });
    self.isRegenerating = ko.observable(false);
    self.regenerateApiKey = function () {
        if (confirm('Are you sure? You will need to update your deployment scripts to use the newly generated ApiKey!')) {
            self.isRegenerating(true);
            $.ajax("/api/account/regenerate", {
                data: {},
                type: "post",
                success: function (result) {
                    self.isRegenerating(false);
                    self.account().apiKey(result);
                }
            });
        }
    };

    self.isUpdatingLicenseKey = ko.observable(false);
    self.updateLicenseKey = function () {
        self.isUpdatingLicenseKey(true);
        $.ajax("/api/account/license", {
            contentType: "application/json",
            data: ko.mapping.toJSON({ license: self.account().licenseKey() }),
            type: "post",
            success: function (result) {
                self.isUpdatingLicenseKey(false);
                self.account(ko.mapping.fromJS(result));
            }
        });
    };

    self.newUser = function () {
        self.editUser({ id: null, emailAddress: '' });
    }

    self.editUser = function (e) {
        self.editUserVM.user(ko.mapping.fromJS(e));
    }

    self.deleteUser = function (user) {
        if (confirm("Are you sure you want to delete this user?")) {
            $.ajax("/api/account/users/" + user.id(), {
                type: "delete", contentType: "application/json",
                success: function (result) {
                    self.users.remove(function (e) { return e.id() == user.id(); });
                    self.account().userCount(self.account().userCount() - 1);
                    self.account.valueHasMutated();
                },
                error: function (response, errorCode, errorMessage) {
                    alert(AppConfig.formUtil.getFirstError(response));
                }
            });
        }
    }

    self.editUserVM = new EditUserViewModel(self);
    self.upgradeAccountVM = new UpgradeAccountViewModel(self);
       
    function loadAccountInfo() {
        // load our account data
        $.ajax("/api/account", {
            contentType: "application/json",
            success: function (response) {
                self.account(ko.mapping.fromJS(response));
                if (self.subitem() == null) self.subitem(self.tabs()[0]);
            }
        });

        $.ajax("/api/account/users", {
            contentType: "application/json",
            success: self.users
        });        
    }
    loadAccountInfo();
}

function EditUserViewModel(parent) {
    var self = this;
    var parent = parent;

    self.saveErrors = ko.observableArray([]);
    self.isSaving = ko.observable(false);
    self.user = ko.observable();
    self.user.subscribe(function (e) {
        if (!e) return;
        e.isNew = ko.computed(function () {
            return self.user() != null && self.user().id() == null;
        }, e);
    });

    self.save = function () {
        var isUpdate = self.user().id() != null;
        var url = "/api/account/users";
        if (isUpdate) url += "/" + self.user().id();

        self.saveErrors([]);
        self.isSaving(true);
        $.ajax(url, {
            data: ko.mapping.toJSON({ Model: self.user() }),
            type: (isUpdate ? "put" : "post"), contentType: "application/json",
            success: function (result) {
                self.isSaving(false);
                removeFromParent();
                parent.users.push(ko.mapping.fromJS(result));
                if (!isUpdate) {
                    parent.account().userCount(parent.account().userCount() + 1);
                    parent.account.valueHasMutated();
                }

                $('#editUserModal').foundation('reveal', 'close');
                toastSaveSuccess('User saved successfully.');
            },
            error: function (response, errorCode, errorMessage) {
                self.isSaving(false);
                self.saveErrors(AppConfig.formUtil.getErrors(response));
            }
        });
    }
    
    self.cancel = function () {
        $('#editUserModal').foundation('reveal', 'close');
        self.user(null);
    }

    function removeFromParent() {
        parent.users.remove(function (e) { return e.id == self.user().id(); });
    }
}

function UpgradeAccountViewModel(parent) {
    var self = this;
    var parent = parent;
    
    self.isSaving = ko.observable(false);
    self.isRequestSent = ko.observable(false);
    self.targetUpgradePackageId = ko.observable();
    self.availableUpgradePackages = ko.observableArray([]);

    parent.account.subscribe(function (a) {
        if (a) {
            self.availableUpgradePackages([]);
            if (a.accountType() < 2) {
                self.availableUpgradePackages.push({ packageName: 'Bolt', packageId: 2 });
            }
            self.availableUpgradePackages.push( { packageName: 'Storm', packageId: 3 } );
        }
    });

    self.requestAccountUpgrade = function () {
        self.isSaving(true);
        $.ajax("/api/account/upgrade", {
            data: ko.mapping.toJSON({ targetPackageId: self.targetUpgradePackageId() }),
            contentType: "application/json",
            type: "put",
            success: function (result) {
                self.isSaving(false);
                self.isRequestSent(true);
                setTimeout(function () {
                    self.cancel();
                    setTimeout(function () { self.isRequestSent(false); }, 2000);
                }, 2000);
            },
            error: function (response, errorCode, errorMessage) {
                self.isSaving(false);
                alert(AppConfig.formUtil.getFirstError(response));
            }
        });
    }

    self.cancel = function () {
        $('#upgradeAccountModal').foundation('reveal', 'close');
    }
}
