﻿var Environments = 'Environments';
function EnvironmentsViewModel(parent) {
    var self = this;
    var parent = parent;

    self.onTabSelected = function () {}

    self.editEnvironmentVM = new EditEnvironmentViewModel(self);

    self.environments = ko.observableArray([]);
    self.environments.subscribe(function (e) {
        e.sort(function (e1, e2) { return ("" + e1.name).toLowerCase() > ("" + e2.name).toLowerCase(); });
    });
    
    self.newEnvironment = function () {
        self.editEnvironment({ id: null, name: '', description: '' });
    }

    self.editEnvironment = function (e) {
        self.editEnvironmentVM.environment(ko.mapping.fromJS(e));
    }

    // load our environments
    $.ajax("/api/environments", {
        contentType: "application/json",
        success: self.environments
    });
}

function EditEnvironmentViewModel(parent) {
    var self = this;
    var parent = parent;
    
    self.saveErrors = ko.observableArray([]);
    self.isSaving = ko.observable(false);
    self.environment = ko.observable();
    self.environment.subscribe(function (e) {
        if (!e) return;
        e.isNew = ko.computed(function () {
            return self.environment() != null && self.environment().id() == null;
        }, e);
    });

    self.save = function () {
        var isUpdate = self.environment().id() != null;
        var url = "/api/environments";
        if (isUpdate) url += "/" + self.environment().id();

        self.saveErrors([]);
        self.isSaving(true);
        $.ajax(url, {
            data: ko.mapping.toJSON({ Model: self.environment() }),
            type: (isUpdate ? "put" : "post"), contentType: "application/json",
            success: function (result) {
                self.isSaving(false);
                removeFromParent();
                parent.environments.push(result);
                
                $('#editEnvironmentModal').foundation('reveal', 'close');
                toastSaveSuccess('Environment saved successfully.');
            },
            error: function (response, errorCode, errorMessage) {
                self.isSaving(false);
                var errors = $.ss.getErrors(response);
                for (var k in errors) {
                    self.saveErrors().push(k + ": " + errors[k]);
                }
                self.saveErrors.valueHasMutated();
            }
        });
    }

    self.deleteEnvironment = function () {
        if (confirm("Deleting this environment will also remove all variable settings you have configured for this environment. Are you sure you want to delete this environment? ")) {
            self.isSaving(true);
            $.ajax("/api/environments/" + self.environment().id(), {
                type: "delete", contentType: "application/json",
                success: function (result) {
                    removeFromParent();
                    self.isSaving(false);
                    self.cancel();
                },
                error: function () { self.isSaving(false); }
            });
        }
    };

    self.cancel = function () {
        $('#editEnvironmentModal').foundation('reveal', 'close');
        self.environment(null);
    }

    function removeFromParent() {
        parent.environments.remove(function (e) { return e.id == self.environment().id(); });
    }

}