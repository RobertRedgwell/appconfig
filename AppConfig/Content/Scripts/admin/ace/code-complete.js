﻿var keys = {
    UNKNOWN: 0,
    SHIFT: 16,
    CTRL: 17,
    ALT: 18,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    DEL: 46,
    TAB: 9,
    RETURN: 13,
    ESC: 27,
    COMMA: 188,
    PAGEUP: 33,
    PAGEDOWN: 34,
    BACKSPACE: 8,
    SPACE: 32
};

function codeComplete(editor, delimiterGroupPairs) {

    $('.auto-complete-dropdown').remove();
    var self = this;
    self.editor = editor;
    self.delimiterGroupPairs = delimiterGroupPairs;
    for (var i = 0; i < self.delimiterGroupPairs.length; i++) {
        var gp = self.delimiterGroupPairs[i];
        if (!gp.suffix) gp.suffix = "";
        for (var j = 0; j < gp.values.length; j++) {
            gp.values[j] = { value: gp.values[j], suffix: gp.suffix };
        }
    }
    
    self.dropdown = $('<select size="20"/>')
        .addClass('auto-complete-dropdown')
        .keydown(onDropdownKeyDown)
        .appendTo($('body'));

    self.autoComplete = function() {
        var pos = self.editor.getCursorPosition();
        var line = self.editor.getSession().getDocument().getLine(pos.row);
        var pairs = [];
        for (var i = 0; i < self.delimiterGroupPairs.length; i++) {
            var delimiter = self.delimiterGroupPairs[i].delimiter;
            var lineSubstring = line.substring(pos.column - delimiter.length, pos.column);
            if (delimiter == lineSubstring) {
                pairs = pairs.concat(self.delimiterGroupPairs[i].values);
            }
        }

        if (pairs.length > 0) {

            pairs.sort(function (a, b) {
                if (a.value < b.value)
                    return -1;
                if (a.value > b.value)
                    return 1;
                return 0;
            });

            var cursorPos = self.editor.getCursorPosition();
            var coords = self.editor.renderer.textToScreenCoordinates(cursorPos.row, cursorPos.column);

            self.dropdown.empty().css({ top: coords.pageY + 20 + $(document).scrollTop(), left: coords.pageX });

            for (var i = 0; i < pairs.length; i++) {
                self.dropdown.append($('<option data-suffix="' + escape(pairs[i].suffix) + '">' + pairs[i].value + '</option>'));
            }
            self.dropdown.children('option:first').attr("selected", "selected");
            self.dropdown.show().focus();
        }
    }

    self.editor.on("change", function () { setTimeout(self.autoComplete, 1); });

    self.editor.commands.addCommand({
        name: "auto-complete",
        bindKey: { win: "Ctrl-Space", mac: "Command-Space" },
        exec: self.autoComplete
    });

    function onDropdownKeyDown(e) {
        switch (e.keyCode) {
            case keys.RETURN:
            case keys.SPACE:
                // Add our selected value into the editor
                var selected = self.dropdown.children('option:selected');
                self.editor.insert(selected.val());

                // Figure out if we need to add our suffix:
                var pos = self.editor.getCursorPosition();
                var line = self.editor.getSession().getDocument().getLine(pos.row);
                var nextChar = line.substring(pos.column, pos.column + 1);
                var suffix = unescape(selected.attr('data-suffix'));
                if (nextChar != suffix) {
                    self.editor.insert(suffix);
                }
                e.preventDefault();
                e.stopPropagation();
                self.dropdown.hide();
                self.editor.focus();
                break;
            case keys.ESC:
            case keys.BACKSPACE:
            case keys.DEL:
                e.preventDefault();
                e.stopPropagation();
                self.dropdown.hide();
                self.editor.focus();
                break;
        }
    }
}