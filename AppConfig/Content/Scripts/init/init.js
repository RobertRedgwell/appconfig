﻿// page init
$(document).ready(function () {
    // Initialize Foundation plugins
    $(document).foundation();

    // Wire up Ajaxable forms etc
    AppConfig.ajaxForm.init();

    $.fn.asuggest.defaults.delimiters = "${";
    $.fn.asuggest.defaults.minChunkSize = 1;
    $.fn.asuggest.defaults.cycleOnTab = true;
    $.fn.asuggest.defaults.endingSymbols = "}";

    $.toast.config.align = 'right';
    $.toast.config.width = 400;
});

var _is403Handled = false;
$(document).ajaxError(function (a, b, c) {
    if (b.status == 403) {
        if (_is403Handled) return;
        _is403Handled = true;
        alert('Your session has expired. We will now redirect you to the login page.');
        document.location = '/Login';
    }
});