﻿var AppConfig;

(function (AppConfig) {
    var ajaxForm = (function () {

        // Page initialization logic
        function init() {
            // Wire up Ajaxable forms
            $('.ajax-submit').each(function () {
                var form = $(this);
                var submitButton = form.find('input[type=submit],button[type=submit]');

                form.ajaxForm({
                    beforeSubmit: function (arr, $form, options) {
                        AppConfig.formUtil.disableButton(submitButton);
                        return true;
                    },
                    success: function (response) {
                        form.clearErrors();
                        // Check for redirect URL
                        if (response.sessionId) response.Url = '/Admin';
                        if (response.Url) {
                            window.location.href = response.Url;
                        } else {
                            AppConfig.formUtil.enableButton(submitButton);
                        }
                    },
                    error: function (response, r2, r3) {
                        AppConfig.formUtil.enableButton(submitButton);
                        form.applyErrors(response);
                    }
                });
            });
        }

        return {
            init: init
        };

    })();

    AppConfig.ajaxForm = ajaxForm;
})(AppConfig || (AppConfig = {}));

(function (AppConfig) {
    var formUtil = (function () {

        // disables the specified button, and sets its value to be that of the data-submitting attribute value
        function disableButton($buttons) {
            $buttons.each(function () {
                var b = $(this);
                // Disable the button
                b.attr("disabled", "disabled");
                // Grab the submitting text
                var submittingText = b.attr('data-submitting');
                if (!submittingText) submittingText = 'Submitting...';

                // store our original value, and set the text to the submitting text
                b.attr('data-original-value', b.val());
                b.val(submittingText);
            });
        }

        // re-enables the specified button, and sets its value if it was disabled using the disableButton function
        function enableButton($buttons) {
            $buttons.each(function () {
                var b = $(this);
                // Disable the button
                b.removeAttr("disabled");
                // Grab the original text
                var originalText = b.attr('data-original-value');
                if (originalText) {
                    // restore our original value
                    b.val(originalText);
                }
            });
        }

        function getErrors(response) {
            var errors = $.ss.getErrors(response);
            var result = [];
            for (var k in errors) {
                result.push(k + ": " + errors[k]);
            }
            return result;
        }

        function getFirstError(response) {
            var errors = $.ss.getErrors(response);
            if (errors && errors['error']) return errors['error'];

            return 'Unknown error';
        }
        

        return {
            disableButton: disableButton,
            enableButton: enableButton,
            getErrors: getErrors,
            getFirstError: getFirstError
        }

    })();

    AppConfig.formUtil = formUtil;
})(AppConfig || (AppConfig = {}));

(function (AppConfig) {
    var dateUtil = (function () {

        // disables the specified button, and sets its value to be that of the data-submitting attribute value
        function serverFormat(d) {
            var date = new Date(Date.parse(d));
            date.setHours(date.getHours() + date.getTimezoneOffset() / 60);
            var d = (date + '').split(' ');
            return [d[3], d[1], d[2], d[4]].join(' ');
        }

        return {
            serverFormat: serverFormat
        }

    })();

    AppConfig.dateUtil = dateUtil;
})(AppConfig || (AppConfig = {}));