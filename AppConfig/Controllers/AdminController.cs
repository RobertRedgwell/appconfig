﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AppConfig.Api.Model;
using AppConfig.Api.ViewModels;
using AppConfig.Api.Extensions;
using AppConfig.Core;
using AppConfig.Logic.Interfaces;
using ServiceStack.Mvc;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;

namespace AppConfig.Api.Controllers
{
    public class AdminController : ServiceStackController<AuthUserSession>
    {
        public override string LoginRedirectUrl { get { return "~/Login"; } }

        private readonly IConfiguration _config;

        public AdminController(IConfiguration config)
        {
            _config = config;
        }

        [Authenticate]
        public ActionResult Index()
        {
            return View(new AdminViewModel
            {
                DocumentationUrl = _config.AppSettings["MainSiteBaseUrl"] + "Docs"
            });
        }

        public ActionResult Login()
        {
            return View(new LoginViewModel
            {
                GetStartedUrl = _config.AppSettings["MainSiteBaseUrl"] + "GetStarted"

            });
        }
    }
}