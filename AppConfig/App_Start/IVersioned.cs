﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppConfig.Api
{
    /// <summary>
    /// Marker interface to set an api request object as something that could be version aware
    /// </summary>
    public interface IVersioned { }
}
