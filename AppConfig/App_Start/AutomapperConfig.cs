﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Api.Model;
using AppConfig.Api.Services;
using AppConfig.Model;
using AutoMapper;

namespace AppConfig.Api
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.CreateMap<EnvironmentDto, AppConfig.Model.Environment>().ReverseMap();
            Mapper.CreateMap<PackageDto, Package>().ReverseMap();
            Mapper.CreateMap<UserDto, User>().ReverseMap();
            Mapper.CreateMap<VariableDto, Variable>().ReverseMap();
            Mapper.CreateMap<VariableEnvironmentDto, VariableEnvironment>().ReverseMap();
            
       }
    }
}