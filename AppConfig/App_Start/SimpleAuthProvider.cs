﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence;
using AppConfig.Persistence.Interfaces;
using ServiceStack.CacheAccess;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;

namespace AppConfig.Api
{
    public class SimpleAuthProvider : CredentialsAuthProvider
    {
        private readonly IUserRepo _userRepo;
        private readonly IAccountLogic _accountLogic;

        public SimpleAuthProvider(IUserRepo userRepo, IAccountLogic accountLogic)
        {
            _userRepo = userRepo;
            _accountLogic = accountLogic;
        }

        public override bool TryAuthenticate(IServiceBase authService, string userName, string password)
        {
            return (!string.IsNullOrEmpty(userName) &&
                _userRepo.GetByEmailAndPassword(userName, password) != null);
        }

        public override void OnAuthenticated(IServiceBase authService, IAuthSession session, IOAuthTokens tokens, Dictionary<string, string> authInfo)
        {
            //Important: You need to save the session!
            var user = _userRepo.GetByEmail(session.UserAuthName);
            _userRepo.SetLastLoginNow(user.Id, user.AccountId);
            session.UserAuthId = user.Id.ToString();
            session.Email = user.EmailAddress;
            session.UserAuthName = user.AccountId.ToString(); // Mungification
            session.IsAuthenticated = true;
            authService.SaveSession(session, SessionExpiry);

            _accountLogic.BeginLicenseValidation(user.AccountId);
        }
    }
}