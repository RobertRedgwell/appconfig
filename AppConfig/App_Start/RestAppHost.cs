﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using AppConfig.Api.Exceptions;
using AppConfig.Api.Extensions;
using Ninject;
using NLog;
using ServiceStack.Configuration;
using ServiceStack.ContainerAdapter.Ninject;
using ServiceStack.Mvc;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;

namespace AppConfig.Api
{
    public class RestAppHost : AppHostBase
    {
        public static readonly string VersionHeaderKey = "version";
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Setup Service Stack with the name of the application and where to find the web services
        /// </summary>
        public RestAppHost() 
            : base("AppConfig RestServices", typeof(RestAppHost).Assembly)
        { 
        }

        public override void Configure(Funq.Container container)
        {
            Kernel = new StandardKernel();
            Kernel.Load("AppConfig.*.dll"); // Load all modules defined in all dlls

            container.Adapter = new NinjectContainerAdapter(Kernel);
            ControllerBuilder.Current.SetControllerFactory(new FunqControllerFactory(container));

            ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;
            ServiceStack.Text.JsConfig.DateHandler = JsonDateHandler.ISO8601;

            ServiceExceptionHandler = HandleServiceException;
            
            ConfigureForVersioning();

            Plugins.Add(
                new AuthFeature(() => new AuthUserSession(), new IAuthProvider[] { Kernel.Get<IAuthProvider>(), Kernel.Get<IApiKeyProvider>() })
            );
        }

        /// <summary>
        /// The current latest version of your services
        /// </summary>
        public Version LatestVersion { get; private set; }

        public IKernel Kernel { get; private set; }

        /// <summary>
        /// This method must be called from your Configure(...) method to enable the version aware binding
        /// </summary>
        private void ConfigureForVersioning()
        {
            // Grab all concrete types that implement IVersioned
            var versionedType = typeof(IVersioned);
            var versionableRequestFilterTypes = new List<Type>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                versionableRequestFilterTypes.AddRange(
                    from t in assembly.GetTypes()
                    where !t.IsAbstract && versionedType.IsAssignableFrom(t)
                    select t);
            }

            foreach (var type in versionableRequestFilterTypes)
            {
                Log.Info(() => "Registering type as versionable: " + type.FullName);
                this.RequestBinders.Add(type, req => BindRequestType(type, req));
            }
        }

        /// <summary>
        /// When a request body is automatically binded to a type, we need to check if the request is intending to send us an old version.
        /// If an old version is found, we deserialize our request into the old version.
        /// </summary>
        /// <param name="defaultType"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public object BindRequestType(Type defaultType, IHttpRequest request)
        {
            if (defaultType == null) throw new ArgumentNullException("defaultType");
            if (request == null) throw new ArgumentNullException("request");

            var version = request.GetVersionHeader(LatestVersion);
            if (version == LatestVersion)
            {
                return null; // Fall back to default input DTO
            }

            var resolvedTypeName = string.Format("{0}.{1}V{2}_{3}, {4}",
                defaultType.Namespace,
                defaultType.Name,
                version.Major,
                version.Minor,
                Assembly.GetCallingAssembly().FullName);

            var resolvedType = Type.GetType(resolvedTypeName);
            if (resolvedType == null)
            {
                throw new UnsupportedVersionException(String.Format("Version {0} is not supported", version));
            }

            var deserializerMethod = this.ContentTypeFilters.GetStreamDeserializer(request.ContentType);
            if (deserializerMethod == null)
            {
                throw new UnsupportedVersionException(String.Format("Version {0} has no deserializer", version));
            }
            object requestFilter = deserializerMethod.Invoke(resolvedType, request.InputStream);

            return requestFilter;
        }

        private object HandleServiceException(object request, Exception ex)
        {
            Log.Error("A service exception occurred", ex);
            return DtoUtils.HandleException(this, request, ex);
        }
    }
}