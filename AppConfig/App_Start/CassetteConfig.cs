﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace AppConfig.Api.App_Start
{
    public class CassetteConfig : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.Add<StylesheetBundle>("Content/Css");

            bundles.Add<ScriptBundle>("Content/Scripts", new FileSearch
            {
                SearchOption = SearchOption.AllDirectories,
                Pattern = "*.js",
                Exclude = new Regex(@"admin.*")
            });

            bundles.Add<ScriptBundle>("Content/Scripts/admin", new [] {
                "ace/ace.js",
                "ace/mode-xml.js",
                "ace/theme-clouds.js",
                "ace/code-complete.js",
                "admin.dashboard.js",
                "admin.environments.js",
                "admin.packages.js",
                "admin.variables.js",
                "admin.account.js",
                "admin.js"
            });
        }
    }
}