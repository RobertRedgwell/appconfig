﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Persistence.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;

namespace AppConfig.Api
{
    public class ApiKeyProvider : AuthProvider, IApiKeyProvider
    {
        public const string ProviderName = "ApiKeyProvider";
        public const string ApiKeyHeader = "ApiKey";

        private readonly IAccountRepo _accountRepo;

        public ApiKeyProvider(IAccountRepo accountRepo)
        {
            Provider = ProviderName;
            _accountRepo = accountRepo;
        }

        public override bool IsAuthorized(IAuthSession session, IOAuthTokens tokens, Auth request = null)
        {
            var headers = HttpContext.Current.Request.Headers;
            if (headers == null || headers.Count == 0) throw new UnauthorizedAccessException("Missing ApiKey header");
            var apiKey = headers[ApiKeyHeader];
            if (apiKey == null) throw new UnauthorizedAccessException("Missing ApiKey header");

            // Load the account with the specified api key
            var account = _accountRepo.GetByApiKey(apiKey);
            if (account == null) throw new UnauthorizedAccessException("Invalid ApiKey");

            session.UserAuthName = account.Id.ToString(); // Mungification
            session.IsAuthenticated = true;
            
            return true;
        }

        public override object Authenticate(IServiceBase authService, IAuthSession session, Auth request)
        {
            throw new NotImplementedException();
        }
    }

    public interface IApiKeyProvider : IAuthProvider
    {
    }
}