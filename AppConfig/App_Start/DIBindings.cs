﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Core;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.CacheAccess;
using ServiceStack.CacheAccess.Providers;

namespace AppConfig.Api
{
    public class DIBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IAuthProvider>().To<SimpleAuthProvider>().InSingletonScope();
            Bind<IApiKeyProvider>().To<ApiKeyProvider>().InSingletonScope();
            Bind<ICacheClient>().ToConstant(new MemoryCacheClient() { FlushOnDispose = false }).InSingletonScope();
        }
    }
}