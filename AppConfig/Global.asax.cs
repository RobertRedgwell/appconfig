﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;
using AppConfig.Api;
using ServiceStack.Logging;
using ServiceStack.Logging.NLogger;
using System.Web.Http.Dependencies;

namespace AppConfig.Api
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            LogManager.LogFactory = new NLogFactory();

            var host = new RestAppHost();
            host.Init();

            DependencyResolver.SetResolver(new NinjectMvcResolver(host.Kernel));

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            AutoMapperConfig.Configure();
        }
    }

    /// <summary>
    /// Provides a Ninject implementation of IDependencyScope which resolves services using the Ninject container.
    /// </summary>
    public class NinjectScope : IDependencyScope
    {
        protected IResolutionRoot resolutionRoot;

        public NinjectScope(IResolutionRoot kernel)
        {
            resolutionRoot = kernel;
        }

        public object GetService(Type serviceType)
        {
            if (resolutionRoot == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            return resolutionRoot.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (resolutionRoot == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            return resolutionRoot.GetAll(serviceType);
        }

        public void Dispose()
        {
            // Don't dispose anything because we want to retain across requests
        }
    }

    /// <summary>
    /// Resolver for MVC controllers 
    /// This class is the resolver, but it is also the global scope so we derive from NinjectScope.    
    /// </summary>
    public class NinjectMvcResolver : NinjectScope, System.Web.Mvc.IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectMvcResolver(IKernel kernel)
            : base(kernel)
        {
            _kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }
    }
}