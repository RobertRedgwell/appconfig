﻿using AppConfig.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using AppConfig.Persistence.Interfaces;
using AppConfig.Api.Extensions;
using System.ComponentModel.DataAnnotations;
using AppConfig.Model;
using AppConfig.Logic.Interfaces;
using AutoMapper;

namespace AppConfig.Api.Services
{
    [Authenticate]
    [Route("/environments", "GET")]
    public class ListEnvironmentsRequest : BaseListRequest<EnvironmentDto> { }

    [Authenticate]
    [Route("/environments", "POST")]
    public class CreateEnvironmentRequest : BaseCreateRequest<EnvironmentDto> { }

    [Authenticate]
    [Route("/environments/{Id}", "PUT")]
    public class UpdateEnvironmentRequest : BaseUpdateRequest<EnvironmentDto> { }

    [Authenticate]
    [Route("/environments/{Id}", "DELETE")]
    public class DeleteEnvironmentRequest : BaseDeleteRequest { }

    [Authenticate(Provider = ApiKeyProvider.ProviderName)]
    [Route("/environments/{EnvironmentName}/configure", "POST")]
    public class ConfigureRequest : IReturn<ConfigureResponse>
    {
        /// <summary>
        /// The name of the environment that is being configured
        /// </summary>
        [Required]
        public string EnvironmentName { get; set; }

        /// <summary>
        /// A Desciptor identifying the core package being deployed
        /// </summary>
        [Required]
        public PackageDescriptor CorePackage { get; set; }

        /// <summary>
        /// A list of packages that the core package is dependent on
        /// </summary>
        [Required]
        public List<PackageDescriptor> DependentPackages { get; set; }

        /// <summary>
        /// The contents of the configuration file of the application that's being deployed
        /// </summary>
        [Required]
        public string InputConfig { get; set; }
    }

    public class ConfigureResponse
    {
        public string OutputConfig { get; set; }
    }

    public class EnvironmentsService : Service
    {
        private readonly IEnvironmentLogic _environmentLogic;
        private readonly IConfigurationTransformer _configurationTransformer;

        public EnvironmentsService(IEnvironmentLogic environmentLogic, IConfigurationTransformer configurationTransformer)
        {
            _environmentLogic = environmentLogic;
            _configurationTransformer = configurationTransformer;
        }

        public List<EnvironmentDto> Get(ListEnvironmentsRequest request)
        {
            return (from e in _environmentLogic.GetListForAccount(this.GetSession().GetAccountId())
                    select Mapper.Map<EnvironmentDto>(e))
                   .ToList();
        }

        public EnvironmentDto Post(CreateEnvironmentRequest request)
        {
            return Save(request.Model);
        }

        public EnvironmentDto Put(UpdateEnvironmentRequest request)
        {
            request.Model.Id = request.Id; // make sure the model's id is set
            return Save(request.Model);
        }

        public bool Delete(DeleteEnvironmentRequest request)
        {
            return _environmentLogic.DeleteForAccount(request.Id, this.GetSession().GetAccountId(), this.GetSession().GetUserId());
        }

        public ConfigureResponse Post(ConfigureRequest request)
        {
            // TODO: validate request
            return new ConfigureResponse
            {
                OutputConfig = _configurationTransformer.DoTransformation(
                    this.GetSession().GetAccountId(),
                    request.CorePackage, 
                    request.DependentPackages, 
                    request.EnvironmentName, 
                    request.InputConfig)
            };
        }

        private EnvironmentDto Save(EnvironmentDto dto)
        {
            Validator.ValidateObject(dto, new ValidationContext(dto, null, null), true);

            var model = Mapper.Map<AppConfig.Model.Environment>(dto);
            _environmentLogic.SaveForAccount(
                model,
                this.GetSession().GetAccountId(),
                this.GetSession().GetUserId());

            return new EnvironmentDto(model);
        }
    }
}