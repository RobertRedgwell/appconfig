﻿using AppConfig.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using AppConfig.Persistence.Interfaces;
using AppConfig.Api.Extensions;
using System.ComponentModel.DataAnnotations;
using AppConfig.Model;

namespace AppConfig.Api.Services
{
    [Authenticate]
    [Route("/dashboard/{Page}", "GET")]
    public class ListDashboardItemsRequest : IReturn<PagedList<AuditEntry>>
    {
        public int Page { get; set; }
    }

    public class DashboardService : BaseCrudService<IAuditEntryRepo, AuditEntry>
    {
        public const int ItemsPerPage = 10;

        public DashboardService(IAuditEntryRepo auditEntryRepo)
            : base(auditEntryRepo)
        {
        }

        public PagedList<AuditEntry> Get(ListDashboardItemsRequest request)
        {
            return _repo.GetListForAccount(
                this.GetSession().GetAccountId(), 
                ItemsPerPage, 
                request.Page,
                false, 
                a => a.When);
        }
    }
}