﻿using AppConfig.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using AppConfig.Persistence.Interfaces;
using AppConfig.Api.Extensions;
using AppConfig.Model;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using ServiceStack.ServiceInterface.ServiceModel;
using AppConfig.Logic.Interfaces;

namespace AppConfig.Api.Services
{
    [Authenticate]
    [Route("/account", "GET")]
    public class GetAccountRequest : IReturn<AccountDto> { }

    [Authenticate]
    [Route("/account/upgrade", "PUT")]
    public class AccountUpgradeRequest : IReturn<bool> 
    {
        [Range(2, 3, ErrorMessage="Please select a plan to upgrade to")]
        public int TargetPackageId { get; set; }
    }

    [Authenticate]
    [Route("/account/regenerate", "POST")]
    public class RegenerateApiKeyRequest : IReturn<string> { }

    [Authenticate]
    [Route("/account/license", "POST")]
    public class SetLicenseRequest : IReturn<AccountDto> 
    {
        public string License { get; set; }
    }

    [Authenticate]
    [Route("/account/users", "GET")]
    public class GetAccountUsersRequest : IReturn<List<UserDto>> { }

    [Authenticate]
    [Route("/account/users", "POST")]
    public class CreateUserRequest : BaseCreateRequest<UserDto> { }

    [Authenticate]
    [Route("/account/users/{Id}", "DELETE")]
    public class DeleteUserRequest: BaseDeleteRequest { }

    public class AccountsService : Service
    {
        private readonly IAccountLogic _accountLogic;
        private readonly IUserLogic _userLogic;

        public AccountsService(IAccountLogic accountLogic, IUserLogic userLogic)
        {
            _accountLogic = accountLogic;
            _userLogic = userLogic;
        }

        public AccountDto Get(GetAccountRequest request)
        {
            return new AccountDto(
                _accountLogic.GetAccountDetails(this.GetSession().GetUserId(), this.GetSession().GetAccountId()));
        }

        public List<UserDto> Get(GetAccountUsersRequest request)
        {
            return (from u in _userLogic.GetListForAccount(this.GetSession().GetAccountId())
                    select new UserDto(u))
                    .ToList();
        }

        public bool Put(AccountUpgradeRequest request)
        {
            Validator.ValidateObject(request, new ValidationContext(request, null, null), true);

            _accountLogic.RequestAccountUpgrade(
                this.GetSession().GetUserId(),
                this.GetSession().GetAccountId(),
                (EAccountType)request.TargetPackageId);

            return true;
        }

        public string Post(RegenerateApiKeyRequest request)
        {
            return _accountLogic.RegenerateApiKey(this.GetSession().GetUserId(), this.GetSession().GetAccountId());
        }

        public AccountDto Post(SetLicenseRequest request) 
        {
            _accountLogic.BeginLicenseValidation(this.GetSession().GetAccountId(), false, request.License);
            return new AccountDto(
                _accountLogic.GetAccountDetails(this.GetSession().GetUserId(), this.GetSession().GetAccountId()));
        }

        public UserDto Post(CreateUserRequest request)
        {
            var user = _userLogic.CreateUser(request.Model.EmailAddress, this.GetSession().GetAccountId(), this.GetSession().GetUserId());
            return new UserDto(user);
        }

        public bool Delete(DeleteUserRequest request)
        {
            if (request.Id == this.GetSession().GetUserId()) throw new ValidationException("You cannot delete your own user account.");
            return _userLogic.DeleteForAccount(request.Id, this.GetSession().GetAccountId(), this.GetSession().GetUserId());
        }
    }
}