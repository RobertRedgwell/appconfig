﻿using AppConfig.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using AppConfig.Persistence.Interfaces;
using AppConfig.Api.Extensions;
using AppConfig.Model;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using AppConfig.Logic.Interfaces;

namespace AppConfig.Api.Services
{
    [Authenticate]
    [Route("/packages", "GET")]
    public class ListPackagesRequest : BaseListRequest<PackageDto> { }

    [Authenticate]
    [Route("/packages", "POST")]
    public class CreatePackageRequest : BaseCreateRequest<PackageDto> { }

    [Authenticate]
    [Route("/packages/{Id}", "PUT")]
    public class UpdatePackageRequest : BaseUpdateRequest<PackageDto> { }

    [Authenticate]
    [Route("/packages/{Id}", "DELETE")]
    public class DeletePackageRequest : BaseDeleteRequest { }

    public class PackagesService : Service
    {
        private readonly IPackageLogic _packageLogic;
        
        public PackagesService(IPackageLogic packageLogic)
        {
            _packageLogic = packageLogic;
        }

        public List<PackageDto> Get(ListPackagesRequest request)
        {
            return (from p in _packageLogic.GetListForAccount(this.GetSession().GetAccountId())
                    select new PackageDto(p))
                   .ToList(); 
        }

        public PackageDto Post(CreatePackageRequest request)
        {
            return Save(request.Model);
        }

        public PackageDto Put(UpdatePackageRequest request)
        {
            request.Model.Id = request.Id; // make sure the model's id is set
            return Save(request.Model);
        }

        public bool Delete(DeletePackageRequest request)
        {
            return _packageLogic.DeleteForAccount(request.Id, this.GetSession().GetAccountId(), this.GetSession().GetUserId());
        }

        private PackageDto Save(PackageDto dto)
        {
            Validator.ValidateObject(dto, new ValidationContext(dto, null, null), true);

            var model = Mapper.Map<Package>(dto);
            _packageLogic.SaveForAccount(
                model, 
                this.GetSession().GetAccountId(), 
                this.GetSession().GetUserId());

            return new PackageDto(model);
        }

    }
}