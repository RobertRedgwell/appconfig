﻿using AppConfig.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using AppConfig.Persistence.Interfaces;
using AppConfig.Api.Extensions;
using AppConfig.Model;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using ServiceStack.ServiceInterface.ServiceModel;
using AppConfig.Logic.Interfaces;

namespace AppConfig.Api.Services
{
    [Authenticate]
    [Route("/variables", "GET")]
    public class GetVariableRequest : BaseListRequest<VariableDto> { }

    [Authenticate]
    [Route("/variables", "POST")]
    public class CreateVariableRequest : BaseCreateRequest<VariableDto> { }

    [Authenticate]
    [Route("/variables/{Id}", "PUT")]
    public class UpdateVariableRequest : BaseUpdateRequest<VariableDto> { }

    [Authenticate]
    [Route("/variables/{Id}", "DELETE")]
    public class DeleteVariableRequest : BaseDeleteRequest { }

    public class VariablesService : Service
    {
        private readonly IVariableLogic _variableLogic;

        public VariablesService(IVariableLogic variableLogic)
        {
            _variableLogic = variableLogic;
        }

        public List<VariableDto> Get(GetVariableRequest request)
        {
            return (from v in _variableLogic.GetListForAccount(this.GetSession().GetAccountId())
                    select ToDto(v))
                   .ToList();
        }

        public VariableDto Post(CreateVariableRequest request)
        {
            // TODO: validate request

            return Save(request.Model);
        }

        public VariableDto Put(UpdateVariableRequest request)
        {
            // TODO: validate request

            request.Model.Id = request.Id; // make sure the model's id is set
            return Save(request.Model);
        }

        public bool Delete(DeleteVariableRequest request)
        {
            return _variableLogic.DeleteForAccount(request.Id, this.GetSession().GetAccountId(), this.GetSession().GetUserId());
        }

        private VariableDto Save(VariableDto dto)
        {
            Validator.ValidateObject(dto, new ValidationContext(dto, null, null), true);

            var model = Mapper.Map<Variable>(dto);
            var variableEnvironments = Mapper.Map<List<VariableEnvironment>>(
                from ve in dto.VariableEnvironments
                where ve.IsOverridden
                select ve);
            
            _variableLogic.SaveForAccount(model, 
                variableEnvironments,
                this.GetSession().GetAccountId(), 
                this.GetSession().GetUserId());

            return ToDto(model);
        }

        private VariableDto ToDto(Variable variable)
        {
            return new VariableDto(variable)
            {
                VariableEnvironments = (from ve in _variableLogic.GetForVariable(variable.Id, this.GetSession().GetAccountId())
                                        select new VariableEnvironmentDto(ve))
                                        .ToList(),
            };

        }
    }
}