﻿using AppConfig.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using AppConfig.Persistence.Interfaces;
using AppConfig.Api.Extensions;
using AppConfig.Model;

namespace AppConfig.Api.Services
{
    public abstract class BaseListRequest<TModel> : IReturn<List<TModel>>
    { }

    public abstract class BaseGetByIdRequest<TModel> : IReturn<TModel>
    {
        public long Id { get; set; }
    }

    public abstract class BaseCreateRequest<TModel> : IReturn<TModel>
    {
        public TModel Model { get; set; }
    }

    public abstract class BaseUpdateRequest<TModel> : IReturn<TModel>
    {
        public TModel Model { get; set; }
        public long Id { get; set; }
    }

    public abstract class BaseDeleteRequest : IReturn<bool>
    {
        public long Id { get; set; }
    }

    public abstract class BaseCrudService<TRepo, TModel> : Service
        where TRepo : IBaseAccountRepo<TModel>
        where TModel : BaseAccountModel
    {
        protected readonly TRepo _repo;

        protected BaseCrudService(TRepo repo)
        {
            _repo = repo;
        }
    }
}