﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Api;
using ServiceStack.ServiceHost;

namespace AppConfig.Api.Extensions
{
    public static class IHttpRequestExtensions
    {
        /// <summary>
        /// Gets the version from the request header, if present.
        /// Defaults to the specified defaultVersion if the version is missing, or invalid
        /// </summary>
        /// <param name="request"></param>
        /// <param name="defaultVersion"></param>
        /// <returns></returns>
        public static Version GetVersionHeader(this IHttpRequest request, Version defaultVersion)
        {
            if (request == null) throw new ArgumentNullException("request", "You should only call this extension method on an instance of an IHttpRequest");

            string versionHeaderValue = request.Headers == null ? string.Empty : request.Headers.Get(RestAppHost.VersionHeaderKey);
            Version version = null;
            if (string.IsNullOrEmpty(versionHeaderValue) || !Version.TryParse(versionHeaderValue, out version))
            {
                return defaultVersion;
            }
            return version;
        }
    }
}