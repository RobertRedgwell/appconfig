﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface.Auth;

namespace AppConfig.Api.Extensions
{
    public static class IAuthSessionExtensions
    {
        /// <summary>
        /// Gets the account id from the session.
        /// Right now, this is munged into the UserAuthName field...
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static int GetAccountId(this IAuthSession session)
        {
            var accountId = 0;
            int.TryParse(session.UserAuthName, out accountId);
            return accountId;
        }

        public static int GetUserId(this IAuthSession session)
        {
            var userId = 0;
            int.TryParse(session.UserAuthId, out userId);
            return userId;
        }
    }
}