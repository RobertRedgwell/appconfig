﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Api.Model;

namespace AppConfig.Api.Exceptions
{
    public class ConfigTransformResolutionException : ApplicationException
    {
        public ConfigTransformResolutionException(DeploymentEnvironment environment)
            : base("Error reading the config for the environment '" + environment + "'")
        {
        }
    }
}