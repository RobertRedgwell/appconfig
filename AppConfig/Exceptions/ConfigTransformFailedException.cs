﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Api.Model;

namespace AppConfig.Api.Exceptions
{
    public class ConfigTransformFailedException : ApplicationException
    {
        public ConfigTransformFailedException()
            : base("Error transforming the configuration xml")
        {
        }
    }
}