﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Api.Model;

namespace AppConfig.Api.Exceptions
{
    public class MissingEnvironmentFolderException : ApplicationException
    {
        public MissingEnvironmentFolderException(DeploymentEnvironment specifiedEnvironment)
            : base("There was no folder for the environment '" + specifiedEnvironment + "'.")
        {
        }
    }
}