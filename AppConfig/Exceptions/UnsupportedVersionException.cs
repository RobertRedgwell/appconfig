﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppConfig.Api.Exceptions
{
    public class UnsupportedVersionException : ApplicationException
    {
        public UnsupportedVersionException(string message)
            : base(message)
        {
        }
    }
}