﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Api.Model;

namespace AppConfig.Api.Exceptions
{
    public class InvalidEnvironmentException : ApplicationException
    {
        public InvalidEnvironmentException(DeploymentEnvironment specifiedEnvironment)
            : base("The environment '" + specifiedEnvironment + "' is not recognised.")
        {
        }
    }
}