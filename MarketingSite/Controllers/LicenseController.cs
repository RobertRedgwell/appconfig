﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using AppConfig.Marketing.Model;
using AppConfig.Logic.Exceptions;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using PoliteCaptcha;
using AppConfig.Marketing.Services;
using AppConfig.Core;
using MarketingSite.Services;
using MarketingSite.Extensions;

namespace AppConfig.Marketing.Controllers
{
    public class LicenseController : Controller
    {
        private readonly IConfiguration _config;
        private readonly ILicenseService _licenseService;

        public LicenseController(IConfiguration config, ILicenseService licenseService)
        {
            _config = config;
            _licenseService = licenseService;
        }

        /// <summary>
        /// Determines if the requester is permitted to keep using ZapConfig, or if their account should be locked down
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Validate(LicenseQueryDto request)
        {
            var status = _licenseService.GetLicenseStatus(
                request.MachineId,
                Request.GetClientIpAddress(),
                request.Version,
                request.License);

            return Json(new LicenseQueryResponseDto
            {
                ValidationResponse = Constants.GetHashForStatus(status, request.RequestId, request.MachineId),
            }, JsonRequestBehavior.AllowGet);
        }

   }
}