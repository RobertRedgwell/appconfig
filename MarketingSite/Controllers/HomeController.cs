﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using AppConfig.Marketing.Model;
using AppConfig.Logic.Exceptions;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using PoliteCaptcha;
using AppConfig.Marketing.Services;
using AppConfig.Core;

namespace AppConfig.Marketing.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _config;
        private readonly IAccountLogic _accountLogic;
        private readonly IUserLogic _userLogic;
        private readonly IMailer _mailer;

        public HomeController(IConfiguration config, IAccountLogic accountLogic, IUserLogic userLogic, IMailer mailer)
        {
            _config = config;
            _accountLogic = accountLogic;
            _userLogic = userLogic;
            _mailer = mailer;
        }

        public ActionResult Index()
        {
            return View(new HomeNavModel { Current = HomeNavigation.About });
        }

        public ActionResult Pricing()
        {
            return View(new HomeNavModel { Current = HomeNavigation.Pricing });
        }

        public ActionResult Support()
        {
            return View(new ContactFormModel());
        }

        public ActionResult Downloads()
        {
            return View(new HomeNavModel { Current = HomeNavigation.Downloads });
        }

        public ActionResult Docs()
        {
            var docs = new DocumentationViewModel
            {
                Sections = DocumentationService.GetDocumentation()
            };
            return View(docs);
        }

        public ActionResult GetStarted(EAccountType? productType = null)
        {
            return View(new GetStartedModel { SelectedProductType = productType });
        }

        [HttpPost]
        [ActionName("GetStarted")]
        public ActionResult GetStartedSubmit(GetStartedModel model)
        {
            if (model == null) return View(new GetStartedModel());
            if (model.ConfirmPassword != model.Password) model.ConfirmPassword = null;
            if (!Enum.IsDefined(typeof(EAccountType), model.SelectedProductType)) model.SelectedProductType = null;
            
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var user = _accountLogic.CreateAccount(model.SelectedProductType.Value, model.CompanyName, model.EmailAddress, model.Password);
                return Login();
                // TODO - Log the user in right away!
                
                /*
                _authService.RequestContext = new HttpRequestContext(
                    new ServiceStack.WebHost.Endpoints.Extensions.HttpRequestWrapper(System.Web.HttpContext.Current.Request),
                    default(IHttpResponse),
                    default(object)
                    );
                
                var response = _authService.Authenticate(new Auth
                {
                    Password = user.Password,
                    UserName = user.EmailAddress,
                });*/
                
            }
            catch (EmailAlreadyRegisteredException e)
            {
                ModelState.AddModelError("EmailAddress", e.Message);
                return View(model);
            }

            return RedirectToAction("Index", "Admin");
        }

        public ActionResult Error()
        {
            return View(new HomeNavModel());
        }

        public ActionResult ContactSuccess()
        {
            return View(new HomeNavModel { Current = HomeNavigation.None });
        }

        [HttpPost, ValidateSpamPrevention]
        [ActionName("Support")]
        public ActionResult SupportSubmit(ContactFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _mailer.SubmitContactUsRequest(model.Name, model.EmailAddress, model.Question);

            return RedirectToAction("ContactSuccess");
        }

        public ActionResult Validate(string id)
        {
            return View(new ValidateModel
            {
                IsValid = _accountLogic.ValidateAccount(id)
            });
        }

        [ActionName("Validate")]
        [HttpPost]
        public ActionResult ValidateSubmit(ValidateModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                _userLogic.ResendValidationEmail(model.EmailAddress);
            }
            catch (AccountNotFoundException e)
            {
                ModelState.AddModelError("EmailAddress", "That email address is not associated with a known ZapConfig account.");
            }
            catch (UnknownUserException e)
            {
                ModelState.AddModelError("EmailAddress", "That email address is not associated with a ZapConfig account.");
            }

            return RedirectToAction("ValidationSent");
        }

        public ActionResult ValidationSent()
        {
            return View(new HomeNavModel { Current = HomeNavigation.None });
        }

        public ActionResult Login()
        {
            return RedirectPermanent(_config.AppSettings["ApiSiteBaseUrl"] + "Login");
        }
    }
}