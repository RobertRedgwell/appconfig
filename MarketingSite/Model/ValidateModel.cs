﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AppConfig.Core;

namespace AppConfig.Marketing.Model
{
    public class ValidateModel : HomeNavModel
    {
        public bool IsValid { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Please enter a valid email address")]
        [RegularExpression(Constants.EmailRegex, ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }
    }
}