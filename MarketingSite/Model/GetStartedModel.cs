﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AppConfig.Core;
using AppConfig.Model;

namespace AppConfig.Marketing.Model
{
    public class GetStartedModel : HomeNavModel
    {
        public GetStartedModel()
        {
            Current = HomeNavigation.GetStarted;
        }

        [Display(Name = "Plan")]
        [Required(ErrorMessage = "Please select a plan")]
        public EAccountType? SelectedProductType { get; set; }

        [Display(Name = "Company/Product name")]
        [Required(ErrorMessage="Please enter your company or product name")]
        public string CompanyName { get; set; }

        [Display(Name = "Email")]
        [Required]
        [RegularExpression(Constants.EmailRegex, ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }

        [Display(Name = "Password")]
        [Required]
        [MinLength(8, ErrorMessage = "Please enter a password at least 8 characters in length")]
        public string Password { get; set; }

        [Display(Name = "Confirm your password")]
        [Required(ErrorMessage = "Please ensure your password is the same in both password fields")]
        public string ConfirmPassword { get; set; }

        public IEnumerable<SelectListItem> AvailableProducts
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text = EAccountType.Shock + " - Free", Value = EAccountType.Shock.ToString(), Selected = SelectedProductType == EAccountType.Shock },
                    new SelectListItem { Text = EAccountType.Bolt + " - $29/Month", Value = EAccountType.Bolt.ToString(), Selected = SelectedProductType == EAccountType.Bolt},
                    new SelectListItem { Text = EAccountType.Storm + " - $99/Month", Value = EAccountType.Storm.ToString(), Selected = SelectedProductType == EAccountType.Storm },
                };
            }
        }

    }
}
