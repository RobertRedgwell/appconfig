﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AppConfig.Core;

namespace AppConfig.Marketing.Model
{
    public class ContactFormModel : HomeNavModel
    {
        public ContactFormModel()
        {
            Current = HomeNavigation.Support;
        }

        [Required]
        public string Name { get; set; }

        [Required]
        [RegularExpression(Constants.EmailRegex, ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }

        [Required]
        public string Question { get; set; }
    }
}