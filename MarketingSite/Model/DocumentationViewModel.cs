﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Marketing.Services.Model;
using AppConfig.Model;

namespace AppConfig.Marketing.Model
{
    public class DocumentationViewModel : HomeNavModel
    {
        public DocumentationViewModel()
        {
            Current = HomeNavigation.Support;
        }

        public List<DocumentationSection> Sections { get; set; }
    }
}