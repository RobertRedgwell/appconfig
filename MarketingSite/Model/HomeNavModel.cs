﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppConfig.Marketing.Model
{
    public class HomeNavModel
    {
        public HomeNavigation Current { get; set; }
    }

    public enum HomeNavigation
    {
        None,
        About,
        Pricing,
        Support,
        Downloads,
        GetStarted,
        Login,
    }
}