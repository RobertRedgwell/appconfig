﻿// page init
$(document).ready(function () {
    // Initialize Foundation plugins
    $(document).foundation();

    // Wire up Ajaxable forms etc
    AppConfig.ajaxForm.init();

    $.fn.asuggest.defaults.delimiters = "${";
    $.fn.asuggest.defaults.minChunkSize = 1;
    $.fn.asuggest.defaults.cycleOnTab = true;
    $.fn.asuggest.defaults.endingSymbols = "}";
});