﻿$(document).ready(function () {
    //<h1 id="home-slogan">Reliable application configuration in one place.</h1>
    if ($('#home-slogan').length > 0) {
        startNextSlogan();
    }    
});

var slogans = ['Reliable application configuration in one place.', 'Automate deployment time configuration quickly and easily.', 'Built by developers, for developers', 'Secure sensitive application configuration values.'];
var sloganIndex = 0;

function getNextSlogan() {
    $('#home-slogan').fadeOut(600, function () {
        sloganIndex++;
        if (sloganIndex >= slogans.length) sloganIndex = 0;
        $('#home-slogan')
            .text(slogans[sloganIndex])
            .show();
        startNextSlogan();
    });
}
function startNextSlogan() {
    setTimeout(getNextSlogan, 2500);
}