﻿(function ($) {

    if (!$.ss) $.ss = {};
    if (!$.ss.validation)
        $.ss.validation = {
            overrideMessages: false,
            messages: {
                NotEmpty: "Required",
                NotNull: "Required",
                Email: "Invalid email",
                AlreadyExists: "Already exists"
            },
            errorFilter: function (errorMsg, errorCode, type) {
                return this.overrideMessages
                    ? this.messages[errorCode] || errorMsg || splitCase(errorCode)
                    : errorMsg || splitCase(errorCode);
            }
        };

    function splitCase(t) {
        return typeof t != 'string' ? t : t.replace(/([A-Z]|[0-9]+)/g, ' $1').replace(/_/g, ' ');
    };

    $.ss.getErrors = function (response) {
        var errors = {};
        var json = response.responseJSON;
        if (json) {
            if (json.responseStatus) {
                if (json.responseStatus.errors && json.responseStatus.errors.length > 0) {
                    for (var i = 0; i < json.responseStatus.errors.length; i++) {
                        errors[json.responseStatus.errors[i].fieldName] = json.responseStatus.errors[i].message;
                    }
                }
                else if (json.responseStatus.errorCode) {
                    errors = { error: json.responseStatus.message };
                }
            }
        }
        return errors;
    }

    $.fn.applyErrors = function (response, opt) {

        var r = JSON.parse(response.responseText);
        var responseStatus = (r && r.responseStatus);

        this.clearErrors();
        if (!responseStatus) return this;

        // Clear and hide validation summary
        var validationSummary = this.find('.validation-summary');
        validationSummary.hide();
        validationSummary.html('');

        // Reset any previous fields with validation errors
        this.find('.invalid').removeClass('invalid');

        // Either a generic request error occurred, or we received either an explicit
        // error message or a list of this validation messages in the result object
        var error = "We're sorry, something went wrong. Please try again later.";
        var hasValidationErrors = false;

        var errors = $.ss.getErrors(response);
        if (errors) {
            // Highlight fields and build validation message summary
            var lis = '';
            for (var ve in errors) {
                hasValidationErrors = true;
                $('#' + ve).addClass('invalid'); // Highlight field in error
                lis += '<li>' + errors[ve] + '</li>'; // Add error message to summary
            }
            validationSummary.html(lis);
            validationSummary.fadeIn(200);
        }

        return this;
    };

    $.fn.clearErrors = function () {

        this.find('.form-content').hide(); // Hide form fields
        this.find('.validation-summary').html('').hide(); // clear and hide validation errors
    };

})(window.jQuery);