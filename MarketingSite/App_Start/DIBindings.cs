﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Core;
using MarketingSite.Services;

namespace AppConfig.Marketing
{
    public class DIBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<ILicenseService>().To<LicenseService>().InSingletonScope();
        }
    }
}