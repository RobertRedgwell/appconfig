﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AppConfig.Marketing
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "LicenseRoute",
                "License/{action}/{id}", // URL with parameters
                new { controller = "License", action = "Index", id = UrlParameter.Optional, itemId = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{action}/{id}/{itemId}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional, itemId = UrlParameter.Optional } // Parameter defaults
            );
        }
    }
}