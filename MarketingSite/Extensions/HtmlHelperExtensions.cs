﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppConfig.Marketing.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static IEnumerable<SelectListItem> SelectListFromEnum<T>(this HtmlHelper helper, Nullable<T> selectedValue = null, params T[] excludedValues)
            where T : struct
        {
            var typeOfT = typeof(T);
            var result = new List<SelectListItem>();
            foreach (var value in Enum.GetValues(typeOfT))
            {
                if (excludedValues.Contains((T)value)) continue;
                result.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeOfT, value),
                    Value = "" + value,
                    Selected = selectedValue != null && selectedValue.Equals(value)
                });
            }
            
            return result;
        }
    }
}