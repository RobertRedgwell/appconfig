﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using AppConfig.Core;
using AppConfig.Model;
using ServiceStack.OrmLite;

namespace MarketingSite.Services
{
    public class LicenseService : ILicenseService
    {
        private readonly IConfiguration _config;
        public const string ConnectionStringKey = "AppConfigDb";
        private OrmLiteConnectionFactory _dbFactory;

        public LicenseService(IConfiguration config)
        {
            _config = config;
            _dbFactory = new OrmLiteConnectionFactory(
                config.ConnectionStrings[ConnectionStringKey], 
                SqlServerDialect.Provider); 
        }

        /// <summary>
        /// Gets the license status for the specified machine Id.
        /// </summary>
        /// <param name="machineId"></param>
        /// <param name="callerIp"></param>
        /// <param name="installationVersion"></param>
        /// <param name="license"></param>
        /// <returns></returns>
        public ELicenseStatus GetLicenseStatus(string machineId, string callerIp, string installationVersion, string license)
        {
            if (string.IsNullOrEmpty(machineId)) return ELicenseStatus.Unknown;

            using (var c = _dbFactory.Open())
            {
                c.CreateTableIfNotExists<AccountLicense>();
                var accountLicense = c.FirstOrDefault<AccountLicense>(a => a.MachineId == machineId);

                // If we don't yet have an account license for this machine, then create one
                if (accountLicense == null)
                {
                    accountLicense = new AccountLicense
                    {
                        MachineId = machineId,
                        Version = installationVersion,
                        WhenCreated = DateTime.Now,
                        Expiry = DateTime.Now.AddDays(30),
                    };
                }

                // Update the license with the last requestor's ip address
                accountLicense.LastRequestIp = callerIp;
                accountLicense.WhenUpdated = DateTime.Now;

                var licenseResponse = ELicenseStatus.Unknown;

                // If we haven't been given a license key, then we are either on trial, or we are out of our trial
                if (accountLicense.Expiry < DateTime.Now)
                {
                    licenseResponse = ELicenseStatus.Unlicensed;
                }
                else if (license == null)
                {
                    licenseResponse = ELicenseStatus.TrialLicensed;
                }

                // If we have a license, but it doesn't match, then we are unlicensed
                else if (accountLicense.License != license)
                {
                    licenseResponse = ELicenseStatus.MismatchedLicense;
                }

                // If we have a matching license, but our version doesn't match, then we are licensed, but have a pending message
                else if (accountLicense.Version != installationVersion)
                {
                    licenseResponse = ELicenseStatus.LicensedOldVersion;
                }

                else
                {
                    licenseResponse = ELicenseStatus.Licensed;
                }

                accountLicense.LicenseStatus = licenseResponse;

                c.Save(accountLicense);

                return licenseResponse;
            }
        }
    }
}