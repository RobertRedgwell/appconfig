﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Core;

namespace MarketingSite.Services
{
    public interface ILicenseService
    {
        /// <summary>
        /// Gets the license status for the specified machine Id.
        /// </summary>
        /// <param name="machineId"></param>
        /// <param name="callerIp"></param>
        /// <param name="installationVersion"></param>
        /// <param name="license"></param>
        /// <returns></returns>
        ELicenseStatus GetLicenseStatus(string machineId, string callerIp, string installationVersion, string license);
    }
}
