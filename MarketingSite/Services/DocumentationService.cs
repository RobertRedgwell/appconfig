﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Marketing.Services.Model;

namespace AppConfig.Marketing.Services
{
    public static class DocumentationService
    {
        public static List<DocumentationSection> GetDocumentation()
        {
            return new List<DocumentationSection>()
            {
                GetMainAppDocs(),
                GetConsoleAppDocs(),
                GetPowershellDocs(),
            };
        }

        private static DocumentationSection GetMainAppDocs()
        {
            return new DocumentationSection
            {
                Name = "Overview",
                Description = @"ZapConfig is a service that allows you to configure a transform to run for your application, or any of the nuget packages your application uses. 

You change the values of important parts of your transform using variables. These variables can be different for one or more of your deployment environments.

Typically, you will want to run one of either the ZapConsole command line tool, or the Powershell script right after you've deployed your application to an environment but before you've started it up. This will modify your app/web.config for the environment you have deployed to.",
                Methods = new List<DocumentationMethod>
                {
                   new DocumentationMethod 
                   {
                       Name = "Environments",
                       Description = @"An Environment in ZapConfig refers to your various deployment environments that your application will be deployed to. In general, you will want to have an environment in ZapConfig for each of your real environments.",
                   },
                   new DocumentationMethod 
                   {
                       Name = "Variables",
                       Description = @"A Variable in ZapConfig refers to a value that may change depending on an environment. For example, you might have the same email gateway for your development and QA environments, but your pre-prod and production environment might use a different gateway. Variables are utilised in the 'Transform' part of each of your packages.",
                   },
                   new DocumentationMethod 
                   {
                       Name = "Packages",
                       Description = @"A Package in ZapConfig refers to a library that your application depends on, or your application itself. As a simple example, you might have a package transform that sets the connection string of your application using a variable. 

You can also set up packages for dependencies of your various applications. For example, you might have multiple applications that use NLog, and you may want to configure log level to be different in each environment, but consistent across your applications. Instead of doing this on a per-application basis, you can just create a transform for NLog. When your application config is transformed, it will not only have its connection string set correctly for the environment but also its log level.",
                   }
                }
            };
        }

        private static DocumentationSection GetConsoleAppDocs()
        {
            return new DocumentationSection
            {
                Name = "Console",
                Description = @"The ZapConfig console tool allows you to interact with ZapConfig using the command line. 
Typically, you will use the ZapConsole command line tool to transform your application config file right after you deploy, but before you start your application.

The .Net Framework v4.0 is required to run the ZapConsole command line tool.",
                Methods = new List<DocumentationMethod>
                {
                   new DocumentationMethod 
                   {
                       Name = "ZapConsole",
                       Description = @"Calls ZapConfig for the specified environment, package, and dependencies.
    This method will update the the contents of the file at $appConfigPath and create a backup file ($appConfigPath + '.bak.config').

    Usage: ZapConsole -apikey=abc123 -environment=QA -appConfigPath=web.config -package=my.package.thing:1.3.4 -dependencies=packages.config",
                       Parameters = new List<MethodParameter> 
                       {
                           new MethodParameter("-apiKey", "The apikey that can be found in your account settings page on the ZapConfig website."), 
                           new MethodParameter("-environment", "The name of the environment that is being deployed to."), 
                           new MethodParameter("-appConfigPath", "The path to the app/web.config of your application."), 
                           new MethodParameter("-package", "A Package name and version (name:version) representing the application being deployed."),
                           new MethodParameter("-dependencies", "An array of Package name and version (name:version) representing the dependencies for the application being deployed, or the path to a nuget packages.config file. If you have no dependencies, use '[]' to signify an empty list of dependencies."),
                           new MethodParameter("-serverUrl", "The url to the server. Defaults to http://cloud.zapconfig.com and can be overridden for self hosted zapconfig instances."), 
                       },
                       Result = "Exit code 0 if successful, non-zero if something went wrong.",
                       Errors = new List<ErrorCase> 
                       { 
                           new ErrorCase("Duplicate arguments.", "Caused by passing in multiple arguments with the same key."), 
                           new ErrorCase("'arg' is required.", "Caused by calling the tool without specifying one of the required arguments."), 
                           new ErrorCase("Invalid package", "Caused by passing in a package not in the correct format of 'name:1.2.3'."), 
                           new ErrorCase("Dependencies file not found", "Caused by passing in a path to a dependencies package.config file that doesn't exist."), 
                           new ErrorCase("Dependencies file is not in a nuget packages.config format", "Caused by passing in a path to a dependencies package.config file that is not in the expected xml format."), 
                           new ErrorCase("Config file not found", "Caused by passing in a path to your application config file that doesn't exist."), 
                       }
                   }
                }
            };
        }

        private static DocumentationSection GetPowershellDocs()
        {
            return new DocumentationSection
            {
                Name = "Powershell",
                Description = @"These methods allow you to interact with ZapConfig using Powershell. 
Typically, you will use the SetupConfig method to transform your application config file right after you deploy, but before you start your application.

Powershell v3 is required to run these methods.",
                Methods = new List<DocumentationMethod>
                {
                   new DocumentationMethod 
                   {
                       Name = "SetupConfig",
                       Description = @"Calls ZapConfig for the specified environment, package, and dependencies.
    This method will update the the contents of the file at $appConfigPath and create a backup file ($appConfigPath + '.bak.config').

    Usage: $core = Package 'my.package.thing' '1.3.4'
           $dependentPackages = NugetPackagesConfigToPackageArray 'packages.config'
           SetupConfig 'abcdef1234567890' 'Production' 'web.config' $core $dependentPackages",
                        Parameters = new List<MethodParameter> 
                       {
                           new MethodParameter("apiKey", "The apikey that can be found in your account settings page on the ZapConfig website."), 
                           new MethodParameter("environment", "The name of the environment that is being deployed to."), 
                           new MethodParameter("appConfigPath", "The path to the app/web.config of your application."), 
                           new MethodParameter("corePackage", "A Package object representing the application being deployed. See the Package method for a quick way to create a package object."),
                           new MethodParameter("dependentPackages", "An array of Package objects representing the dependencies for the application being deployed. See the NugetPackagesConfigToPackageArray method for a quick way to create a package array based on a nuget packages.config file."),
                           new MethodParameter("serverUrl (optional)", "The url to the zap config server. You will need to specify this if you have a self hosted instance."), 
                       },
                       Result = "$true if successful",
                       Errors = new List<ErrorCase> 
                       { 
                           new ErrorCase("'path' does not exist.", "Caused by passing in a path to a non-existent app/web config file."), 
                           new ErrorCase("An error occurred calling the ZapConfig configure service.", "Check the exception output for details."), 
                       }
                   },
                   new DocumentationMethod 
                   {
                       Name = "NugetPackagesConfigToPackageArray",
                       Description = "Used to convert a packages.config file (like the one that nuget creates) into a package array suitable for passing to the SetupConfig method.",                       
                       Parameters = new List<MethodParameter> {new MethodParameter("packagesConfigPath", "The path to the package.config file.")},
                       Result = "An array of Package objects",
                       Errors = new List<ErrorCase> 
                       { 
                           new ErrorCase("'path' is not a packages config file.", "Caused by passing in a path to a non-existent file."), 
                           new ErrorCase("Cannot convert value \"System.Object[]\" to type \"System.Xml.XmlDocument\". Error: \"Data at the root level is invalid. Line 1, position 1.\"", "Caused by passing in a path to a file that isn't in the nuget packages.config file format."), 
                       }
                   },

                   new DocumentationMethod 
                   {
                       Name = "Package",
                       Description = "Creates a package object from the specified name and version",
                       Parameters = new List<MethodParameter> 
                       {
                           new MethodParameter("name", "The name of the package - e.g. \"My.Awesome.Package\"."), 
                           new MethodParameter("version", "The version of the package - e.g. \"1.2.3-develop\".") 
                       },
                       Result = "A Package object",
                   },

                   /*
                   new DocumentationMethod 
                   {
                       Name = "",
                       Description = "",
                       Parameters = new List<MethodParameter> 
                       {
                           new MethodParameter("", ""),
                       },
                       Result = "",
                       Errors = new List<ErrorCase> 
                       { 
                           new ErrorCase("", ""), 
                       }
                   }
                   */
                }
            };
        }
    }
}
