﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppConfig.Marketing.Services.Model
{
    public class DocumentationSection
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<DocumentationMethod> Methods { get; set; }
    }
}