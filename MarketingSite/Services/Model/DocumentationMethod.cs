﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppConfig.Marketing.Services.Model
{
    public class DocumentationMethod
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<MethodParameter> Parameters { get; set; }
        public string Result { get; set; }
        public List<ErrorCase> Errors { get; set; }

        public DocumentationMethod()
        {
            Parameters = new List<MethodParameter>();
            Errors = new List<ErrorCase>();
        }
    }

    public class MethodParameter
    {
        public string Name { get; set; }
        public string Purpose { get; set; }
        
        public MethodParameter(string name, string purpose)
        {
            Name = name;
            Purpose = purpose;
        }
    }

    public class ErrorCase
    {
        public string ErrorMessage { get; set; }
        public string Meaning { get; set; }

        public ErrorCase(string errorMessage, string meaning)
        {
            ErrorMessage = errorMessage;
            Meaning = meaning;
        }
    }
}