﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ZapConsole
{
    [DataContract]
    public class PackageDescriptorDto
    {
        /// <summary>
        /// The fully qualified package name
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// The (semantic) version of the package
        /// </summary>
        [DataMember]
        public string Version { get; set; }
    }
}
