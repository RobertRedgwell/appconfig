﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace ZapConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Options options = null;
            ConfigRequestDto requestDto = null;
            try
            {
                options = GetOptions(args);

                // Grab our option values and make sure they are sane:
                requestDto = ValidateOptions(options);
            }
            catch (UnrecognisedArgumentException e)
            {
                PrintUsage(e.Message);
                return;
            }

            // Make the request:
            Console.WriteLine("Making request...");
            var httpRequest = WebRequest.Create(string.Format("{0}/api/environments/{1}/configure?format=json", options.ServerUrl, options.Environment));
            httpRequest.Method = "Post";
            httpRequest.ContentType = "application/json";
            httpRequest.Headers.Add("ApiKey", options.ApiKey);

            var bytes = Encoding.UTF8.GetBytes(requestDto.AsJson());
            httpRequest.ContentLength = bytes.Length;
            using (var s = httpRequest.GetRequestStream())
            {
                s.Write(bytes, 0, bytes.Length);
            }

            WebResponse response = null;
            try
            {
                response = httpRequest.GetResponse();
            }
            catch (WebException e)
            {
                Error("An error occurred calling ZapConfig. Response: " + e.Message);

                var httpWebResponse = e.Response as HttpWebResponse;
                
                if (httpWebResponse != null)
                {
                    Error(httpWebResponse.StatusDescription);
                    if (httpWebResponse.StatusCode == HttpStatusCode.Forbidden)
                        Error("Ensure you have the correct api key specified");
                }
                Environment.Exit(1);
            }

            if (response == null)
            {
                Console.WriteLine("No response received when calling ZapConfig...");
                Environment.Exit(1);
            }

            Console.WriteLine("Response received... (" + response.ContentLength + " bytes)");
            
            var result = ConfigResponseDto.FromJsonStream(response.GetResponseStream());
            if (result == null || result.OutputConfig == null)
            {
                Console.WriteLine("An error occurred calling ZapConfig. Response:");
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    Console.WriteLine(reader.ReadToEnd());
                    Environment.Exit(1);
                }
            }
            else
            {                
                Console.WriteLine("Received a response. Updating configuration...");
                File.Copy(options.AppConfigPath,
                    Path.Combine(Path.GetDirectoryName(options.AppConfigPath), Path.GetFileNameWithoutExtension(options.AppConfigPath) + ".bak" + Path.GetExtension(options.AppConfigPath)),
                    true);
                
                File.WriteAllText(options.AppConfigPath, result.OutputConfig);
                Console.WriteLine("Successfully updated configuration.");
                Environment.Exit(0);
            }
        }

        public static Options GetOptions(string[] args)
        {
            if (args == null)
            {
                throw new UnrecognisedArgumentException("No arguments specified");
            }

            var argumentItems = new Dictionary<string, string>();
            foreach (var a in args)
            {
                if (string.IsNullOrEmpty(a)) continue;

                var equalsIndex = a.IndexOf('=');
                if (equalsIndex == -1)
                {
                    throw new UnrecognisedArgumentException(string.Format("Could not recognize argument '{0}'. No '=' character found.", a));
                }

                var key = a.Substring(0, equalsIndex).TrimStart('-');
                var value = a.Substring(equalsIndex + 1);

                if (argumentItems.ContainsKey(key))
                {
                    throw new UnrecognisedArgumentException(string.Format("Duplicate argument with key: {0}.", key));
                }

                argumentItems.Add(key.ToLowerInvariant(), value);
            }
            
            var result = new Options();
            foreach (var p in typeof(Options).GetProperties())
            {
                var key = p.Name.ToLowerInvariant();
                if (argumentItems.ContainsKey(key))
                {
                    p.SetValue(result, argumentItems[key], null);
                }
            }

            try
            {
                Validator.ValidateObject(result, new ValidationContext(result, null, null));
            }
            catch (ValidationException e)
            {
                throw new UnrecognisedArgumentException(e.Message);
            }

            result.ServerUrl = result.ServerUrl.Trim('\'', '"', '/');
            Console.WriteLine("Server url: {0}", result.ServerUrl);

            return result;
        }

        public static ConfigRequestDto ValidateOptions(Options options)
        {
            if (options == null) throw new UnrecognisedArgumentException("No arguments provided.");

            var result = new ConfigRequestDto();

            result.CorePackage = ValidatePackage(options.Package);

            if (string.IsNullOrEmpty(options.Dependencies) || options.Dependencies == "[]")
            {
                Console.WriteLine("Determined that no dependencies were specified.");
            }
            else if (options.Dependencies.Contains('[') && options.Dependencies.Contains(']'))
            {
                Console.WriteLine("Loading dependencies as an explicit list...");
                var dependencies = options.Dependencies.Trim("[]".ToCharArray()).Split(',');
                foreach (var d in dependencies)
                {
                    var package = d.Trim(" '".ToCharArray());
                    result.DependentPackages.Add(ValidatePackage(package));
                }
            }
            else
            {
                Console.WriteLine("Loading dependencies as a file...");
                options.Dependencies = options.Dependencies.Trim('\'');
                if (!File.Exists(options.Dependencies))
                {
                    throw new UnrecognisedArgumentException(string.Format("Dependencies file not found at '{0}'", options.Dependencies));
                }

                var xml = new XmlDocument();
                try
                {
                    xml.Load(options.Dependencies);
                    foreach (XmlElement el in xml.SelectNodes("/packages/package"))
                    {
                        var idElement = el.Attributes["id"];
                        var versionElement = el.Attributes["version"];
                        if (idElement == null || versionElement == null) continue;

                        result.DependentPackages.Add(new PackageDescriptorDto
                        {
                            Name = idElement.Value,
                            Version = versionElement.Value,
                        });
                    }
                }
                catch (XmlException)
                {
                    throw new UnrecognisedArgumentException("Dependencies file is not in a nuget packages.config format");
                }
            }

            options.AppConfigPath = options.AppConfigPath.Trim('\'');
            if (!File.Exists(options.AppConfigPath))
            {
                throw new UnrecognisedArgumentException(string.Format("Config file not found at '{0}'", options.AppConfigPath));
            }

            result.InputConfig = File.ReadAllText(options.AppConfigPath);

            return result;
        }

        private static PackageDescriptorDto ValidatePackage(string packageString)
        {
            if (string.IsNullOrEmpty(packageString))
            {
                throw new UnrecognisedArgumentException(string.Format("Invalid package '{0}'", packageString));
            }

            var packageComponents = packageString.Trim('\'').Split(':');
            if (packageComponents.Length != 2 ||
                packageComponents[0] == string.Empty ||
                packageComponents[1] == string.Empty ||
                !Regex.IsMatch(packageComponents[1], @"^\d+(\.\d+){1,2}(-?([0-9a-zA-Z]))*"))
            {
                throw new UnrecognisedArgumentException(string.Format("Invalid package '{0}'", packageString));
            }

            return new PackageDescriptorDto { Name = packageComponents[0], Version = packageComponents[1] };
        }

        private static void Error(string message)
        {
            var originalColour = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine();
            Console.WriteLine("ERROR: " + message);
            Console.ForegroundColor = originalColour;
        }

        private static void PrintUsage(string message)
        {
            Error(message);
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------"); 
            Console.WriteLine("ZapConsole calls the ZapConfig service and updates your application configuration for the specified environment.");
            Console.WriteLine();
            Console.WriteLine("You are using version " + Assembly.GetExecutingAssembly().GetName().Version);
            Console.WriteLine();
            Console.WriteLine("Usage:");
            Console.WriteLine("--------------------------------------------"); 
            Console.WriteLine("Using a nuget packages.config file for dependencies:");
            Console.WriteLine("ZapConsole -environment=Production -appConfigPath='c:\\code\\myApp\\web.config' -apikey=abcdef1234567890 -package='my.awesome.app:1.2.3' -dependencies='c:\\code\\myApp\\packages.config'");
            Console.WriteLine();
            Console.WriteLine("Using explicit dependencies:");
            Console.WriteLine("ZapConsole -environment=Production -appConfigPath='c:\\code\\myApp\\web.config' -apikey=abcdef1234567890 -package='my.awesome.app:1.2.3' -dependencies=['some.other.lib:4.5.6', 'another.lib:2.3.4', 'yet.another.lib:2.1.3-prerelease']");
            Console.WriteLine();
            Console.WriteLine("Options:");
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine("-environment\tSpecifies the name of the environment that is being deployed to.");
            Console.WriteLine("\t-environment=Abc123");
            Console.WriteLine("\t-environment='my environment'");
            Console.WriteLine();
            Console.WriteLine("-appConfigPath\tSpecifies the path to the app/web.config of your application.");
            Console.WriteLine("\t-appConfigPath=web.config");
            Console.WriteLine("\t-appConfigPath='c:\\my path\\web.config'");
            Console.WriteLine();
            Console.WriteLine("-apikey\tThe apikey that can be found in your account settings page on the ZapConfig website.");
            Console.WriteLine("\t-apikey=abcdef1234567890");
            Console.WriteLine();
            Console.WriteLine("-package\tThe name and semantic version of the package being deployed.");
            Console.WriteLine("\t-package=some.package:1.2.3-prerelease");
            Console.WriteLine("\t-package='some.quoted.package:4.5.6-alpha'");
            Console.WriteLine();
            Console.WriteLine("-dependencies\tThe dependencies for the application being deployed. This can either be a path to a nuget packages.config file, or a list of dependencies.");
            Console.WriteLine("\t-dependencies='c:\\code\\myApp\\packages.config'");
            Console.WriteLine("\t-dependencies=['some.other.lib:4.5.6', 'another.lib:2.3.4', 'yet.another.lib:2.1.3-prerelease']");
            Console.WriteLine();
        }
    }
}
