﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ZapConsole.Tests
{
    [TestFixture]
    public class GetOptionsTests
    {
        [Test]
        public void GetOptions_GivenNullArguments_ThrowsExpectedException()
        {
            Assert.Throws<UnrecognisedArgumentException>(() => Program.GetOptions(null));
        }

        [Test]
        public void GetOptions_GivenArgumentWithMissingEquals_ThrowsExpectedException()
        {
            var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.GetOptions(new[] { "-noEquals" }));
            Assert.IsTrue(ex.Message.Contains("No '=' character found"));
        }

        [Test]
        public void GetOptions_GivenDuplicateArguments_ThrowsExpectedException()
        {
            var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.GetOptions(new[] { "-environment=prod", "-environment=qa" }));
            Assert.IsTrue(ex.Message.Contains("Duplicate argument"));
        }

        [Test]
        [TestCase("", "-appConfigPath=web.config", "apikey=abc123", "package=something.something:1.2.3", "dependencies=packages.config")]
        [TestCase("-environment=prod", "", "apikey=abc123", "package=something.something:1.2.3", "dependencies=packages.config")]
        [TestCase("-environment=prod", "-appConfigPath=web.config", "", "package=something.something:1.2.3", "dependencies=packages.config")]
        [TestCase("-environment=prod", "-appConfigPath=web.config", "apikey=abc123", "", "dependencies=packages.config")]
        [TestCase("-environment=prod", "-appConfigPath=web.config", "apikey=abc123", "package=something.something:1.2.3", "")]
        public void GetOptions_GivenMissingRequiredArgument_ThrowsExpectedException(string environment, string appConfigPath, string apikey, string package, string dependencies)
        {
            var args = new[] { environment, appConfigPath, apikey, package, dependencies };
            var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.GetOptions(args));
            Assert.IsTrue(ex.Message.Contains("is required"));
        }

        [Test]
        public void GetOptions_GivenValidArguments_PopulatesOptionsAsExpected()
        {
            var environment = "prod";
            var appConfigPath = "web.config";
            var apikey = "abc123";
            var package = "my.app:123";
            var dependencies = "packages.config";

            var args = new[] { "-environment=" + environment, "-appConfigPath=" + appConfigPath, "-apikey=" + apikey, "-package=" + package, "-dependencies=" + dependencies };
            var options = Program.GetOptions(args);
            Assert.AreEqual(environment, options.Environment);
            Assert.AreEqual(appConfigPath, options.AppConfigPath);
            Assert.AreEqual(apikey, options.ApiKey);
            Assert.AreEqual(package, options.Package);
            Assert.AreEqual(dependencies, options.Dependencies);
        }
    }
}
