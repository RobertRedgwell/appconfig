﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ZapConsole.Tests
{
    [TestFixture]
    public class ValidateOptionsTests
    {
        [Test]
        public void ValidateOptions_GivenNullArguments_ThrowsExpectedException()
        {
            Assert.Throws<UnrecognisedArgumentException>(() => Program.ValidateOptions(null));
        }

        [Test]
        [TestCase("")]
        [TestCase("something.somethingelse")]
        [TestCase("something.somethingelse:")]
        [TestCase(":1.2.3")]
        [TestCase(":")]
        [TestCase("something.somethingelse:not-a-version")]
        public void ValidateOptions_GivenInvalidPackageValue_ThrowsExpectedException(string packageValue)
        {
            var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.ValidateOptions(new Options { Package = packageValue }));
            Assert.IsTrue(ex.Message.Contains("Invalid package"));
        }

        [Test]
        [TestCase("['some.other.lib:4.5.6',]")]
        [TestCase("['some.other.lib:4.5.6',']")]
        [TestCase("['some.other.lib:4.5.6', ':2.3.4']")]
        public void ValidateOptions_GivenInvalidDependencies_ThrowsExpectedException(string dependencies)
        {
            var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.ValidateOptions(new Options { Package = "something.something:1.2.3", Dependencies = dependencies }));
            Assert.IsTrue(ex.Message.Contains("Invalid package"));
        }

        [Test]
        public void ValidateOptions_GivenInvalidPathToDependencies_ThrowsExpectedException()
        {
            var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.ValidateOptions(new Options { Package = "Some.thing:1.2.3", Dependencies = "InvalidPath" }));
            Assert.IsTrue(ex.Message.Contains("Dependencies file not found"), "Exception message was: " + ex.Message);
        }

        [Test]
        public void ValidateOptions_GivenInvalidDependenciesFile_ThrowsExpectedException()
        {
            var tempPackagesConfig = Path.GetTempFileName();
            try
            {
                File.WriteAllText(tempPackagesConfig, Guid.NewGuid().ToString());
                var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.ValidateOptions(new Options { Package = "Some.thing:1.2.3", Dependencies = tempPackagesConfig }));
                Assert.IsTrue(ex.Message.Contains("Dependencies file is not in a nuget packages.config format"), "Exception message was: " + ex.Message);
            }
            finally
            {
                File.Delete(tempPackagesConfig);
            }
        }

        [Test]
        public void ValidateOptions_GivenInvalidPathToAppConfig_ThrowsExpectedException()
        {
            var ex = Assert.Throws<UnrecognisedArgumentException>(() => Program.ValidateOptions(new Options { Package = "Some.thing:1.2.3", Dependencies = "[]", AppConfigPath = "InvalidPath" }));
            Assert.IsTrue(ex.Message.Contains("Config file not found"), "Exception message was: " + ex.Message);
        }

        [Test]
        public void ValidateOptions_GivenValidObjectWithDependenciesArray_ReturnsValidDto()
        {
            var tempAppConfig = Path.GetTempFileName();
            try
            {
                var fileContents = Guid.NewGuid().ToString();

                File.WriteAllText(tempAppConfig, fileContents);

                var options = new Options
                {
                    Environment = "Production",
                    Package = "Some.thing:1.2.3",
                    Dependencies = "['another.thing:4.5.6', 'yet.another:7.8.9']",
                    AppConfigPath = tempAppConfig
                };

                var result = Program.ValidateOptions(options);

                Assert.IsNotNull(result.CorePackage);
                Assert.AreEqual("Some.thing", result.CorePackage.Name);
                Assert.AreEqual("1.2.3", result.CorePackage.Version);

                Assert.AreEqual(2, result.DependentPackages.Count);
                Assert.AreEqual("another.thing", result.DependentPackages[0].Name);
                Assert.AreEqual("4.5.6", result.DependentPackages[0].Version);
                Assert.AreEqual("yet.another", result.DependentPackages[1].Name);
                Assert.AreEqual("7.8.9", result.DependentPackages[1].Version);

                Assert.AreEqual(fileContents, result.InputConfig);
            }
            finally
            {
                File.Delete(tempAppConfig);
            }
        }

        [Test]
        public void ValidateOptions_GivenPackagesConfigFileForDependencies_ReturnsValidDto()
        {
            var tempAppConfig = Path.GetTempFileName();
            var tempPackagesConfig = Path.GetTempFileName();
            try
            {
                var fileContents = Guid.NewGuid().ToString();
                File.WriteAllText(tempAppConfig, fileContents);

                var packagesConfigContents = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                                             "<packages>" + 
                                                "<package id=\"another.thing\" version=\"4.5.6\" targetFramework=\"net45\" />" +
                                                "<package id=\"yet.another\" version=\"7.8.9\" targetFramework=\"net45\" />" +
                                                "<package id=\"ignored.no.version\" targetFramework=\"net45\" />" +
                                                "<package version=\"ignored.no.id\" targetFramework=\"net45\" />" +
                                             "</packages>";
                File.WriteAllText(tempPackagesConfig, packagesConfigContents);

                var options = new Options
                {
                    Environment = "Production",
                    Package = "Some.thing:1.2.3",
                    Dependencies = tempPackagesConfig,
                    AppConfigPath = tempAppConfig
                };

                var result = Program.ValidateOptions(options);

                Assert.IsNotNull(result.CorePackage);
                Assert.AreEqual("Some.thing", result.CorePackage.Name);
                Assert.AreEqual("1.2.3", result.CorePackage.Version);

                Assert.AreEqual(2, result.DependentPackages.Count);
                Assert.AreEqual("another.thing", result.DependentPackages[0].Name);
                Assert.AreEqual("4.5.6", result.DependentPackages[0].Version);
                Assert.AreEqual("yet.another", result.DependentPackages[1].Name);
                Assert.AreEqual("7.8.9", result.DependentPackages[1].Version);

                Assert.AreEqual(fileContents, result.InputConfig);
            }
            finally
            {
                File.Delete(tempAppConfig);
                File.Delete(tempPackagesConfig);
            }
        }
    }
}