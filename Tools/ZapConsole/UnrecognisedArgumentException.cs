﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZapConsole
{
    public class UnrecognisedArgumentException : ArgumentException
    {
        public UnrecognisedArgumentException()
        {
        }

        public UnrecognisedArgumentException(string message)
            : base(message)
        {
        }
    }
}
