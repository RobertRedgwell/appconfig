﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace ZapConsole
{
    [DataContract]
    public class ConfigRequestDto
    {
        public ConfigRequestDto()
        {
            DependentPackages = new List<PackageDescriptorDto>();
        }

        /// <summary>
        /// A Desciptor identifying the core package being deployed
        /// </summary>
        [DataMember]
        public PackageDescriptorDto CorePackage { get; set; }

        /// <summary>
        /// A list of packages that the core package is dependent on
        /// </summary>
        [DataMember]
        public List<PackageDescriptorDto> DependentPackages { get; set; }

        /// <summary>
        /// The contents of the configuration file of the application that's being deployed
        /// </summary>
        [DataMember]
        public string InputConfig { get; set; }

        public string AsJson()
        {
            using (var ms = new MemoryStream())
            {
                var serialiser = new DataContractJsonSerializer(GetType());
                serialiser.WriteObject(ms, this);
                ms.Position = 0;
                return new StreamReader(ms).ReadToEnd();
            }
        }
    }
}
