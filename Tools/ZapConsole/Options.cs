﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ZapConsole
{
    public class Options
    {
        public Options()
        {
            ServerUrl = "http://cloud.zapconfig.com";
        }

        [Required]
        public string Environment { get; set; }
        [Required]
        public string AppConfigPath { get; set; }
        [Required]
        public string ApiKey { get; set; }
        [Required]
        public string Package { get; set; }

        public string Dependencies { get; set; }

        public string ServerUrl { get; set; }
    }
}
