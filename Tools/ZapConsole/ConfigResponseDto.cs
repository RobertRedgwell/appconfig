﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace ZapConsole
{
    [DataContract]
    public class ConfigResponseDto
    {
        public static ConfigResponseDto FromJsonStream(Stream jsonStream)
        {
            var serialiser = new DataContractJsonSerializer(typeof(ConfigResponseDto));
            return serialiser.ReadObject(jsonStream) as ConfigResponseDto;
        }

        [DataMember(Name = "outputConfig")]
        public string OutputConfig { get; set; }
    }
}
