#
# Calls ZapConfig for the specified environment, package, and dependencies
#   Usage: $core = Package 'my.package.thing' '1.3.4'
#          $dependentPackages = NugetPackagesConfigToPackageArray("c:\Code\MyApp\packages.config")
#          SetupConfig 'abcdef1234567890' 'Production' 'web.config' $core $dependentPackages
#          SetupConfig 'abcdef1234567890' 'Production' 'web.config' $core $dependentPackages http://my.local.instance:8080
#
function SetupConfig([string]$apiKey, 
    [string]$environment,
    [string]$appConfigPath,
    [PSObject]$corePackage,
    [PSObject[]]$dependentPackages,
	[string]$serverUrl = 'http://cloud.zapconfig.com') {
    
    if ((Test-Path $appConfigPath -pathType leaf) -eq $false) {
        throw "$appConfigPath does not exist."
    }

    $configContents = ([System.IO.File]::ReadAllText($appConfigPath))
    $requestObject = New-Object PSObject -Property @{ corePackage = $corePackage; dependentPackages = $dependentPackages; inputConfig = $configContents }

    $requestJson = ConvertTo-Json -InputObject $requestObject -Compress

    $response = Invoke-RestMethod -Uri "${serverUrl}/api/environments/${environment}/configure?format=json" -Body $requestJson -Method Post -Headers @{ "ApiKey" = $apiKey } -ContentType "application/json"
    if ($response -eq $null) {
        throw 'An error occurred calling the ZapConfig configure service'
    }

	Copy-Item $appConfigPath "${appConfigPath}.bak.config" -Force
    Set-Content $appConfigPath $response.outputConfig

    return $true
}

#
# Creates a package array object suitable for passing to the SetupConfig method for the dependentPackages argument
# Usage: $dependentPackages = NugetPackagesConfigToPackageArray("c:\Code\MyApp\packages.config")
#
function NugetPackagesConfigToPackageArray([string]$packagesConfigPath) {
    if ((Test-Path $packagesConfigPath -pathType leaf) -eq $false) {
        throw "$packagesConfigPath is not a packages config file."
    }
    
     [xml]$xml = Get-Content $packagesConfigPath
     
     $result = @()
     
     $xml.packages.package | foreach {
        $result += Package $_.id $_.version
     }
     return $result
}

#
# Creates a package object from the specified name and version
#   Usage: $core = Package 'my.awesome.package' '1.2.3'
#
function Package([string]$name, [string]$version) {
    New-Object PSObject -Property @{ Name = $name; Version = $version }
}