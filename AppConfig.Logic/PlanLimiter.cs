﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Logic.Exceptions;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Logic
{
    public class PlanLimiter : IPlanLimiter
    {
        private readonly IAccountRepo _accountRepo;
        private readonly IEnvironmentRepo _environmentRepo;
        private readonly IPackageRepo _packageRepo;
        private readonly IVariableRepo _variableRepo;
        private readonly IUserRepo _userRepo;

        public PlanLimiter(IAccountRepo accountRepo, IEnvironmentRepo environmentRepo, IPackageRepo packageRepo, IVariableRepo variableRepo, IUserRepo userRepo)
        {
            _accountRepo = accountRepo;
            _environmentRepo = environmentRepo;
            _packageRepo = packageRepo;
            _variableRepo = variableRepo;
            _userRepo = userRepo;
        }

        public bool CanAddEnvironment(long accountId)
        {
            var account = GetAccountOrThrow(accountId);
            var environmentCount = _environmentRepo.GetCountForAccount(accountId);
            switch (account.AccountType)
            {
                case EAccountType.Bolt:
                    return environmentCount < 5;
                case EAccountType.Storm:
                    return environmentCount < Int32.MaxValue;
                default:
                    return environmentCount < 2;
            }            
        }

        public bool CanAddPackage(long accountId)
        {
            var account = GetAccountOrThrow(accountId);
            var packageCount = _packageRepo.GetCountForAccount(accountId);
            switch (account.AccountType)
            {
                case EAccountType.Bolt:
                    return packageCount < 100;
                case EAccountType.Storm:
                    return packageCount < Int32.MaxValue;
                default:
                    return packageCount < 10;
            }
        }

        public bool CanAddVariable(long accountId)
        {
            var account = GetAccountOrThrow(accountId);
            var variableCount = _variableRepo.GetCountForAccount(accountId);
            switch (account.AccountType)
            {
                case EAccountType.Bolt:
                    return variableCount < 100;
                case EAccountType.Storm:
                    return variableCount < Int32.MaxValue;
                default:
                    return variableCount < 20;
            }
        }

        public bool CanAddUser(long accountId)
        {
            var account = GetAccountOrThrow(accountId);
            var userCount = _userRepo.GetCountForAccount(accountId);
            switch (account.AccountType)
            {
                case EAccountType.Bolt:
                    return userCount < 5;
                case EAccountType.Storm:
                    return userCount < Int32.MaxValue;
                default:
                    return userCount < 1;
            }
        }

        private Account GetAccountOrThrow(long accountId)
        {
            var account = _accountRepo.GetById(accountId);
            if (account == null) throw new AccountNotFoundException(accountId);
            return account;
        }


        public void CanAddEnvironmentOrThrow(long accountId)
        {
            if (!CanAddEnvironment(accountId)) throw new PlanLimitException("You have reached the environment limit for your current plan.");
        }

        public void CanAddPackageOrThrow(long accountId)
        {
            if (!CanAddPackage(accountId)) throw new PlanLimitException("You have reached the package limit for your current plan.");
        }

        public void CanAddVariableOrThrow(long accountId)
        {
            if (!CanAddVariable(accountId)) throw new PlanLimitException("You have reached the variable limit for your current plan.");
        }

        public void CanAddUserOrThrow(long accountId)
        {
            if (!CanAddUser(accountId)) throw new PlanLimitException("You have reached the user limit for your current plan.");
        }

        public void CanAddAccountItemOrThrow(EAccountItem accountItemType, long accountId)
        {
            switch (accountItemType)
            {
                case EAccountItem.Environment:
                    CanAddEnvironmentOrThrow(accountId);
                    break;
                case EAccountItem.Package:
                    CanAddPackageOrThrow(accountId);
                    break;
                case EAccountItem.User:
                    CanAddUserOrThrow(accountId);
                    break;
                case EAccountItem.Variable:
                    CanAddVariableOrThrow(accountId);
                    break;
                default:
                    throw new UnknownAccountItemTypeException(accountItemType);
            }
        }
    }
}
