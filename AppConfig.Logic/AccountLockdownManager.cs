﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Logic.Interfaces;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Logic
{
    public class AccountLockdownManager : IAccountLockdownManager
    {
        private readonly IAccountRepo _accountRepo;
        private readonly Dictionary<long, bool> _lockedDownStates;

        public AccountLockdownManager(IAccountRepo accountRepo)
	    {
            _accountRepo = accountRepo;
            _lockedDownStates = new Dictionary<long, bool>();
	    }

        public bool IsAccountLockedDown(long accountId)
        {
            lock (_lockedDownStates)
            {
                if (!_lockedDownStates.ContainsKey(accountId))
                {
                    var account = _accountRepo.GetById(accountId);
                    _lockedDownStates.Add(accountId, account == null ? true : account.IsLockedDown());
                }
            }
            return _lockedDownStates[accountId];
        }

        public void SetAccountLockedDownState(long accountId, bool isLockedDown)
        {
            lock (_lockedDownStates)
            {
                if (!_lockedDownStates.ContainsKey(accountId))
                    _lockedDownStates.Add(accountId, isLockedDown);
                else
                    _lockedDownStates[accountId] = isLockedDown;
            }
        }
    }
}
