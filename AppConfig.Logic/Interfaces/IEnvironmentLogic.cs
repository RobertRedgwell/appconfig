﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Logic.Interfaces
{
    public interface IEnvironmentLogic : IBaseAccountLogic<AppConfig.Model.Environment>
    {
    }
}
