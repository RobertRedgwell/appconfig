﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Logic.Interfaces
{
    public interface IBaseAccountLogic<TModel>
        where TModel : BaseAccountModel
    {
        List<TModel> GetListForAccount(long accountId);

        bool DeleteForAccount(long packageId, long accountId, long userId);

        void SaveForAccount(TModel model, long accountId, long userId);
    }
}
