﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Logic.Interfaces
{
    public interface IAccountLogic
    {
        void RequestAccountUpgrade(long userId, long accountId, EAccountType targetPackageId);

        string RegenerateApiKey(long userId, long accountId);

        AccountDetails GetAccountDetails(long userId, long accountId);

        //void InitDatabase();

        User CreateAccount(EAccountType product, string companyName, string emailAddress, string password);

        bool ValidateAccount(string validationKey);

        /// <summary>
        /// Begins the async process of validating the license for this account
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="async">If true, then fire and forget</param>
        /// <param name="newLicenseKey">A new license key to set, or null/empty if we should do nothing</param>
        void BeginLicenseValidation(long accountId, bool async = true, string newLicenseKey = null);
    }
}
