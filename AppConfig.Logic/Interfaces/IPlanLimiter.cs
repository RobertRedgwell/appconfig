﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Logic.Interfaces
{
    public interface IPlanLimiter
    {
        void CanAddEnvironmentOrThrow(long accountId);
        bool CanAddEnvironment(long accountId);

        void CanAddPackageOrThrow(long accountId);
        bool CanAddPackage(long accountId);

        void CanAddVariableOrThrow(long accountId);
        bool CanAddVariable(long accountId);

        void CanAddUserOrThrow(long accountId);
        bool CanAddUser(long accountId);

        void CanAddAccountItemOrThrow(EAccountItem accountItemType, long accountId);
    }
}
