﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Logic.Interfaces
{
    public interface IAccountLockdownManager
    {
        bool IsAccountLockedDown(long accountId);

        void SetAccountLockedDownState(long accountId, bool isLockedDown);
    }
}
