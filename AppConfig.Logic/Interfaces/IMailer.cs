﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Logic.Interfaces
{
    public interface IMailer
    {
        void SubmitContactUsRequest(string name, string emailAddress, string message);
        
        void SubmitUpgradeRequest(long accountId, string userEmailAddress, EAccountType targetAccountType);

        void SendWelcomeEmail(string emailAddress, string companyName, string validationKey, EAccountType accountType);

        void SendValidateAccountEmail(string emailAddress, string companyName, string validationKey, EAccountType accountType);

        void SendWelcomeAdditionalUserEmail(string emailAddress, string password, string companyName);
    }
}
