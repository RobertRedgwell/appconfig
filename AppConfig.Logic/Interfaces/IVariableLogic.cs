﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Model;

namespace AppConfig.Logic.Interfaces
{
    public interface IVariableLogic : IBaseAccountLogic<Variable>
    {
        void SaveForAccount(Variable model, List<VariableEnvironment> variableEnvironments, long accountId, long userId);

        List<VariableEnvironment> GetForVariable(long variableId, long accountId);
    }
}
