﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AppConfig.Core;
using AppConfig.Logic.Interfaces;

namespace AppConfig.Logic
{
    public class DIBindings : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IMailer>().To<Mailer>().InSingletonScope();
            Kernel.Bind<IPlanLimiter>().To<PlanLimiter>().InSingletonScope();
            Kernel.Bind<IAccountLockdownManager>().To<AccountLockdownManager>().InSingletonScope();

            Kernel.Bind<IAccountLogic>().To<AccountLogic>().InSingletonScope();
            Kernel.Bind<IEnvironmentLogic>().To<EnvironmentLogic>().InSingletonScope();
            Kernel.Bind<IPackageLogic>().To<PackageLogic>().InSingletonScope();
            Kernel.Bind<IUserLogic>().To<UserLogic>().InSingletonScope();
            Kernel.Bind<IVariableLogic>().To<VariableLogic>().InSingletonScope();
       }
    }
}