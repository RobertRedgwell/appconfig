﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using AppConfig.Core;
using AppConfig.Logic.Exceptions;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;
using ServiceStack.ServiceClient.Web;

namespace AppConfig.Logic
{
    public class AccountLogic : IAccountLogic
    {
        private readonly IAccountRepo _accountRepo;
        private readonly IUserRepo _userRepo;
        private readonly IEnvironmentRepo _environmentRepo;
        private readonly IVariableRepo _variableRepo;
        private readonly IPackageRepo _packagesRepo;
        private readonly IMailer _mailer;
        private readonly IAuditEntryRepo _auditEntryRepo;
        private readonly IAccountLockdownManager _lockdownManager;

        public AccountLogic(IAccountRepo accountRepo, IUserRepo userRepo, IEnvironmentRepo environmentRepo, IVariableRepo variableRepo, IPackageRepo packagesRepo, IMailer mailer, IAuditEntryRepo auditEntryRepo, IAccountLockdownManager accountLockdownManager)
        {
            _accountRepo = accountRepo;
            _userRepo = userRepo;
            _environmentRepo = environmentRepo;
            _variableRepo = variableRepo;
            _packagesRepo = packagesRepo;
            _mailer = mailer;
            _auditEntryRepo = auditEntryRepo;
            _lockdownManager = accountLockdownManager;
        }

        public void RequestAccountUpgrade(long userId, long accountId, EAccountType targetPackageId)
        {
            var user = _userRepo.GetById(userId);
            if (user == null) throw new UnknownUserException(userId);

            _mailer.SubmitUpgradeRequest(accountId, user.EmailAddress, targetPackageId);
        }

        public string RegenerateApiKey(long userId, long accountId)
        {
            var user = _userRepo.GetByIdForAccount(userId, accountId);
            if (user == null) throw new UnknownUserException(userId);

            return _accountRepo.ResetApiKey(accountId, GenerateApiKey(), user);
        }

        public AccountDetails GetAccountDetails(long userId, long accountId)
        {
            var user = _userRepo.GetByIdForAccount(userId, accountId);
            if (user == null) throw new UnknownUserException(userId);

            var users = _userRepo.GetCountForAccount(accountId);
            var environments = _environmentRepo.GetCountForAccount(accountId);
            var variables = _variableRepo.GetCountForAccount(accountId);
            var packages = _packagesRepo.GetCountForAccount(accountId);
            return new AccountDetails(_accountRepo.GetById(accountId),
                users,
                environments,
                variables,
                packages);

        }

        public User CreateAccount(EAccountType accountType, string companyName, string emailAddress, string password)
        {
            if (_userRepo.GetByEmail(emailAddress) != null)
                throw new EmailAlreadyRegisteredException(emailAddress);

            var account = new Account
            {
                AccountType = accountType,
                ApiKey = GenerateApiKey(),
                Name = companyName,
                ValidationString = GenerateAccountValidationKey(),
                HasBeenValidated = false,
            };

            account.SetLicenseData(string.Empty, DateTime.MinValue, ELicenseStatus.Unknown, false);

            _accountRepo.Save(account, null);
            var user = new User(account.Id, emailAddress, password);
            _userRepo.Save(user, null);

            _mailer.SendWelcomeEmail(emailAddress, companyName, account.ValidationString, accountType);

            return user;
        }

        public bool ValidateAccount(string validationKey)
        {
            return _accountRepo.ValidateAccount(validationKey);
        }

        /// <summary>
        /// Begins the async process of validating the license for this account
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="async">If true, then fire and forget</param>
        /// <param name="newLicenseKey">A new license key to set, or null/empty if we should do nothing</param>
        public void BeginLicenseValidation(long accountId, bool async = true, string newLicenseKey = null)
        {
            var account = _accountRepo.GetById(accountId);
            if (account != null)
            {
                var licenseData = account.GetLicenseData();
                if (newLicenseKey != null)
                {
                    account.SetLicenseKey(newLicenseKey);
                    licenseData = account.GetLicenseData();
                    _accountRepo.Save(account, null);
                }

                var request = new LicenseQueryDto
                {
                    License = licenseData == null ? null : licenseData.License,
                    MachineId = UniqueMachineId(account.ValidationString),
                    RequestId = Guid.NewGuid().ToString(),
                    Version = "1.0"
                };

#if DEBUG
                var baseUrl = "http://localhost:8873/";
#else
                var baseUrl = "http://www.zapconfig.com";
#endif

                Action<LicenseQueryResponseDto> onSuccess = dto =>
                {
                    foreach (ELicenseStatus status in Enum.GetValues(typeof(ELicenseStatus)))
                    {
                        if (dto.ValidationResponse == Constants.GetHashForStatus(status, request.RequestId, request.MachineId))
                        {
                            HandleLicenseResponse(status, account, licenseData, dto.NotificationMessage);
                            return;
                        }
                    }

                    HandleLicenseResponse(ELicenseStatus.Unknown, account, licenseData, dto.NotificationMessage);
                };

                Action<LicenseQueryResponseDto, Exception> onError = (dto, ex) =>
                {
                    HandleLicenseResponse(ELicenseStatus.Unknown, account, licenseData, ex == null ? string.Empty : ex.Message);
                };

                if (async)
                {
                    new JsonServiceClient(baseUrl).PostAsync<LicenseQueryResponseDto>("/License/Validate", request, onSuccess, onError);
                }
                else
                {
                    LicenseQueryResponseDto response = null;
                    try
                    {
                        response = new JsonServiceClient(baseUrl).Post<LicenseQueryResponseDto>("/License/Validate", request);
                        onSuccess(response);
                    }
                    catch (Exception e)
                    {
                        onError(response, e);
                    };
                }
            }
            // We should never get to a point where the account is null here ...!
        }

        private void HandleLicenseResponse(ELicenseStatus status, Account account, Account.LicenseCheckData licenseData, string additionalMessage)
        {
            if (licenseData == null) licenseData = new Account.LicenseCheckData();

            switch (status)
            {
                case ELicenseStatus.Licensed:
                case ELicenseStatus.TrialLicensed:
                    if (licenseData.LastLicenseStatus != status)
                    {
                        _auditEntryRepo.Save(AuditEntry.AccountLicensingCheckSucceeded(account.Id, status), null);
                    }
                    account.SetLicenseData(licenseData.License, DateTime.Now, status, false);
                    _lockdownManager.SetAccountLockedDownState(account.Id, false);
                    break;
                default:
                    _auditEntryRepo.Save(AuditEntry.AccountLicensingCheckFailed(account.Id, additionalMessage), null);

                    // If we have an unknown status, and we've recently had a valid check, then leave things alone for now...
                    var lockDown = true;
                    if (status == ELicenseStatus.Unknown && 
                        licenseData.LastCheck.AddDays(7) > DateTime.Now)
                    {
                        // We should not alter our lockdown status...
                        lockDown = licenseData.LockedDown;
                    }

                    // Save our license data
                    account.SetLicenseData(
                        licenseData.License, 
                        status == ELicenseStatus.Unknown ? licenseData.LastCheck : DateTime.Now, 
                        status,
                        lockDown);
                    _lockdownManager.SetAccountLockedDownState(account.Id, lockDown);
                    break;
            }

            _accountRepo.Save(account, null);
        }

        private static string UniqueMachineId(string accountValidationString)
        {
            var builder = new StringBuilder();
            builder.Append(System.Environment.ProcessorCount + "-");
            builder.Append(System.Environment.MachineName + "-");
            builder.Append(System.Environment.UserDomainName + "-");
            builder.Append(System.Environment.UserName + "-");
            builder.Append(System.Environment.GetLogicalDrives().Length + "-");
            builder.Append(System.Environment.OSVersion + "-");
            builder.Append(accountValidationString);
            var bytes = Encoding.UTF8.GetBytes(builder.ToString());
            return Convert.ToBase64String(MD5.Create().ComputeHash(bytes, 0, bytes.Length));
        }

        private static string GenerateApiKey()
        {
            return Guid.NewGuid().ToString()
                .Replace("-", string.Empty)
                .Substring(0, 16);
        }

        private static string GenerateAccountValidationKey()
        {
            return (Guid.NewGuid().ToString() + Guid.NewGuid().ToString())
                .Replace("-", string.Empty);
        }
    }
}
