﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Logic.Exceptions;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Logic
{
    public class UserLogic : BaseAccountLogic<User, IUserRepo>, IUserLogic
    {
        private readonly IAccountRepo _accountRepo;
        private readonly IMailer _mailer;

        public UserLogic(IUserRepo userRepo, IPlanLimiter planLimiter, IAccountLockdownManager accountLockdownManager, IAccountRepo accountRepo, IMailer mailer)
            : base(userRepo, planLimiter, accountLockdownManager, EAccountItem.User)
        {
            _accountRepo = accountRepo;
            _mailer = mailer;
        }

        public User CreateUser(string emailAddress, long accountId, long performingUserId)
        {
            _planLimiter.CanAddAccountItemOrThrow(EAccountItem.User, accountId); 
            if (_repo.GetByEmail(emailAddress) != null)
            {
                throw new EmailAlreadyRegisteredException(emailAddress);
            }
            var account = _accountRepo.GetById(accountId);
            if (account == null)
            {
                throw new AccountNotFoundException(accountId);
            }

            var user = new User(account.Id, emailAddress, Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 8));

            _repo.Save(user, performingUserId);
            _mailer.SendWelcomeAdditionalUserEmail(emailAddress, user.Password, account.Name);

            return user;
        }


        public void ResendValidationEmail(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress)) throw new ArgumentException("emailAddress");

            var user = _repo.GetByEmail(emailAddress);
            if (user == null) throw new UnknownUserException(emailAddress);

            var account = _accountRepo.GetById(user.AccountId);
            if (account == null) throw new AccountNotFoundException(user.AccountId);

            _mailer.SendValidateAccountEmail(emailAddress, account.Name, account.ValidationString, account.AccountType);
        }
    }
}
