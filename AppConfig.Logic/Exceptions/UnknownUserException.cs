﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppConfig.Logic.Exceptions
{
    public class UnknownUserException : ArgumentException
    {
        public UnknownUserException(long userId)
            : base(string.Format("Requesting user with id {0} was not found", userId))
        {
        }
        
        public UnknownUserException(string emailAddress)
            : base(string.Format("Requesting user with email {0} was not found", emailAddress))
        {
        }
    }
}
