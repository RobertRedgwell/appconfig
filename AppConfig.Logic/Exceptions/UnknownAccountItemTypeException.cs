﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppConfig.Logic.Exceptions
{
    public class UnknownAccountItemTypeException : ArgumentException
    {
        public UnknownAccountItemTypeException(EAccountItem accountItemType)
            : base(string.Format("Account item type {0} is not valid", accountItemType))
        {
        }
    }
}
