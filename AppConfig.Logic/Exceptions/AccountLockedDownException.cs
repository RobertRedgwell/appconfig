﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Logic.Exceptions
{
    public class AccountLockedDownException : ArgumentException
    {
        public AccountLockedDownException()
            : base("Your account is currently locked down. Please verify your account license settings on the account page.")
        {
        }
    }
}
