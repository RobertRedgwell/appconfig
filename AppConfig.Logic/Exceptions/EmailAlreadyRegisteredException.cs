﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppConfig.Logic.Exceptions
{
    public class EmailAlreadyRegisteredException : ArgumentException
    {
        public EmailAlreadyRegisteredException(string emailAddress)
            : base(string.Format("The email address {0} has already been registered", emailAddress))
        {
        }
    }
}
