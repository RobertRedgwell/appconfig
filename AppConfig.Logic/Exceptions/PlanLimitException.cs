﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Logic.Exceptions
{
    public class PlanLimitException : ArgumentException
    {
        public PlanLimitException(string message)
            : base(message)
        {
        }
    }
}
