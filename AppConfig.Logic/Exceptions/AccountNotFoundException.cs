﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Logic.Exceptions
{
    public class AccountNotFoundException : ArgumentException
    {
        public AccountNotFoundException(long accountId)
            : base(string.Format("The specified account with id {0} was not found", accountId))
        {

        }
    }
}
