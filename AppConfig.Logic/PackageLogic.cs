﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Logic
{
    public class PackageLogic : BaseAccountLogic<Package, IPackageRepo>, IPackageLogic
    {
        public PackageLogic(IPackageRepo packageRepo, IPlanLimiter planLimiter, IAccountLockdownManager accountLockdownManager)
            : base(packageRepo, planLimiter, accountLockdownManager, EAccountItem.Package)
        {
        }
    }
}
