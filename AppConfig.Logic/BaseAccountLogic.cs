﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Logic.Exceptions;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Logic
{
    public abstract class BaseAccountLogic<TModel, TRepo> : IBaseAccountLogic<TModel>
        where TRepo : IBaseAccountRepo<TModel>
        where TModel : BaseAccountModel
    {
        protected readonly TRepo _repo;
        protected readonly IPlanLimiter _planLimiter;
        protected readonly IAccountLockdownManager _lockdownManager;
        private readonly EAccountItem _accountItemType;

        protected BaseAccountLogic(TRepo repo, IPlanLimiter planLimiter, IAccountLockdownManager accountLockdownManager, EAccountItem accountItemType)
        {
            _repo = repo;
            _planLimiter = planLimiter;
            _lockdownManager = accountLockdownManager;
            _accountItemType = accountItemType;
        }

        public List<TModel> GetListForAccount(long accountId)
        {
            return _repo.GetListForAccount(accountId);
        }

        public bool DeleteForAccount(long packageId, long accountId, long userId)
        {
            return _repo.DeleteForAccount(packageId, accountId, userId);
        }

        public void SaveForAccount(TModel model, long accountId, long userId)
        {
            if (model == null) throw new ArgumentNullException("model");

            if (model.Id == 0) _planLimiter.CanAddAccountItemOrThrow(_accountItemType, accountId);

            if (_lockdownManager.IsAccountLockedDown(accountId))
                throw new AccountLockedDownException();

            model.AccountId = accountId; // enforce the account id we are running under
            _repo.SaveForAccount(model, accountId, userId);
        }
    }
}
