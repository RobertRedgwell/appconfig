﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Logic
{
    public class EnvironmentLogic : BaseAccountLogic<AppConfig.Model.Environment, IEnvironmentRepo>, IEnvironmentLogic
    {
        public EnvironmentLogic(IEnvironmentRepo environmentRepo, IPlanLimiter planLimiter, IAccountLockdownManager accountLockdownManager)
            : base(environmentRepo, planLimiter, accountLockdownManager, EAccountItem.Environment)
        {
        }
    }
}
