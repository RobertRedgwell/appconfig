﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using AppConfig.Persistence.Interfaces;

namespace AppConfig.Logic
{
    public class VariableLogic : BaseAccountLogic<Variable, IVariableRepo>, IVariableLogic
    {
        private readonly IVariableEnvironmentRepo _variableEnvironmentRepo;

        public VariableLogic(IVariableRepo packageRepo, IPlanLimiter planLimiter, IAccountLockdownManager accountLockdownManager, IVariableEnvironmentRepo variableEnvironmentRepo)
            : base(packageRepo, planLimiter, accountLockdownManager, EAccountItem.Variable)
        {
            _variableEnvironmentRepo = variableEnvironmentRepo;
        }

        public void SaveForAccount(Variable model, List<VariableEnvironment> variableEnvironments, long accountId, long userId)
        {
            // No MSDTC on Azure...
            //using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                SaveForAccount(model, accountId, userId);
                _variableEnvironmentRepo.SetVariableEnvironmentsForVariable(model, variableEnvironments, userId);
               //scope.Complete();
            }
        }

        public List<VariableEnvironment> GetForVariable(long variableId, long accountId)
        {
            return _variableEnvironmentRepo.GetForVariable(variableId, accountId);
        }
    }
}
