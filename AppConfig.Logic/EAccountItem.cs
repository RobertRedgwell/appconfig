﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppConfig.Logic
{
    /// <summary>
    /// An enum describing the various items that can vary on an account
    /// </summary>
    public enum EAccountItem
    {
        Unspecified,
        User,
        Environment,
        Variable,
        Package
    }
}
