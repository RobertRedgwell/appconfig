﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AppConfig.Logic.Interfaces;
using AppConfig.Model;
using FluentEmail;

namespace AppConfig.Logic
{
    public class Mailer : IMailer
    {
        private const string AdminEmailAddress = "redgwell@gmail.com";
        private const string NoReplyEmailAddress = "noreply@zapconfig.com";

        public void SubmitContactUsRequest(string name, string emailAddress, string message)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (emailAddress == null) throw new ArgumentNullException("emailAddress");
            if (message == null) throw new ArgumentNullException("message");

            var dto = new ContactUsRequestDto
            {
                Name = name,
                EmailAddress = emailAddress,
                Question = message,
            };

            var email = Email
                .From(emailAddress)
                .UsingClient(GetClient())
                .To(AdminEmailAddress, "Admin")
                .Subject("Contact form submitted on ZapConfig.com from " + emailAddress)
                .UsingTemplate("@Model.Name (@Model.EmailAddress) contacted you from the ZapConfig.com contact page:<br/><br/>@Model.Question", dto)
                .Send();
        }

        public void SubmitUpgradeRequest(long accountId, string userEmailAddress, EAccountType targetAccountType)
        {
            if (userEmailAddress == null) throw new ArgumentNullException("userEmailAddress");

            var dto = new UpgradeRequestDto
            {
                AccountId = accountId,
                UserEmailAddress = userEmailAddress,
                TargetAccountType = targetAccountType,
            };

            var email = Email
                .From(userEmailAddress)
                .UsingClient(GetClient())
                .To(AdminEmailAddress, "Admin")
                .Subject("Account upgrade request form submitted on ZapConfig.com from " + userEmailAddress)
                .UsingTemplate("A user with account id @Model.AccountId (@Model.UserEmailAddress) contacted you from the ZapConfig.com upgrade account page.<br/><br/>TargetAccountType: @Model.TargetAccountType", dto)
                .Send();
        }

        private enum ETemplate
        {
            WelcomeMail,
            WelcomeAdditionalUserMail,
        }

        public void SendWelcomeEmail(string emailAddress, string companyName, string validationKey, EAccountType accountType)
        {
            var dto = new WelcomeDto
            {
                UserEmailAddress = emailAddress,
                CompanyName = companyName,
                ValidationKey = validationKey,
                AccountType = accountType,
            };

            var email = Email
                .From(NoReplyEmailAddress)
                .UsingClient(GetClient())
                .To(emailAddress)
                .Subject("Welcome to ZapConfig")
                .UsingTemplateFromFile(GetTemplateFileName(ETemplate.WelcomeMail), dto)
                .Send();

        }

        public void SendValidateAccountEmail(string emailAddress, string companyName, string validationKey, EAccountType accountType)
        {
            SendWelcomeEmail(emailAddress, companyName, validationKey, accountType);
        }
        

        public void SendWelcomeAdditionalUserEmail(string emailAddress, string password, string companyName)
        {
            var dto = new WelcomeAdditionalUserDto { Password = password, CompanyName = companyName, };

            var email = Email
                .From(NoReplyEmailAddress)
                .UsingClient(GetClient())
                .To(emailAddress)
                .Subject("Welcome to ZapConfig")
                .UsingTemplateFromFile(GetTemplateFileName(ETemplate.WelcomeAdditionalUserMail), dto)
                .Send();
            
        }

        private static SmtpClient GetClient()
        {
            return new SmtpClient
            {
                Credentials = new NetworkCredential("azure_ee9d8f9b7591be3d809e7abb37537486@azure.com", "ebjg98c4")
            };
        }

        private string GetTemplateFileName(ETemplate template)
        {
            var rootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "bin",
                "EmailTemplates");
            
            switch (template)
            {
                case ETemplate.WelcomeMail:
                case ETemplate.WelcomeAdditionalUserMail:
                    return Path.Combine(rootFolder, template.ToString() + ".html");
                default:
                    throw new NotImplementedException(string.Format("Template {0} not implemented", template));
            }
        }

    }

    public class ContactUsRequestDto
    {
        public string EmailAddress { get; set; }
        public string Name { get; set; }
        public string Question { get; set; }
    }

    public class UpgradeRequestDto
    {
        public long AccountId { get; set; }
        public string UserEmailAddress { get; set; }
        public EAccountType TargetAccountType { get; set; }
    }

    public class WelcomeDto
    {
        public string CompanyName { get; set; }
        public string UserEmailAddress { get; set; }
        public string ValidationKey { get; set; }
        public EAccountType AccountType { get; set; }
    }

    public class WelcomeAdditionalUserDto
    {
        public string CompanyName { get; set; }
        public string Password { get; set; }
    }
}
